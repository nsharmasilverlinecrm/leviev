'use strict';

angular.module('levievApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'uiSlider'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'SearchCtrl'
      })
      .when('/results',{
        templateUrl: 'views/SearchResults.html',
        controller: 'SearchCtrl'
      })
      .when('/singleitem',{
        templateUrl: 'views/SingleItem.html',
        controller: 'SearchCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
