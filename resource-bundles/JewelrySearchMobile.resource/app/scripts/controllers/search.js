  'use strict';

  angular.module('levievApp')
  .service('CommonVariables', function CommonVariables($rootScope, $q) {

      this.queryParams = {
        searchType: '',
        stone: {
          stoneNumber: '',
          shapes: [],
          baseColor: '',
          colorQualityHigh: 0,
          colorQualityLow: 0,
          clarityLow: 0,
          clarityHigh: 0,
          caratLow: 0,
          caratHigh: 0,
          lotLayout: '',
          createDateLow: '',
          createDateHigh: '',
          certificateNumber: ''
        },
        jewelry: {
          jewelryNumber: '',
          setNumber: '',
          types: [],
          statuses: [],
          family: '',
          design: '',
          priceLow: 0,
          priceHigh: 0,
          colorPrimary: '',
          colorSecondary: '',
          location: '',
        }
      };

      this.searchResults = [];

      this.selectedItem = {};

      this.typeOptions = [{'Jewelry':'Bracelet','isChecked':'false'}
                          ,{'Jewelry':'Brooch','isChecked':'false'}
                          ,{'Jewelry':'Cufflink','isChecked':'false'}
                          ,{'Jewelry':'Earring','isChecked':'false'}
                          ,{'Jewelry':'Necklace','isChecked':'false'}
                          //{'Jewelry':'Other','isChecked':'false'}
                          ,{'Jewelry':'Pendant','isChecked':'false'}
                          ,{'Jewelry':'Ring','isChecked':'false'}
                          ,{'Jewelry':'Tiara','isChecked':'false'}
                          //,{'Jewelry':'Watch','isChecked':'false'}
                          //,{'Jewelry':'Loose','isChecked':'false'}
                          ];

      this.statusOptions = [{'Status':'I','isChecked':'false'},
                            {'Status':'M','isChecked':'false'},
                            {'Status':'S','isChecked':'false'},
                            {'Status':'IN','isChecked':'false'} 
                            ];

      this.shapeOptions = [{'Type':'MQ','isChecked':'false'}, 
                            {'Type':'OV','isChecked':'false'}, 
                            {'Type':'PS','isChecked':'false'},
                            {'Type':'PRIN','isChecked':'false'},
                            {'Type':'RAD','isChecked':'false'},
                            {'Type':'BR','isChecked':'false'},
                            {'Type':'trillion','isChecked':'false'},
                            {'Type':'HS','isChecked':'false'}, 
                            //{'Type':'emerald','isChecked':'false'},
                            //{'Type':'cushion','isChecked':'false'},
                            //{'Type':'asscher','isChecked':'false'}
                            ];

      this.path = '/';

      this.showStonesDetail = true;
      this.showJewelryDetail = true;

  })
    .controller('SearchCtrl', function ($scope, $q, Salesforce, $location, myService, CommonVariables, favList) {
      
      $scope.queryParams = CommonVariables.queryParams;
      $scope.searchResults = [];
      $scope.searchResults = CommonVariables.searchResults;
      $scope.selectedItem = CommonVariables.selectedItem;

      $scope.typeOptions = CommonVariables.typeOptions;
      $scope.statusOptions = CommonVariables.statusOptions;
      $scope.shapeOptions = CommonVariables.shapeOptions;

      $scope.showStonesDetail = CommonVariables.showStonesDetail;
      $scope.showJewelryDetail = CommonVariables.showJewelryDetail;
      
      CommonVariables.path = $location.path();
      
      if($location.path() == '/favorites') {
    
        $scope.searchMode = false;
        $scope.favoritesMode = true;
      } else {
          $scope.searchMode = true;
          $scope.favoritesMode = false;
      }

      favList().then(function(result) {$scope.sfdcResult = result;},
                           function(error)  {$scope.sfdcResult = 'ERROR: ' + error.message;}
                                      )
     
      /*$scope.switchMode = function(mode) {
        if(mode == 'search') {
          $scope.searchMode = true;
          $scope.favoritesMode = false;
        } else if(mode == 'favorites') {
          $scope.searchMode = false;
          $scope.favoritesMode = true;
        }
      };*/

       $scope.navigateToDetailPage = function (itemId) {
            
            //var jewelryId = this.item.Id.slice(0,-3);
            sforce.one.navigateToSObject($scope.selectedItem.ProductClone__c); 
        }

      $scope.colorOptions = ['White', 
                              'Yellow', 
                              'Pink', 
                              'Green', 
                              'Orange', 
                              'Red', 
                              'Brown', 
                              'Ruby', 
                              'Sapphire', 
                              'Emerald', 
                              'Pearl', 
                              'Other', 
                              'Onyx', 
                              'Metal'];

      $scope.familyOptions = ['General',
                                'Ferris Wheel',
                                'Figurative',
                                'Hopscotch',
                                'Swing',
                                'Vivid Heart',
                                'Cross'];
      $scope.designOptions = ['Bracelet - Bangle',
                              'Bracelet - Cluster',
                              'Bracelet - Line',
                              'Bracelet - Motif',
                              'Bracelet - Multi-Row',
                              'Brooch',
                              'Cufflink',
                              'Earring - Chandelier',
                              'Earring - Avant Garde',
                              'Earring - Clips',
                              'Earring - Cluster',
                              'Earring - Drops',
                              'Earring - Hoops',
                              'Earring - Studs',
                              'Necklace - Avant Garde',
                              'Necklace - Bib',
                              'Necklace - Big Drop',
                              'Necklace - Fringe',
                              'Necklace - Light Drop',
                              'Necklace - Line',
                              'Necklace - Motif',
                              'Necklace - Oldies',
                              'Necklace - Pendant',
                              'Necklace - Riviere',
                              'Ring - 3-Stone',
                              'Ring  - Band',
                              'Ring - Bypass',
                              'Ring - Chevaliere',
                              'Ring - Cluster',
                              'Ring - Cocktail',
                              'Ring - Engagement',
                              'Ring - Solitaire without Side Stones',
                              'Ring - Solitaire with Pave',
                              'Ring - Solitaire with Side Stones',
                              'Watch - Double Elle no. 17',
                              'Watch - Double Elle no. 16',
                              'Watch - Double Elle no. 30',
                              'Watch - Double Elle no. 8',
                              'Watch - Double Elle no. 13',
                              'Watch - Double Elle no. 15',
                              'Watch - Double Elle no. 5',
                              'Watch - GMT Alarm 18K Rose Gold',
                              'Watch - GMT Chrono 18K White Gold',
                              'Watch - GMT Chrono 18K White Gold Pave',
                              'Watch - GMT Chrono 18K White Gold Baguettes',
                              'Watch - GMT Chrono 18K Red Gold "Mega Yacht"',
                              'Loose - Pair',
                              'Loose - Single'];
      
      $scope.locationOptions = ['New York',
                                  'London',
                                  'Moscow',
                                  'Dubai',
                                  'Singapore'];


      if(CommonVariables.showJewelryDetail == false) {
        $('#jewelryDetails').slideDown();
      }
      if(CommonVariables.showStonesDetail == false) {
        $('#stoneDetails').slideDown();
      }
      
  /* methods */


      $scope.goToProduct = function(jewelry) {
        console.log(jewelry);
      }

      $scope.showStones = function() {
        $scope.showStonesDetail = false;
        $scope.showJewelryDetail = true;
        //$('#stoneDetails').slideDown();
        //$('#jewelryDetails').slideUp();
        CommonVariables.showStonesDetail = false;
        CommonVariables.showJewelryDetail = true;
      }

      $scope.hideStones = function() {
        $scope.showStonesDetail = true;
        //$('#stoneDetails').slideUp();
        CommonVariables.showStonesDetail = true;
      }

      $scope.showJewelry = function() {
        $scope.showJewelryDetail = false;
        $scope.showStonesDetail = true;
        //$('#jewelryDetails').slideDown();
        //$('#stoneDetails').slideUp();
        CommonVariables.showJewelryDetail = false;
        CommonVariables.showStonesDetail = true;
      }

      $scope.hideJewelry = function() {
        $scope.showJewelryDetail = true;
        //$('#jewelryDetails').slideUp();
        CommonVariables.showJewelryDetail = true;
      }

      $scope.addItem = function(item, list) {
        
        if(list.indexOf(item) === -1) {
          list.push(item);
        } else {
          list.splice(list.indexOf(item), 1);
        }
      };

      $scope.addShape = function(item, index, list) {

        if($scope.shapeOptions[index].isChecked == 'false' || $scope.shapeOptions[index].isChecked == false) {
          $scope.shapeOptions[index].isChecked = 'true';
          $('#'+item).addClass('overlay');
          
        } else {
          
          $scope.shapeOptions[index].isChecked = 'false';
          $('#'+item).removeClass('overlay');
        }

        if(list.indexOf(item) === -1) {
          list.push(item);
        } else {
          list.splice(list.indexOf(item), 1);
        }
      };

      $scope.resetStoneProperties = function() {
        
          var reset = confirm('Are you sure want to reset ?');

          if(reset == true) {
            
            /*for(var key in $scope.shapeOptions) {
              $scope.shapeOptions[key].isChecked = false;
            }*/

            for(var key in $scope.shapeOptions) {
              $scope.shapeOptions[key].isChecked = false;
              
              if($('#'+$scope.shapeOptions[key].Type).hasClass('overlay')) {
                $('#'+$scope.shapeOptions[key].Type).removeClass('overlay');
              }
            }

            $scope.queryParams.searchType = '';
            $scope.queryParams.stone = {
              stoneNumber: '',
              shapes: [],
              baseColor: '',
              colorQualityHigh: 0,
              colorQualityLow: 0,
              clarityLow: 0,
              clarityHigh: 0,
              caratLow: 0,
              caratHigh: 0,
              lotLayout: '',
              createDateLow: '',
              createDateHigh: '',
              certificateNumber: ''
            };

          $scope.searchResults.length = 0;
        }
          
      };

      $scope.selectAllShapes = function(list) {
        
        //alert('SET ALL CALLED');
        for(var key in $scope.shapeOptions) {
            
            $scope.shapeOptions[key].isChecked = true;
            
            if($('#'+$scope.shapeOptions[key].Type).hasClass('overlay')) {
                //do nothing
            } else {
                $('#'+$scope.shapeOptions[key].Type).addClass('overlay');
                list.push($scope.shapeOptions[key].Type);
            }
        }

      };

      $scope.resetJewelryProperties = function() {
        
          var reset = confirm('Are you sure want to reset ?');

          if(reset == true) {
            
            for(var key in $scope.typeOptions) {
              $scope.typeOptions[key].isChecked = false;
            }

            for(var key in $scope.statusOptions) {
              $scope.statusOptions[key].isChecked = false;
            }

            /*for(var key in $scope.shapeOptions) {
              $scope.shapeOptions[key].isChecked = false;
              
              if($('#'+$scope.shapeOptions[key].Type).hasClass('overlay')) {
                $('#'+$scope.shapeOptions[key].Type).removeClass('overlay');
              }
            }*/

            $scope.queryParams.searchType = '';
            $scope.queryParams.jewelry = {
              jewelryNumber: '',
              setNumber: '',
              types: [],
              statuses: [],
              family: '',
              design: '',
              priceLow: 20,
              priceHigh: 400,
              colorPrimary: '',
              colorSecondary: '',
              location: '',
            };

            /*$scope.queryParams.stone = {
              stoneNumber: '',
              shapes: [],
              baseColor: '',
              colorQualityHigh: 0,
              colorQualityLow: 0,
              clarityLow: 0,
              clarityHigh: 0,
              caratLow: 0,
              caratHigh: 0,
              lotLayout: '',
              createDateLow: '',
              createDateHigh: '',
              certificateNumber: ''
            };*/
        
          $scope.searchResults.length = 0;
        }
      };

      $scope.selectAllTypes = function(list) {
        
        //alert('SET ALL CALLED');
        for(var key in $scope.typeOptions) {
            
            if($scope.typeOptions[key].isChecked == false || $scope.typeOptions[key].isChecked == 'false') {
              list.push($scope.typeOptions[key].Jewelry);
            }
            $scope.typeOptions[key].isChecked = true;
        }

      };
      
      $scope.doSearch = function(searchType) {

        $scope.searchModeAnimate="animated fadeOut";
        //REFACTOR THIS TO USE ng-change and put slider values in separate object
        $scope.queryParams.searchType = searchType;

        //alert('==in do search=='+$scope.queryParams.stone.shapes);
        //alert('==in do search jewelry=='+$scope.queryParams.jewelry.types);

        var qpPrepped = JSON.parse(JSON.stringify($scope.queryParams));
        qpPrepped.stone.clarityLow = $scope.clarityFormatting(qpPrepped.stone.clarityLow);
        qpPrepped.stone.clarityHigh = $scope.clarityFormatting(qpPrepped.stone.clarityHigh);
        qpPrepped.stone.colorQualityLow = $scope.colorQualityFormatting(qpPrepped.stone.colorQualityLow);
        qpPrepped.stone.colorQualityHigh = $scope.colorQualityFormatting(qpPrepped.stone.colorQualityHigh);
        qpPrepped.jewelry.priceLow *= 1000;
        qpPrepped.jewelry.priceHigh *= 1000; 
        //console.log(qpPrepped);
        //alert(Salesforce.searchProducts);

        var promise = Salesforce.searchProducts(qpPrepped);
        promise.then(
          function(result) {
                console.log('RESULT: ' + result);
                
                $scope.searchResults.length = 0;
                for(var key in result) {
                  //alert(result[key] + "=====" + typeof result[key]);
                  if(typeof result[key] !== 'function') {
                    $scope.searchResults.push(result[key]);
                  }
                }
                $location.path('/results');
                
            }, function(reason) {
                //alert('Fail! Beacuse: ' + reason);
          }
        );
      };

      /*$scope.handleChangeCriteria = function() {
        $scope.showSearchControls = true;
        $scope.showResults = true;
        $scope.searchResults = {};
      };*/

      /* formatters for sliders */

      $scope.currencyFormattingThousands = function(value) { 
        if (value == undefined) {
          value = 0;
        }
        value =  Math.floor(value);
        var unit = 'K';
        if(value >= 1000) {
          value = value/1000;
          unit = 'M';
        }
        return '$' + value.toString() + unit;
      };
      $scope.clarityFormatting = function(value) {
        var retval;
        switch(Math.round(value)) {
          case 0:
            retval = 'FL';
            break;
          case 1:
            retval = 'IF';
            break;
          case 2:
            retval = 'WS1';
            break;
          case 3:
            retval = 'WS2';
            break;
          case 4:
            retval = 'VS1';
            break;
          case 5:
            retval = 'VS2';
            break;
          case 6:
            retval = 'SI1';
            break;
          case 7:
            retval = 'SI2';
            break;
          case 8:
            retval = 'I1';
            break;
          case 9:
            retval = 'I2';
            break;
          default:
            break;
        }
        return retval;
      };
      $scope.colorQualityFormatting = function(value) {
        var retval;
        var isWhite = ($scope.queryParams.stone.baseColor == 'White');
        switch(Math.round(value)) {
          case 0:
            retval = isWhite ? 'D' : 'Ft';
            break;
          case 1:
            retval = isWhite ? 'E' :  'Lt';
            break;
          case 2:
            retval = isWhite ? 'F' : 'F';
            break;
          case 3:
            retval = isWhite ? 'G' :  'Fl';
            break;
          case 4:
            retval = isWhite ? 'H' :  'FD';
            break;
          case 5:
            retval = isWhite ? 'I' :  'FV';
            break;
          case 6:
            retval = isWhite ? 'J' :  'FDk';
            break;
          default:
            break;
        }
        return retval;
      };

      $scope.createFavoriteList=function(){

          $location.path('/createFavoriteListPage');
      }

      $scope.favoriteDetailPage=function(favoriteListId){
          //alert("----"+favoriteListId);
          $scope.favoriteModeAnimate="animated fadeOut";
          //console.log("======"+favoriteListId);
          myService.openFavDetailPage(favoriteListId);

        }
    })
  .controller('SearchResultCtrl',function ($scope, $q, Salesforce, $location, CommonVariables, $swipe) {
        

        $scope.colors = ["#fc0003", "#f70008", "#f2000d", "#ed0012", "#e80017", "#e3001c", "#de0021", "#d90026", "#d4002b", "#cf0030", "#c90036", "#c4003b", "#bf0040", "#ba0045", "#b5004a", "#b0004f", "#ab0054", "#a60059", "#a1005e", "#9c0063", "#960069", "#91006e", "#8c0073", "#870078", "#82007d", "#7d0082", "#780087", "#73008c", "#6e0091", "#690096", "#63009c", "#5e00a1", "#5900a6", "#5400ab", "#4f00b0", "#4a00b5", "#4500ba", "#4000bf", "#3b00c4", "#3600c9", "#3000cf", "#2b00d4", "#2600d9", "#2100de", "#1c00e3", "#1700e8", "#1200ed", "#0d00f2", "#0800f7", "#0300fc"];
        $scope.searchMode = true;
        $scope.favoritesMode = false;
        $scope.queryParams = CommonVariables.queryParams;
        $scope.searchResults = CommonVariables.searchResults;
        console.log($scope.searchResults);

        //alert($scope.searchResults.length);
        $scope.showToggleButton = true;
        if($scope.searchResults.length == 0) {
          $scope.showToggleButton = false;
          $scope.displayText = 'There are no items avaibles related to this search';
        }

        $scope.switchToListView = function () {

            $location.path('/switchToListView');
        }

        $scope.switchToGridView = function () {

            $location.path('/results');
        }

        $scope.markAsFavorite = function (itemId) {
            
            $scope.searchResultAnimate="animated fadeOut";
            //alert(itemId);
            console.log('== item id =='+itemId);
            //alert(itemId);
            $location.path('/favoriteList/'+itemId);
            //document.getElementById(itemId).className = 'glyphicon glyphicon-star starColor';
        }

        $scope.viewSingleItem = function(item) {
          console.log(item);
          CommonVariables.selectedItem = item;
          $scope.searchResultAnimate="animated fadeOut";
          $location.path('/singleitem');
        }

        $scope.navigateToDetailPage = function (itemId) {
            
            //var jewelryId = this.item.Id.slice(0,-3);
            sforce.one.navigateToSObject($scope.selectedItem.ProductClone__c); 
        }
          
        function addSlide(target, style) {
                    
            var i = target.length;
            target.push({
                label: 'slide #' + (i + 1),
                img: 'http://lorempixel.com/450/300/' + style + '/' + (i % 10) ,
                //color: $scope.colors[ (i*10) % $scope.colors.length],
                odd: (i % 2 === 0)
            });
        };

        function addSlides(target, style, qty) {
            for (var i=0; i < qty; i++) {
                addSlide(target, style);
            }
        }

        // 1st ngRepeat demo
        // $scope.slides = [];
        // addSlides($scope.slides, 'sports', 50);

        // 2nd ngRepeat demo
        // $scope.slides2 = [];
        // $scope.slideIndex = 0;
        // addSlides($scope.slides2, 'city', 5);


        // demo with controls
        // $scope.slides4 = [];
        // addSlides($scope.slides4, 'people', 5);
        // $scope.pushSlide = function() {
        //     addSlides($scope.slides4, 'people', 3);
        // }
        $scope.prev = function() {
            $scope.slideIndex--;
        }
        $scope.next = function() {
            $scope.slideIndex++;
        }
        $scope.swipe = true;
        $scope.toggleSwipe = function() {
            $scope.swipe = !$scope.swipe;
        }

        // 4rd demo, object based iterable
        // $scope.slideIndex3 = 2;
    

        // thumbs demo
        // $scope.slideIndex2 = 2;

        // ngIf demo
        // $scope.showCarousel = false;
        // $scope.demo = {
        //     ifIndex: 2
        // }
        // $scope.slides5 = [
        //     $scope.colors[5],
        //     $scope.colors[15],
        //     $scope.colors[25],
        //     $scope.colors[35],
        //     $scope.colors[45]
        // ]

        $scope.searchMode = true;
        $scope.favoritesMode = false
        $scope.queryParams = CommonVariables.queryParams;
        $scope.searchResults = CommonVariables.searchResults;
        console.log($scope.searchResults);

        if($scope.searchResults.length <4){
          $scope.iterate=1;
        }
        else{
          $scope.iterate=Math.ceil($scope.searchResults.length/4);
        }

        //for pagination
        $scope.itemsPerPage = 4;
        $scope.pagedItems = [];
        $scope.currentPage = 0;
        
        for (var i = 0; i < $scope.searchResults.length; i++) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.searchResults[i] ];
            } else {
                $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.searchResults[i]);
            }
        }
        
        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
              
                $scope.currentPage--;
            }
        };
        
        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                $scope.currentPage++;
            }
        };
        
    })
  .controller('createFavoriteListCtrl',function ($scope,$routeParams,$location,myService,accountList,filteredFavList,CommonVariables){
  
        if(CommonVariables.path == '/') {
          
          $scope.searchMode = true;
          $scope.favoritesMode = false;
        } else {

          $scope.searchMode = false;
          $scope.favoritesMode = true;
        }

        $scope.toggleLookupView=function(){

          $scope.showDetails = !$scope.showDetails;
        }

        $scope.createNewFavoriteList=function(favoriteListName,accountId){
          //alert("==="+favoriteListName+"--"+accountId);

          myService.createNewFavoriteList(favoriteListName,accountId).then(function(success) {

                                                                              console.log('========'+$routeParams.itemId);
                                                                              console.log(success);

                                                                              if(typeof $routeParams.itemId == 'undefined') {

                                                                                $location.path('/favorites');
                                                                              } else {

                                                                                myService.addFavouriteListItem(success.Id,$routeParams.itemId).then(function (success) {
                                                                                                                      
                                                                                                                      $location.path('/results');
                                                                                                                  },function (error) {
                                                                                                                      console.log('==='+error);
                                                                                                                  });

                                                                              }
                                                                              
                                                                          },function(error) {
                                                                              console.log('==='+error);
                                                                          });
        }
  })

   .controller('addToFavoritePageCtrl',function ($scope,$routeParams,$location,myService,accountList,filteredFavList){
  
        $scope.searchMode = true;
        $scope.favoritesMode = false;

        $scope.createFavoriteList=function(selectedJewelryId){
          //alert(selectedJewelryId);
          $location.path('/createFavoriteListPage/'+selectedJewelryId);
        }

        $scope.createNewFavoriteList=function(favoriteListName,accountId){
          //alert("==="+favoriteListName+"--"+accountId);

          myService.createNewFavoriteList(favoriteListName,accountId).then(function (success) {
                                                        
                                                                    $location.path('/favorites');
                                                                },function (error) {
                                                                    console.log('==='+error);
                                                                });
        }

        $scope.addItem=function(favList,favListId,itemId){

          $scope.favListId=favListId;

          $scope.favoritesHomeViewAnimate="animated fadeOut";
         
           myService.addFavouriteListItem(favListId,itemId).then(function (success) {
                                                                    
                                                                    //alert('item added');
                                                                    favList.isDisabled = true;
                                                                    //$location.path('/FavoriteDetailPage/'+favListId);
                                                                },function (error) {
                                                                    console.log('==='+error);
                                                                });
        }


        filteredFavList($routeParams.id).then(function(result) {
                                                
                                                for(var index = 0; index< result.length;index++) {
                                                  result[index].isDisabled = false;
                                                }

                                                $scope.sfdcResult = result;
                                              }, function(error)  {
                                                $scope.sfdcResult = 'ERROR: ' + error.message;
                                           })

        $scope.selectedJewelryId = $routeParams.id;

  })

  .controller('favoriteDetailPageCtrl',function($routeParams, $location, $scope,favoriteDetailPageFactory,CommonVariables){
 
        if(CommonVariables.path == '/favorites') {
            
            $scope.searchMode = false;
            $scope.favoritesMode = true;

        } else {
            
            $scope.searchMode = true;
            $scope.favoritesMode = false;
        }

        $scope.navigateToDetailPage = function (itemId) {
            
            //var jewelryId = this.item.Id.slice(0,-3);
            sforce.one.navigateToSObject(itemId); 
        }
        
        var favoriteListId=$routeParams.id.substr(0,15);
 
        favoriteDetailPageFactory(favoriteListId).then(function(result) { 
                                                    
                                                      $scope.sfdcResult = result;

                                                      for(var key in result) {

                                                        console.log('== key =='+key);
                                                        console.log(result[key]);
                                                      }

                                                      function addSlide(target, style) {
                                                                    
                                                          var i = target.length;
                                                          target.push({
                                                              label: 'slide #' + (i + 1),
                                                              img: 'http://lorempixel.com/450/300/' + style + '/' + (i % 10) ,
                                                              //color: $scope.colors[ (i*10) % $scope.colors.length],
                                                              odd: (i % 2 === 0)
                                                          });
                                                      };

                                                      function addSlides(target, style, qty) {
                                                          for (var i=0; i < qty; i++) {
                                                              addSlide(target, style);
                                                          }
                                                      }

                                                      $scope.prev = function() {
                                                          $scope.slideIndex--;
                                                      }
                                                      $scope.next = function() {
                                                          $scope.slideIndex++;
                                                      }
                                                      $scope.swipe = true;
                                                      $scope.toggleSwipe = function() {
                                                          $scope.swipe = !$scope.swipe;
                                                      } 

                                                      if($scope.sfdcResult.length == 0) {
                                                        $scope.displayText = 'There are no items added to this list';
                                                      }

                                                      if($scope.sfdcResult.length <4){
                                                        $scope.iterate=1;
                                                      }
                                                      else{
                                                        $scope.iterate=Math.ceil($scope.sfdcResult.length/4);
                                                      }
                                                      
                                                      $scope.itemsPerPage = 4;
                                                      $scope.pagedItems = [];
                                                      $scope.currentPage = 0;
                                                      
                                                      for (var i = 0; i < $scope.sfdcResult.length; i++) {
                                                          if (i % $scope.itemsPerPage === 0) {
                                                              $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.sfdcResult[i] ];
                                                          } else {
                                                              $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.sfdcResult[i]);
                                                          }
                                                      }
                                                      
                                                      $scope.prevPage = function () {
                                                          if ($scope.currentPage > 0) {
                                                              $scope.currentPage--;
                                                          }
                                                      };
                                                      
                                                      $scope.nextPage = function () {
                                                          if ($scope.currentPage < $scope.pagedItems.length - 1) {
                                                              $scope.currentPage++;
                                                          }
                                                      };                
                                                  },
                                                    function(error) {
                                                        $scope.sfdcResult = 'ERROR: ' + error.message;
                                                    })
    
  });

