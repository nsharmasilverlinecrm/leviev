'use strict';

var leviev = angular.module('levievApp');

  leviev.directive('levievHeader', function ($location) {
    return {
      template: '<div class="header" style="margin-right:10px;">'+
                  '<center><h3 id="headerLogo"></h3><div class="headerLogo"></div></center>'+
                  '<span id="backButton" class="" style="float:left;margin-top:-44px;margin-left:10px;display:none; font-weight:bolder; font-size: 27px;cursor:pointer;" ng-click=backToHome()>&#12296;</span>'+
                  '<span id="backButtonToSearchResults" class="" style="float:left;margin-top:-44px;margin-left:10px;display:none;font-weight:bolder; font-size: 27px;cursor:pointer;" ng-click=backToSearchResults()>&#12296;</span>'+
                  '<span id="backButtonToFavoritesList" class="" style="float:left;margin-top:-44px;margin-left:10px;display:none;font-weight:bolder; font-size: 27px;cursor:pointer;" ng-click=backToFavoritesList()>&#12296;</span>'+
                  
                  '<ul class="nav nav-pills nav-pull-right" style="float:right;margin-top:-44px;">'+
                  '<li ng-class="{active:searchMode}" ><a ng-click=switchMode("search")>Search</a></li>'+
                  '<li ng-class="{active:favoritesMode}"><a ng-click=switchMode("favorites")>Favorites</a></li>'+
                  '</ul>'+
                '</div>',
      restrict: 'E',
      replace:true,
      scope:{
        searchMode:'=',
        favoritesMode:'='
      },
      link: function($scope, $element, $attrs){
        
          //$scope.searchMode = true;
          //$scope.favoritesMode = false;

          $scope.switchMode = function(mode) {
            
            if(mode == 'search') {
              /*window.setTimeout(function() {
                alert('FROM SEARCH');
              }, 2000);*/
              $location.path('/');
              $scope.searchMode = true;
              //$('#favoriteMode').fadeOut();
              //$('#searchMode').fadeIn(5000);
              $scope.favoritesMode = false;
            } else if(mode == 'favorites') {
              /*window.setTimeout(function() {
                alert('FROM FAV');
              }, 2000);*/
              $location.path('/favorites');
              $scope.searchMode = false;
              $scope.favoritesMode = true;
              //$('#favoriteMode').fadeIn(5000);
              //$('#searchMode').fadeOut();
            }
          }

          $scope.backToHome = function () {
            $location.path('/');
          }

          $scope.backToSearchResults = function () {
            $location.path('/results');
          }

          $scope.backToFavoritesList = function () {
            $location.path('/favorites');
          }
      }
    };
  });

  leviev.directive('searchHeader', function () {
    return {
      template: '<div style="background:#808080;border-radius:2px;font-size:20px;font-weight:300;padding-top:5px;padding-bottom:5px;"><center>'+
      '<span id="searchResultText">Search Results<span><center></div>',
      restrict: 'E',
      replace:true,
      scope:{
        queryParams:'='
      },
      link: function($scope, $element, $attrs){
        //console.log('==queryParams=='+$scope.queryParams);

        /*<h4>Searching For</h4>'+
        '<ul class="list-inline">'+
        '<!--li>Shapes: <span ng-repeat="shape in queryParams.stone.shapes">{{shape}}{{$last ? "" : ", "}}</span></li-->'+
        '<li>Types: <span ng-repeat="type in queryParams.jewelry.types">{{type}}{{$last ? "" : ", "}}</span></li>|'+
        '<li>Statuses: <span ng-repeat="stat in queryParams.jewelry.statuses">{{stat}}{{$last ? "" : ", "}}</span></li>|'+
        '<li>Colors: <span>{{queryParams.jewelry.colorPrimary}}{{$last ? "" : ", "}}{{queryParams.jewelry.colorSecondary}}</span></li>|'+
        '<li>Selected Location: <span>{{queryParams.jewelry.location}}</span></li>'+
        '<!--li>Color Quality: {{colorQualityFormatting(queryParams.stone.colorQualityLow)}} to {{colorQualityFormatting(queryParams.stone.colorQualityHigh)}}</li-->'+
        '<!--li>Carats: {{queryParams.stone.caratLow}} to {{queryParams.stone.caratHigh}}</li--></ul>'
          */
      }
    };
  });

  // leviev.directive('searchResult', function ($q,$location) {
  //   return {
  //     template: '',
  //     restrict: 'E',
  //     replace:true,
  //     scope:{
  //       item:'='
  //     },
  //     link: function($scope, $element, $attrs){
        
  //         $scope.navigateToDetailPage = function (itemId) {
            
  //           var jewelryId = this.item.Id.slice(0,-3);
  //           sforce.one.navigateToSObject(jewelryId); 
  //         }

  //         $scope.markAsFavorite = function (itemId) {
            
  //           //alert(itemId);
  //           $location.path('/favoriteList/'+itemId);
  //           //document.getElementById(itemId).className = 'glyphicon glyphicon-star starColor';
  //         }
  //     }
  //   };
  // });

  leviev.directive('favouriteDetailPage', function (favoriteDetailPageFactory) {
    return {
      template: ' <div class="col-md-6 col-lg-6 boxContainer animated fadeIn">' +
                '<div class="contentBody"><div class="lev-image" style="background-image:url(\'{{item.Jewelry__r.Image_Source__c}}\')"/></div>'+
                '<div class="contentFooter" style="color:black;"><span style="margin-left:10px;">{{item.Jewelry__r.Name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{item.Jewelry__r.Total_Weight__c}} carats </span>'+
                '<span id="{{item.Jewelry__r.Id}}" ng-click="markAsFavorite(item.Id)" style="float:right;margin-right:10px;cursor:pointer;" class="glyphicon glyphicon-star starColor"></span><br/>'+
                '<span style="margin-left:10px;">{{item.Jewelry__r.GBP_Search_Price__c}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{item.Jewelry__r.Item_Location__c}}</span>'+
                '</div>' ,
 
      restrict: 'E',
      scope:{
        item:'='
      }
    
    };
  });


  leviev.directive('backButton', function () {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attrs) {

        $element.find('#backButton').eq(0).show();
      }
    };
  });

  leviev.directive('backButtonToSearchResults', function () {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attrs) {

        $element.find('#backButtonToSearchResults').eq(0).show();
      }
    };
  });

  leviev.directive('backButtonToFavoritesList', function () {
    return {
      restrict: 'A',
      link: function ($scope, $element, $attrs) {

        $element.find('#backButtonToFavoritesList').eq(0).show();
      }
    };
  });
  
  leviev.directive('favouriteList', function (favList) {
    return {
      template: 
                ' <div style="margin-top:10px;"><ul style="list-style-type:none;">' +
                ' <li class=\"list text\" ng-click="favoriteDetailPage(item.Id)" ng-repeat=\"item in sfdcResult\"><span style="margin-left:10px;">{{item.Name}}<span><span class=\"tab\" style="color:#808080;">{{item.FavouriteList_Account__r.FirstName}}{{item.FavouriteList_Account__r.LastName}}</span></li>\n' +
                ' </ul></div>',
      restrict: 'E'
    };
  });

  leviev.directive('favouriteListWithSelectButton', function (filteredFavList) {
    return {
      template: ' <div style="margin-top:10px;"><ul style="list-style-type:none;">' +
                ' <li class=\"list text\" ng-repeat=\"item in sfdcResult\"><span style="margin-left:10px;">{{item.Name}}<span><span class=\"tab\" style="color:#808080;">{{item.FavouriteList_Account__r.FirstName}}{{item.FavouriteList_Account__r.LastName}}</span>'+
                '<button class=\"btn btn-default right\" style="margin-right:10px;" ng-disabled="item.isDisabled" ng-click=\"addItem(item,(item.Id).substr(0,15),selectedJewelryId)\">Select</button></li>\n' +
                ' </ul></div>',
      restrict: 'E',
      link: function($scope, $element, $attrs) {
         
      }
    };
  });

 leviev.directive('s1LookupView', function(accountList){
  return {
    restrict: 'E',
    template:  "<div class=\"s1-modal\" >\n" +
    "<s1-Header-Primary-Cancel title=\"{{fieldName}}\" on-button-left=\"onButtonLeft()\" button-Left=\"Cancel\"></s1-Header-Primary-Cancel>\n" + 
    "<input type=\"text\" placeholder=\"Search\" ng-model=\"searchTerm\" class=\"size-full plm prx pvm input input--default input--ph-1 input--focus-1\"></input>\n" +
    "<s1-Anchor-Light-No-Background icon=\"{{sObjectType}}\" title=\"{{fieldName}}\"></s1-Anchor-Light-No-Background>\n" +
    "<s1-List-Item-Container>\n" +
    "<s1-List-Item-Default ng-click=\"onSelectItem()(item)\" ng-repeat=\"item in items | filter:searchTerm\"  text=\"{{item.Name}}\"/>\n" +
    "</s1-List-Item-Container>\n" +
    "<s1-List-Item-Container ng-if=\"searchTerm.length > 2\">\n" +
    "<s1-List-Item-Default ng-click=\"soslSearch(searchTerm)\" text=\"Search for {{searchTerm}}\"/>\n" +
    "</s1-List-Item-Container>\n" +    
    "</div>",
    replace: true,
    scope: {
      listItemDefaultTitle: '@text',
      sObjectType:'@',
      fieldName:'@',
      onButtonLeft:'&',
      onSelectItem:'&'
    },
    link: function($scope, $element, $attrs) {
      
      accountList().then(function(results){
            console.log("==="+results);
            $scope.items = results;


      },
      function(error){
          console.log(error);
      });    

      

      $scope.soslSearch = function (searchString) {
        var sosl = 'FIND {' + searchString + '*} IN ALL FIELDS RETURNING ' + $scope.sObjectType + '(Id,Name)';
        vfr.search(sosl)
        .then(function(results){
              console.log(results);
              $scope.items = results[0];
        },
        function(error){
            console.log(error);
        });    
      }
    }
  };
});

leviev.directive('s1Lookup', function(){
  return {
    restrict: 'E',
    template:   "<div>\n" +
    "   <s1-Lookup-Default account-Id=\"{{selectedItem.Id}}\" search-Text=\"{{selectedItem.Name}}\" placeholder-text=\"{{textInputPlaceholder}}\" ng-click=\"showDetails = ! showDetails\"></s1-Lookup-Default>\n" +
    "   <s1-Lookup-View on-select-item=\"itemSelected\" on-button-left=\"toggleLookupView()\" field-Name=\"{{fieldName}}\" s-Object-Type=\"{{sObjectType}}\" class=\"s1-modal\"  ng-if=\"showDetails\" button-Left=\"Cancel\" items=\"{{recentItems}}\"></s1-Lookup-View>\n" +
    "</div>",
    replace: true,
    scope: {
      textInputPlaceholder: '@textPlaceholder',
      sObjectType:'@',
      fieldName:'@',
      selectedId:'=',
      selectedItem:'='
    },
    link: function($scope, $element, $attrs) {
      $scope.itemSelected = function(item) {
        //alert("==!!!=="+item.id);
        $scope.selectedItem = item;
        $scope.selectedId = item.Id;
        $scope.showDetails = !$scope.showDetails;
        console.log(item);
      }      
      $scope.toggleLookupView = function() {

        $scope.showDetails = !$scope.showDetails;
      }
    }
  };
});

leviev.directive('s1LookupDefault', function(){
  return {
    restrict: 'E',
    template:   "<div class=\"mbm pos-rel\">\n" +
    "  <label class=\"size-full glyphicon glyphicon-search pos-abs label--lookup\">\n" +
    "    <span class=\"dn\">Lookup</span>\n" +
    "  </label>\n" +
    "  <input type='text' style=\"width:90%;float:right;\" id=\"lookup_Account\" ng-model=\"searchText\" ng-click=\"lookup()\" name=\"{{accountId}}\" value=\"{{displayText}}\"  class=\"plm prx pvm input input--default input--ph-1 input--focus-1\" readonly />\n" +
    "</div>",
    replace: true,
    scope: {
      searchText: '@',
      displayText: '@',
      placeholderText: '@',
      accountId : '@'
    },
    link: function($scope, $element, $attrs) {
      var isTouchDevice;
      return isTouchDevice = function() {
        return "ontouchstart" in window || "onmsgesturechange" in window;
      };
    }
  };
});

leviev.directive('s1HeaderPrimaryCancel', function(){
  return {
    restrict: 'E',
    template:  "<header class=\"bg-generic-3 ht-44 dropglow-1 tc\" role=\"group\">\n" +
    "  <a href=\"javascript:void(0)\" title=\"{{headerPrimaryModalButtonLeft}}\"\n" +
    "     class=\"btn--header-secondary brm border border--header_button-1 bg-secondary-header-btn fl mts mls phm ht-30 is_1\">\n" +
    "    <span class=\"text-color-5 lh-30 f6\">{{headerPrimaryModalButtonLeft || 'Close'}}</span>\n" +
    "  </a>\n" +
    "  <span class=\"text-color-5 dib lh-44 fw-semibold f3\">{{headerPrimaryModalTitle || 'New Contact'}}</span>\n" +
    "</header>",
    replace: true,
    scope: {
      headerPrimaryModalButtonLeft: '@buttonLeft',
      headerPrimaryModalTitle: '@title',
      onButtonLeft:'&',
    },
    link: function($scope, $element, $attrs) {
      var clickHandler,attrName;
      clickHandler = function (event) {
      var func;
      // if ($attrs[attrName] === void 0) {
      //   return console.log("Warning: No event listener for onNew");
      // }
      attrName = 'onButtonLeft';
      func = $scope[attrName];
      console.log('click======'+attrName);
      if (typeof func === 'function') {
        return $scope.$apply(function() {
          return func(event);
        });
      } else {
        return console.log("Error: onNew needs to be a function.");
      }
    };  
    angular.element($element[0].querySelector('a[title="{{headerPrimaryModalButtonLeft}}"]')).on("click", clickHandler);

      //return clickEl($scope, $element, $attrs, [{querySelector : 'a[title="{{headerPrimaryModalButtonLeft}}"]', attrName : 'onButtonLeft'},{querySelector : 'a[title="{{headerPrimaryModalButtonRight}}"]', attrName : 'onButtonRight'}]);
    }
  };
});

leviev.directive('s1AnchorLightNoBackground', function(){
  return {
    restrict: 'E',
    template:   "<header class=\"bg-2\">\n" +
    "  <div class=\"icon icon--{{anchorLightNoBGIcon || 'lead'}} bgs-100 a-mid mhm sq-30\"></div>\n" +
    "  <h1 class=\"dib thin a-mid\">\n" +
    "    <span class=\"db f3 text-color-1\">\n" +
    "      {{anchorLightNoBGTitle || 'Leads'}}\n" +
    "    </span>\n" +
    "    <span class=\"db f6 lower\">\n" +
    "      {{anchorLightNoBGLabel || 'recent'}}\n" +
    "    </span>\n" +
    "  </h1>\n" +
    "</header>",
    replace: true,
    scope: {
      anchorLightNoBGLabel: '@label',
      anchorLightNoBGTitle: '@title',
      anchorLightNoBGIcon: '@icon',
      onNew: '&'
    },
    link: function($scope, $element, $attrs) {
      // return clickEl($scope, $element, $attrs, [{querySelector : 'a[title="Email"]', attrName : 'onEmail'}]);
    }
  };
});


leviev.directive('s1ListItemContainer', function(){
  return {
    restrict: 'E',
    template:  "<ul class=\"list-plain fw-normal bg-2 man pan\" ng-transclude>\n" +
    "</ul>",
    replace: true,
    transclude: true,
    scope: {},
    link: function($scope, $element, $attrs) {
      var isTouchDevice;
      return isTouchDevice = function() {
        return "ontouchstart" in window || "onmsgesturechange" in window;
      };
    }
  };
});

leviev.directive('s1ListItemDefault', function(){
  return {
    restrict: 'E',
    template:  "  <li class=\"list-plain active--list-1 pam text-color-1 f4 border-bottom border--3\" style=\"cursor:default\">{{listItemDefaultTitle || 'List Item'}}</li>"
  ,
    replace: true,
    scope: {
      listItemDefaultTitle: '@text'
    },
    link: function($scope, $element, $attrs) {
      var isTouchDevice;
      return isTouchDevice = function() {
        return "ontouchstart" in window || "onmsgesturechange" in window;
      };
    }
  };
});



/*----------- Carousel ------------- */
leviev.directive('rnCarousel', function($swipe,$compile,$document,$parse,$window) {
        // internal ids to allow multiple instances
        var carouselId = 0,
            // used to compute the sliding speed
            timeConstant = 75,
            // in container % how much we need to drag to trigger the slide change
            moveTreshold = 0.05,
            // in absolute pixels, at which distance the slide stick to the edge on release
            rubberTreshold = 3;

        return {
            restrict: 'A',
            scope: true,
            compile: function(tElement, tAttributes) {
                // use the compile phase to customize the DOM
                var firstChildAttributes = tElement.children()[0].attributes,
                    isRepeatBased = false,
                    isBuffered = false,
                    slidesCount = 0,
                    isIndexBound = false,
                    repeatItem,
                    repeatCollection;

                // add CSS classes
                tElement.addClass('rn-carousel-slides');
                tElement.children().addClass('rn-carousel-slide');

                // try to find an ngRepeat expression
                // at this point, the attributes are not yet normalized so we need to try various syntax
                ['ng-repeat', 'data-ng-repeat', 'x-ng-repeat'].every(function(attr) {
                    var repeatAttribute = firstChildAttributes[attr];
                    if (angular.isDefined(repeatAttribute)) {
                        // ngRepeat regexp extracted from angular 1.2.7 src
                        var exprMatch = repeatAttribute.value.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),
                            trackProperty = exprMatch[3];

                        repeatItem = exprMatch[1];
                        repeatCollection = exprMatch[2];

                        if (repeatItem) {
                            if (angular.isDefined(tAttributes['rnCarouselBuffered'])) {
                                // update the current ngRepeat expression and add a slice operator if buffered
                                isBuffered = true;
                                repeatAttribute.value = repeatItem + ' in ' + repeatCollection + '|carouselSlice:carouselBufferIndex:carouselBufferSize';
                                if (trackProperty) {
                                    repeatAttribute.value += ' track by ' + trackProperty;
                                }
                            }
                            isRepeatBased = true;
                            return false;
                        }
                    }
                    return true;
                });

                return function(scope, iElement, iAttributes, containerCtrl) {

                    carouselId++;

                    var containerWidth,
                        transformProperty,
                        pressed,
                        startX,
                        amplitude,
                        offset = 0,
                        destination,
                        slidesCount = 0,
                        // javascript based animation easing
                        timestamp;

                    // add a wrapper div that will hide the overflow
                    var carousel = iElement.wrap("<div id='carousel-" + carouselId +"' class='rn-carousel-container'></div>"),
                        container = carousel.parent();

                    // if indicator or controls, setup the watch
                    if (angular.isDefined(iAttributes.rnCarouselIndicator) || angular.isDefined(iAttributes.rnCarouselControl)) {
                        updateIndicatorArray();
                        scope.$watch('carouselIndex', function(newValue) {
                            scope.indicatorIndex = newValue;
                        });
                        scope.$watch('indicatorIndex', function(newValue) {
                            goToSlide(newValue, true);
                        });

                    }

                    // enable carousel indicator
                    if (angular.isDefined(iAttributes.rnCarouselIndicator)) {
                        var indicator = $compile("<div id='carousel-" + carouselId +"-indicator' index='indicatorIndex' items='carouselIndicatorArray' rn-carousel-indicators class='rn-carousel-indicator'></div>")(scope);
                        container.append(indicator);
                    }

                    // enable carousel controls
                    if (angular.isDefined(iAttributes.rnCarouselControl)) {
                        var controls = $compile("<div id='carousel-" + carouselId +"-controls' index='indicatorIndex' items='carouselIndicatorArray' rn-carousel-controls class='rn-carousel-controls'></div>")(scope);
                        container.append(controls);
                    }

                    scope.carouselBufferIndex = 0;
                    scope.carouselBufferSize = 5;
                    scope.carouselIndex = 0;

                    // handle index databinding
                    if (iAttributes.rnCarouselIndex) {
                        var indexModel = $parse(iAttributes.rnCarouselIndex);
                        if (angular.isFunction(indexModel.assign)) {
                            /* check if this property is assignable then watch it */
                            scope.$watch('carouselIndex', function(newValue) {
                                indexModel.assign(scope.$parent, newValue);
                            });
                            scope.carouselIndex = indexModel(scope);
                            scope.$parent.$watch(indexModel, function(newValue, oldValue) {
                              if (newValue!==undefined) {
                                // todo: ensure valid
                                goToSlide(newValue, true);
                              }
                            });
                            isIndexBound = true;
                        } else if (!isNaN(iAttributes.rnCarouselIndex)) {
                          /* if user just set an initial number, set it */
                          scope.carouselIndex = parseInt(iAttributes.rnCarouselIndex, 10);
                        }
                    }

                    // watch the given collection
                    if (isRepeatBased) {
                        scope.$watchCollection(repeatCollection, function(newValue, oldValue) {
                            slidesCount = 0;
                            if (angular.isArray(newValue)) {
                                slidesCount = newValue.length;
                            } else if (angular.isObject(newValue)) {
                                slidesCount = Object.keys(newValue).length;
                            }
                            updateIndicatorArray();
                            if (!containerWidth) updateContainerWidth();
                            goToSlide(scope.carouselIndex);
                        });
                    } else {
                        slidesCount = iElement.children().length;
                        updateIndicatorArray();
                        updateContainerWidth();
                    }

                    function updateIndicatorArray() {
                        // generate an array to be used by the indicators
                        var items = [];
                        for (var i = 0; i < slidesCount; i++) items[i] = i;
                        scope.carouselIndicatorArray = items;
                    }

                    function getCarouselWidth() {
                       // container.css('width', 'auto');
                        var slides = carousel.children();
                        if (slides.length === 0) {
                            containerWidth = carousel[0].getBoundingClientRect().width;
                        } else {
                            containerWidth = slides[0].getBoundingClientRect().width;
                        }
                        // console.log('getCarouselWidth', containerWidth);
                        return Math.floor(containerWidth);
                    }

                    function updateContainerWidth() {
                        // force the carousel container width to match the first slide width
                        container.css('width', '100%');
                        container.css('width', getCarouselWidth() + 'px');
                    }

                    function scroll(x) {
                        // use CSS 3D transform to move the carousel
                        //console.log('scroll', x, 'index', scope.carouselIndex);
                        if (isNaN(x)) {
                            x = scope.carouselIndex * containerWidth;
                        }

                        offset = x;
                        var move = -Math.round(offset);
                        move += (scope.carouselBufferIndex * containerWidth);
                        carousel[0].style[transformProperty] = 'translate3d(' + move + 'px, 0, 0)';
                    }

                    function autoScroll() {
                        // scroll smoothly to "destination" until we reach it
                        // using requestAnimationFrame
                        var elapsed, delta;

                        if (amplitude) {
                            elapsed = Date.now() - timestamp;
                            delta = amplitude * Math.exp(-elapsed / timeConstant);
                            if (delta > rubberTreshold || delta < -rubberTreshold) {
                                scroll(destination - delta);
                                requestAnimationFrame(autoScroll);
                            } else {
                                goToSlide(destination / containerWidth);
                            }
                        }
                    }

                    function capIndex(idx) {
                        // ensure given index it inside bounds
                        return (idx >= slidesCount) ? slidesCount: (idx <= 0) ? 0 : idx;
                    }

                    function updateBufferIndex() {
                        // update and cap te buffer index
                        var bufferIndex = 0;
                        var bufferEdgeSize = (scope.carouselBufferSize - 1) / 2;
                        if (isBuffered) {
                            if (scope.carouselIndex <= bufferEdgeSize) {
                                bufferIndex = 0;
                            } else if (scope.carouselIndex > slidesCount - scope.carouselBufferSize) {
                                bufferIndex = slidesCount - scope.carouselBufferSize;
                            } else {
                                bufferIndex = scope.carouselIndex - bufferEdgeSize;
                            }
                        }
                        scope.carouselBufferIndex = bufferIndex;
                    }

                    function goToSlide(i, animate) {
                        if (isNaN(i)) {
                            i = scope.carouselIndex;
                        }
                        if (animate) {
                            // simulate a swipe so we have the standard animation
                            // used when external binding index is updated or touch canceed
                            offset = (i * containerWidth);
                            swipeEnd(null, null, true);
                            return;
                        }
                        scope.carouselIndex = capIndex(i);
                        updateBufferIndex();
                        // if outside of angular scope, trigger angular digest cycle
                        // use local digest only for perfs if no index bound
                        if (scope.$$phase!=='$apply' && scope.$$phase!=='$digest') {
                            if (isIndexBound) {
                                scope.$apply();
                            } else {
                                scope.$digest();
                            }
                        }
                        scroll();
                    }

                    function getAbsMoveTreshold() {
                        // return min pixels required to move a slide
                        return moveTreshold * containerWidth;
                    }

                    function documentMouseUpEvent(event) {
                        // in case we click outside the carousel, trigger a fake swipeEnd
                        swipeEnd({
                            x: event.clientX,
                            y: event.clientY
                        }, event);
                    }

                    function capPosition(x) {
                        // limit position if start or end of slides
                        var position = x;
                        if (scope.carouselIndex===0) {
                            position = Math.max(-getAbsMoveTreshold(), position);
                        } else if (scope.carouselIndex===slidesCount-1) {
                            position = Math.min(((slidesCount-1)*containerWidth + getAbsMoveTreshold()), position);
                        }
                        return position;
                    }

                    function swipeStart(coords, event) {
                        //console.log('swipeStart', coords, event);
                        $document.bind('mouseup', documentMouseUpEvent);
                        pressed = true;
                        startX = coords.x;

                        amplitude = 0;
                        timestamp = Date.now();

                        //event.preventDefault();
                        //event.stopPropagation();
                        return false;
                    }

                    function swipeMove(coords, event) {
                        //console.log('swipeMove', coords, event);
                        var x, delta;
                        if (pressed) {
                            x = coords.x;
                            delta = startX - x;
                            if (delta > 2 || delta < -2) {
                                startX = x;
                                requestAnimationFrame(function() {
                                    scroll(capPosition(offset + delta));
                                });
                            }
                        }
                       // event.preventDefault();
                        //event.stopPropagation();
                        return false;
                    }

                    function swipeEnd(coords, event, forceAnimation) {
                        //console.log('swipeEnd', 'scope.carouselIndex', scope.carouselIndex);
                        $document.unbind('mouseup', documentMouseUpEvent);
                        pressed = false;

                        destination = offset;

                        var minMove = getAbsMoveTreshold(),
                            currentOffset = (scope.carouselIndex * containerWidth),
                            absMove = currentOffset - destination,
                            slidesMove = -Math[absMove>=0?'ceil':'floor'](absMove / containerWidth),
                            shouldMove = Math.abs(absMove) > minMove;

                        if ((slidesMove + scope.carouselIndex) >= slidesCount ) {
                            slidesMove = slidesCount - 1 - scope.carouselIndex;
                        }
                        if ((slidesMove + scope.carouselIndex) < 0) {
                            slidesMove = -scope.carouselIndex;
                        }
                        var moveOffset = shouldMove?slidesMove:0;

                        destination = (moveOffset + scope.carouselIndex) * containerWidth;
                        amplitude = destination - offset;
                        timestamp = Date.now();
                        if (forceAnimation) {
                            amplitude = offset - currentOffset;
                        }
                        requestAnimationFrame(autoScroll);

                        if (event) {
                            //event.preventDefault();
                            //event.stopPropagation();
                        }
                        return false;
                    }

                    iAttributes.$observe('rnCarouselSwipe', function(newValue, oldValue) {
                        // only bind swipe when it's not switched off
                        if(newValue !== 'false' && newValue !== 'off') {
                            $swipe.bind(carousel, {
                                start: swipeStart,
                                move: swipeMove,
                                end: swipeEnd,
                                cancel: function(event) {
                                  swipeEnd({}, event);
                                }
                            });
                        } else {
                            // unbind swipe when it's switched off
                            carousel.unbind();
                        }
                    });

                    // initialise first slide only if no binding
                    // if so, the binding will trigger the first init
                    if (!isIndexBound) {
                        goToSlide(scope.carouselIndex);
                    }

                    // detect supported CSS property
                    transformProperty = 'transform';
                    ['webkit', 'Moz', 'O', 'ms'].every(function (prefix) {
                        var e = prefix + 'Transform';
                        if (typeof document.body.style[e] !== 'undefined') {
                            transformProperty = e;
                            return false;
                        }
                        return true;
                    });

                    function onOrientationChange() {
                        updateContainerWidth();
                        goToSlide();
                    }

                    // handle orientation change
                    var winEl = angular.element($window);
                    winEl.bind('orientationchange', onOrientationChange);
                    winEl.bind('resize', onOrientationChange);

                    scope.$on('$destroy', function() {
                        $document.unbind('mouseup', documentMouseUpEvent);
                        winEl.unbind('orientationchange', onOrientationChange);
                        winEl.unbind('resize', onOrientationChange);
                    });

                };
            }
        };
    });


    leviev.directive('rnCarouselControls', function() {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      items: '=',
      index: '='
    },
    link: function(scope, element, attrs) {
      scope.prev = function() {
        scope.index--;
      };
      scope.next = function() {
        scope.index++;
      };
    },
    template: '<div class="rn-carousel-controls">' +
                '<span class="rn-carousel-control rn-carousel-control-prev" ng-click="prev()" ng-if="index > 0"></span>' +
                '<span class="rn-carousel-control rn-carousel-control-next" ng-click="next()" ng-if="index < items.length - 1"></span>' +
              '</div>'
  };
});


 leviev.filter('carouselSlice', function() {
        return function(collection, start, size) {
            if (angular.isArray(collection)) {
                return collection.slice(start, start + size);
            } else if (angular.isObject(collection)) {
                // dont try to slice collections :)
                return collection;
            }
        };
    });


leviev.directive('rnCarouselIndicators', function() {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      items: '=',
      index: '='
    },
    template: '<div class="rn-carousel-indicator" style="margin-top:0px">' +
                '<span ng-repeat="item in items" ng-click="$parent.index=$index" ng-class="{active: $index==$parent.index}"></span>' +
              '</div>'
  };
});
