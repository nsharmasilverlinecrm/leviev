'use strict';
angular.module('levievApp')
  .service('Salesforce', function Salesforce($rootScope, $q) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.searchProducts = function(params) {
      var deferred = $q.defer();
      var sfObj = {};
      sfObj.stone2 = {};
      sfObj.prod = {};
      sfObj.prod2 = {};
      sfObj.prod4 = {};
      sfObj.proxyObject = {};
      sfObj.proxyObject2 = {};
      sfObj.proxyObject.closeDate =  params.stone.createDateLow;
      sfObj.proxyObject2.closeDate = params.stone.createDateHigh;
      sfObj.prod4.ProductCode = params.jewelry.jewelryNumber;
      sfObj.FromClarity = params.stone.clarityLow;
      sfObj.ToClarity = params.stone.clarityHigh;
      sfObj.baseColor = params.stone.baseColor;
      sfObj.prod.Set__c = params.jewelry.setNumber;
      sfObj.BaseColorModBeg = params.stone.colorQualityLow;
      sfObj.BaseColorModEnd = params.stone.colorQualityHigh;
      sfObj.JewTypeList = params.jewelry.types;
      sfObj.prod4.Primary_Color__c = params.jewelry.colorPrimary;
      sfObj.prod4.Secondary_Color__c = params.jewelry.colorSecondary;
      sfObj.prod.Item_Location__c = params.jewelry.location;
      sfObj.prod.Total_Weight__c = params.stone.caratLow;
      sfObj.prod2.Total_Weight__c = params.stone.caratHigh;
      sfObj.prod.Reporting_Price__c = params.jewelry.priceLow;
      sfObj.prod2.Reporting_Price__c = params.jewelry.priceHigh;
      sfObj.prod.Family = params.jewelry.family;
      //sfObj.stone2.Certificate_Link__c = params.stone.certificate;
      sfObj.prod.Design_Type__c = params.jewelry.design;
      sfObj.shapeList = params.stone.shapes;
      sfObj.searchType = params.searchType;
      var p = JSON.stringify(sfObj);

      Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.SL_SearchLeviev_Remote.searchProducts}',p, function(result, event){
                if(event) {
                     $rootScope.$apply(function() {
                        deferred.resolve(result);
                    });

                } else { 
                    deferred.reject(result);
                }            
            });
            return deferred.promise;
    }
});
    /*
    Search Params: {"prod":{"Primary_Color__c":null,"Total_Weight__c":"0"},
            "prod2":{"Total_Weight__c":"100"},
            "FromClarity":"FL",
            "ToClarity":"I2",
            "BaseColorModBeg":"Ft",
            "BaseColorModEnd":"FDk",
            "JewTypeList":["Select Type(s)",
            "Bracelet","Brooch","Cufflink","Earring","Necklace","Other","Pendant","Ring","Tiara","Watch","Loose"],
            "ShapeList":["Select Type(s)","Bracelet","Brooch","Cufflink","Earring","Necklace","Other","Pendant","Ring","Tiara","Watch","Loose"],
            "SearchType":"Jewerly"}
            */
