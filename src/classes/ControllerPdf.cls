public class ControllerPdf {

	public Stone__c stone {get; set;}
	public Product2 product {get; set;}
	public String items {get; set;}
	public String entityType {get; set;}
	public String sortingType {get; set;}
	public String sortingDirection {get; set;}

	public Stone__c[] stones {get; set;}
	public Product2[] products {get; set;}

	public Lookuper__c lookup {get; set;}

	public Integer total {get; set;}

	public ControllerPdf() {}

	public ControllerPdf(ApexPages.StandardController controller) {}

	public String getFilename() {
		return entityType.equals('Product2') ? 'jewelries' : 'stones';
	}

	public SelectOption[] getEntityTypes() {
		SelectOption[] types = new SelectOption[]{};
		types.add(new SelectOption('Product2', 'Jewelry'));
		types.add(new SelectOption('Stone__c', 'Stone'));
		return types;
	}

	public SelectOption[] getSortingTypes() {
		SelectOption[] types = new SelectOption[]{};
		types.add(new SelectOption('name', 'Item name'));
		types.add(new SelectOption('code', 'Jewelry code'));
		types.add(new SelectOption('type', 'Jewelry type'));
		return types;
	}

	public SelectOption[] getSortingDirections() {
		SelectOption[] directions = new SelectOption[]{};
		directions.add(new SelectOption('ASC', 'ascending'));
		directions.add(new SelectOption('DESC', 'descending'));
		return directions;
	}

	public PageReference redirect() {
		al.SoqlBuilder builder = new al.SoqlBuilder();
		builder.selectAll();
		builder.fromx(entityType);
		builder.wherex(whereCondition(items));
		builder.orderByx(orderBy(sortingType));
		SObject[] objects = Database.query(builder.toSoql());
		if (objects.size() == 0) {
			HelperMessage.error('No objects found');
			return ApexPages.currentPage();
		}
		total = objects.size();
		if (entityType.equals('Product2')) {
			products = objects;
			stones = new Stone__c[]{};
		}
		else {
			stones = objects;
			products = new Product2[]{};
		}
		PageReference page = new PageReference('/apex/PdfContainerSet');
		page.setRedirect(false);
		return page;
	}

	private al.OrCondition whereCondition(String items) {
		al.OrCondition condition = new al.OrCondition();
		for (String item : items.split(',')) {
			condition.add(new al.FieldCondition('Name').equals(item.trim()));
		}
		return condition;
	}

	private al.OrderBy orderBy(String typex) {
		al.OrderBy orderx;
		if (typex.equals('code') && entityType.equals('Product2')) {
			orderx = new al.OrderBy('ProductCode');
		}
		else if (typex.equals('type') && entityType.equals('Product2')) {
			orderx = new al.OrderBy('Jewelry_Type__c');
		}
		else if (typex.equals('code') && entityType.equals('Stone__c')) {
			orderx = new al.OrderBy('Jewelry__r.ProductCode');
		}
		else if (typex.equals('type') && entityType.equals('Stone__c')) {
			orderx = new al.OrderBy('Jewelry__r.Jewelry_Type__c');
		}
		else {
			orderx = new al.OrderBy('Name');
		}
		return sortingDirection.equals('DESC') ? orderx.descending() : orderx.ascending();
	}

}