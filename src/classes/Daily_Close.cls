public with sharing class Daily_Close {

	public Map<Integer,String> MapMonth = new Map<Integer,String>();
	public Jewelry_Request__c JRDateBegObj {get; set;}
	public Jewelry_Request__c JRDateEndObj {get; set;}
	
	public Date JTBegDate = date.today();
	public Date JTEndDate = date.today();
	public Boolean isTest = false;
	
 	public Daily_Close() 
 	{
		MapMonth.put(12,'December');
		MapMonth.put(11,'November');
		MapMonth.put(10,'October');
		MapMonth.put(9,'September');
		MapMonth.put(8,'August');
		MapMonth.put(7,'July');
		MapMonth.put(6,'June');
		MapMonth.put(5,'May');
		MapMonth.put(4,'April');
		MapMonth.put(3,'March');
		MapMonth.put(2,'February');
		MapMonth.put(1,'January');
		
		JRDateBegObj = new Jewelry_Request__c(Due_Date__c  = date.today());
		JRDateEndObj = new Jewelry_Request__c(Due_Date__c  = date.today());
		
		try
		{
			if(System.currentPageReference().getParameters().get('bdate')!=null)
			{
				JTBegDate = date.valueOf(System.currentPageReference().getParameters().get('bdate'));
			}
			if(System.currentPageReference().getParameters().get('edate')!=null)
			{
				JTEndDate = date.valueOf(System.currentPageReference().getParameters().get('edate'));
			}
		}
		catch(Exception ex)
		{}
		
    }
	
	public String getCurSelDates()
	{
		return ''+MapMonth.get(JTBegDate.month())+' '+JTBegDate.day()+', '+JTBegDate.year()+' - '+MapMonth.get(JTEndDate.month())+' '+JTEndDate.day()+', '+JTEndDate.year();
	}
	
	public String getCurrentDate()
	{
		date cDate = date.today();
		return MapMonth.get(cDate.month())+' '+cDate.day()+', '+cDate.year();
	}
	
	


	public PageReference selDates()
    {
    	if(JRDateBegObj.Due_Date__c.daysBetween(JRDateEndObj.Due_Date__c)>7)
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'We do not allow report more than 7 days.'));
			return null;    		
    	}
    	
    	
    	PageReference pageRef = new PageReference('/apex/Report_Tracking_History?bdate='+JRDateBegObj.Due_Date__c+'&edate='+JRDateEndObj.Due_Date__c);   
        pageRef.setRedirect(true);   
        return pageRef;   
    }
    
    public List<TSObj> getTSListRep2()
	{
		//Time.newInstance(18, 30, 2, 20);
		//boolean isTest = false;
		
		datetime oDTBeg = datetime.newInstance(JTBegDate, Time.newInstance(0, 0, 0, 0));
		datetime oDTEnd = datetime.newInstance(JTEndDate, Time.newInstance(23, 59, 59, 50));
		
		System.debug('-------------oDTBeg'+String.valueOf(oDTBeg));
		System.debug('-------------oDTEnd'+String.valueOf(oDTEnd));
		User userObj = [Select u.Id, u.Division From User u where id=:UserInfo.getUserId() limit 1];
		Map<String,List<Jewerly_Tracking__c>> tmpMapJT = new Map<String,List<Jewerly_Tracking__c>>();
		List<TSObj> listRet = new List<TSObj>();
		List<String> mJTKeys = new List<String>();
		for(Jewerly_Tracking__c curObj:[Select check_out_user__c, j.check_out_user__r.FirstName, j.check_out_user__r.LastName, check_out_date__c, 
											check_in_user__c, j.check_in_user__r.FirstName, j.check_in_user__r.LastName, check_in_date__c, j.Requestor__c, 
											j.Reason_for_check_out__c, j.Product__c, j.Product__r.IsCheckOut__c, j.Product__r.Jewelry_Type__c, 
											j.Product__r.ProductCode, j.Name, j.Id, Requestor__r.FirstName, Requestor__r.LastName, Status__c,  
											j.Exception__c, j.Description__c 
										From Jewerly_Tracking__c j 
										where j.check_out_date__c>:oDTBeg and j.check_out_date__c<:oDTEnd and Division__c=:userObj.Division 
										order by j.Reason_for_check_out__c, Status__c, j.Product__r.ProductCode
										limit 2000])
		{
			if(!tmpMapJT.containsKey(curObj.Reason_for_check_out__c)){ 
				tmpMapJT.put(curObj.Reason_for_check_out__c, new List<Jewerly_Tracking__c>());
				mJTKeys.add(curObj.Reason_for_check_out__c);
			} 
			tmpMapJT.get(curObj.Reason_for_check_out__c).add(curObj);
		}
		
		if(tmpMapJT.size()>0)
		{
			Map<String,integer> tmpMapJTCounsIn;
			Map<String,integer> tmpMapJTCounsOut;
			integer iTotalCounIn;
			integer iTotalCounOut;
			
			for(String curKey:mJTKeys)
			{
				TSObj oTSObj = new TSObj();
				oTSObj.sType = curKey;
				oTSObj.listJTObj = convertToJTWrapperList(tmpMapJT.get(curKey));
				tmpMapJTCounsIn = new Map<String,integer>();
				tmpMapJTCounsOut = new Map<String,integer>();
				iTotalCounIn = 0;
				iTotalCounOut = 0;

				for(Jewerly_Tracking__c curObj:tmpMapJT.get(curKey))
				{

					if(curObj.Status__c=='Out')
					{
						iTotalCounOut++;
						if(tmpMapJTCounsOut.containsKey(curObj.Product__r.Jewelry_Type__c)) tmpMapJTCounsOut.put(curObj.Product__r.Jewelry_Type__c, tmpMapJTCounsOut.get(curObj.Product__r.Jewelry_Type__c)+1);
						else tmpMapJTCounsOut.put(curObj.Product__r.Jewelry_Type__c,1);
					}
					else
					{
						iTotalCounIn++;
						if(tmpMapJTCounsIn.containsKey(curObj.Product__r.Jewelry_Type__c)) tmpMapJTCounsIn.put(curObj.Product__r.Jewelry_Type__c, tmpMapJTCounsIn.get(curObj.Product__r.Jewelry_Type__c)+1);
						else tmpMapJTCounsIn.put(curObj.Product__r.Jewelry_Type__c,1);
					}
				}
				
				if((tmpMapJTCounsIn.size()>0)||(isTest))
				{
					List<String> mJTCountKeys = new List<String>();
					mJTCountKeys.addAll(tmpMapJTCounsIn.keySet());
					List<TSCountersObj> listTSCountersObj = new List<TSCountersObj>();
					mJTCountKeys.sort();
					for(String curCountKey:mJTCountKeys)
					{
						system.debug('curCountKey (In-2) ===' + curCountKey);
						TSCountersObj tmpTS = new TSCountersObj();
						tmpTS.sName = curCountKey;
						tmpTS.iVal = tmpMapJTCounsIn.get(curCountKey);
						listTSCountersObj.add(tmpTS);
					}
					oTSObj.listTSCountersInObj = listTSCountersObj;
				}
				if(tmpMapJTCounsOut.size()>0)
				{
					List<String> mJTCountKeys = new List<String>();
					mJTCountKeys.addAll(tmpMapJTCounsOut.keySet());
					List<TSCountersObj> listTSCountersObj = new List<TSCountersObj>();
					mJTCountKeys.sort();
					for(String curCountKey:mJTCountKeys)
					{
						system.debug('curCountKey (Out-2) ===' + curCountKey);
						TSCountersObj tmpTS = new TSCountersObj();
						tmpTS.sName = curCountKey;
						tmpTS.iVal = tmpMapJTCounsOut.get(curCountKey);
						listTSCountersObj.add(tmpTS);
					}
					oTSObj.listTSCountersOutObj = listTSCountersObj;
				}				
				
				oTSObj.TotalCounterIn = iTotalCounIn;
				oTSObj.TotalCounterOut = iTotalCounOut;
				
				
				listRet.add(oTSObj);
			}
		}
		//System.debug('RESULT OBJECT::::::::::::'+listRet);
		
		return listRet;
	}
    
	
	public List<TSObj> getTSListRep1()
	{
		datetime oDTBeg = datetime.newInstance(JTBegDate, Time.newInstance(0, 0, 0, 0));
		datetime oDTEnd = datetime.newInstance(JTEndDate, Time.newInstance(23, 59, 59, 50));
		
		User userObj = [Select u.Id, u.Division From User u where id=:UserInfo.getUserId() limit 1];
		Map<String,List<Jewerly_Tracking__c>> tmpMapJT = new Map<String,List<Jewerly_Tracking__c>>();
		List<TSObj> listRet = new List<TSObj>();
		List<String> mJTKeys = new List<String>();
		for(Jewerly_Tracking__c curObj:[SELECT check_out_user__c, j.check_out_user__r.FirstName, j.check_out_user__r.LastName, check_out_date__c, 
											check_in_user__c, j.check_in_user__r.FirstName, j.check_in_user__r.LastName, check_in_date__c, j.Requestor__c, 
											j.Reason_for_check_out__c, j.Product__c, j.Product__r.IsCheckOut__c, j.Product__r.Jewelry_Type__c, 
											j.Product__r.ProductCode, j.Name, j.Id, Requestor__r.FirstName, Requestor__r.LastName, Status__c,  
											j.Exception__c, j.Description__c 
										FROM Jewerly_Tracking__c j 
										WHERE j.check_out_date__c>:oDTBeg and j.check_out_date__c<:oDTEnd and Division__c=:userObj.Division 
										order by j.Reason_for_check_out__c, Status__c, j.Product__r.ProductCode
										limit 2000]) /*j.Product__r.IsCheckOut__c=true and*/
		{
			if(!tmpMapJT.containsKey(curObj.Reason_for_check_out__c)){ 
				tmpMapJT.put(curObj.Reason_for_check_out__c, new List<Jewerly_Tracking__c>());
				mJTKeys.add(curObj.Reason_for_check_out__c);
			} 
			tmpMapJT.get(curObj.Reason_for_check_out__c).add(curObj);
		}
		
		if(tmpMapJT.size()>0)
		{
			
			Map<String,integer> tmpMapJTCounsIn;
			Map<String,integer> tmpMapJTCounsOut;
			integer iTotalCounIn;
			integer iTotalCounOut;
			
			for(String curKey:mJTKeys)
			{
				TSObj oTSObj = new TSObj();
				oTSObj.sType = curKey;
				oTSObj.listJTObj = convertToJTWrapperList(tmpMapJT.get(curKey));
				tmpMapJTCounsIn = new Map<String,integer>();
				tmpMapJTCounsOut = new Map<String,integer>();
				iTotalCounIn = 0;
				iTotalCounOut = 0;

				for(Jewerly_Tracking__c curObj:tmpMapJT.get(curKey))
				{

					if(curObj.Status__c=='Out')
					{
						iTotalCounOut++;
						if(tmpMapJTCounsOut.containsKey(curObj.Product__r.Jewelry_Type__c)) tmpMapJTCounsOut.put(curObj.Product__r.Jewelry_Type__c, tmpMapJTCounsOut.get(curObj.Product__r.Jewelry_Type__c)+1);
						else tmpMapJTCounsOut.put(curObj.Product__r.Jewelry_Type__c,1);
					}
					else
					{
						iTotalCounIn++;
						if(tmpMapJTCounsIn.containsKey(curObj.Product__r.Jewelry_Type__c)) tmpMapJTCounsIn.put(curObj.Product__r.Jewelry_Type__c, tmpMapJTCounsIn.get(curObj.Product__r.Jewelry_Type__c)+1);
						else tmpMapJTCounsIn.put(curObj.Product__r.Jewelry_Type__c,1);
					}
				}
				
				if((tmpMapJTCounsIn.size()>0)||(isTest))
				{
					List<String> mJTCountKeys = new List<String>();
					mJTCountKeys.addAll(tmpMapJTCounsIn.keySet());
					List<TSCountersObj> listTSCountersObj = new List<TSCountersObj>();
					mJTCountKeys.sort();
					for(String curCountKey:mJTCountKeys)
					{
						system.debug('curCountKey (In-1) ===' + curCountKey);
						TSCountersObj tmpTS = new TSCountersObj();
						tmpTS.sName = curCountKey;
						tmpTS.iVal = tmpMapJTCounsIn.get(curCountKey);
						listTSCountersObj.add(tmpTS);
					}
					oTSObj.listTSCountersInObj = listTSCountersObj;
				}
				if(tmpMapJTCounsOut.size()>0) 
				{
					List<String> mJTCountKeys = new List<String>();
					mJTCountKeys.addAll(tmpMapJTCounsOut.keySet());
					List<TSCountersObj> listTSCountersObj = new List<TSCountersObj>();
					mJTCountKeys.sort();
					for(String curCountKey:mJTCountKeys)
					{
						system.debug('curCountKey (Out-1) ===' + curCountKey);
						TSCountersObj tmpTS = new TSCountersObj();
						tmpTS.sName = curCountKey;
						tmpTS.iVal = tmpMapJTCounsOut.get(curCountKey);
						listTSCountersObj.add(tmpTS);
					}
					oTSObj.listTSCountersOutObj = listTSCountersObj;
				}				
				
				oTSObj.TotalCounterIn = iTotalCounIn;
				oTSObj.TotalCounterOut = iTotalCounOut;
				
				
				listRet.add(oTSObj);
			}
		}
		//System.debug('RESULT OBJECT::::::::::::'+listRet);
		
		return listRet;
	}

    private List<JTWrapper> convertToJTWrapperList(List<Jewerly_Tracking__c> objList) {
    	List<JTWrapper> resultList = new List<JTWrapper>();
    	for(Jewerly_Tracking__c item : objList)
    		resultList.add(new JTWrapper(item));
    	return resultList;
    }
   

	public class JTWrapper {
		public Jewerly_Tracking__c obj {get;set;}
		public String datetimeOut {get;set;}
		public String datetimeIn  {get;set;}
		Datetime outDT;
		Datetime inDT;
		public JTWrapper(Jewerly_Tracking__c p_obj) {
			datetimeOut = '';
			datetimeIn  = '';
			obj = p_obj;
			outDT = obj.check_out_date__c;
			inDT  = obj.check_in_date__c;
			if (outDT != null)
				datetimeOut =/* normalize(outDT.month()) + '/' + normalize(outDT.day()) + '/' + outDT.year() + ' ' + */normalize(outDT.hour()) + ':' + normalize(outDT.minute());
			if (inDT != null)
				datetimeIn  =/* normalize(inDT.month()) + '/' + normalize(inDT.day()) + '/' + inDT.year() + ' ' + */normalize(inDT.hour()) + ':' + normalize(inDT.minute());
		}
		private String normalize(Integer p_number) {
			String result = '' + p_number;
			if(result.length() == 1) {
				result = '0' + result;
			}
			return result; 
		} 	
	}
	
	public class TSObj
    {
        public String sType {get; set;}
        public List<JTWrapper> listJTObj {get; set;}
        public List<TSCountersObj> listTSCountersInObj {get; set;}
        public List<TSCountersObj> listTSCountersOutObj {get; set;}
        public integer TotalCounterIn {get; set;}
        public integer TotalCounterOut {get; set;}

        public TSObj()
        {
        	sType = '';
        	listJTObj = new List<JTWrapper>();
        	listTSCountersInObj = new List<TSCountersObj>();
        	listTSCountersOutObj = new List<TSCountersObj>();
        	TotalCounterIn = 0;
        	TotalCounterOut = 0;
        }
    }
    
    public class TSCountersObj
    {
    	public String sName {get; set;}
    	public integer iVal {get; set;}
    	
    	public TSCountersObj()
    	{
    		sName = '';
    		iVal = 0;
    	}
    }
    
    public static testMethod void testDaily_Close() {
    	Boolean isTest = true;
    	
    	Test.startTest();
    	List <Jewerly_Tracking__c> JewTrack = new List<Jewerly_Tracking__c>();
        Jewerly_Tracking__c jt = new Jewerly_Tracking__c(Reason_for_check_out__c = 'Showcase', Division__c='New York', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50))); 
        JewTrack.add(jt);
   		insert JewTrack;
  		
  		System.debug('-------------InDB'+datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
     	
     	DateTime bdate = DateTime.now().addYears(-2);
		DateTime edate = DateTime.now();
		ApexPages.currentPage().getParameters().put('bdate', bdate.format('yyyy-MM-dd'));
		ApexPages.currentPage().getParameters().put('edate', edate.format('yyyy-MM-dd'));
		ApexPages.currentPage().getParameters().put('edate', '2010-13-13');
		Daily_Close dc = new Daily_Close();
		ApexPages.currentPage().getParameters().put('edate', edate.format('yyyy-MM-dd'));
		
     	
     	
		dc = new Daily_Close();
		date myDate = date.today();
		date newDate = mydate.addDays(7);
		dc.JRDateBegObj= new Jewelry_Request__c(Due_Date__c  = myDate);
		dc.JRDateEndObj = new Jewelry_Request__c(Due_Date__c  = newDate);
		
		
		String CurrentDate = dc.getCurrentDate();
		String CurSelDates = dc.getCurSelDates();
		List<TSObj> TSListRep = dc.getTSListRep1();
		List<TSObj> TSListRep2 = dc.getTSListRep2();

		
		PageReference selD = dc.selDates();
		TSObj tso = new TSObj();
		TSCountersObj tsco = new TSCountersObj();
   
        Test.stopTest();
    }
	

}