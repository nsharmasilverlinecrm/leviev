public class EmailClosedSales {
    public Opportunity Opportunity { get;set; }
    public List<OpportunityLineItem> products = new List<OpportunityLineItem>();
    public string apiUrl { get;set; }
    
    public EmailClosedSales ()
    {
        Opportunity = new Opportunity();
    }
    
    public string getserverUrl()
    {
        return apiUrl.substring(0, apiUrl.indexOf('/', 10));
    }
    
    public List<OpportunityLineItem> getproducts()
    {
        products = [
            Select Id,  
                UnitPrice,
                TotalPrice,
                CurrencyIsoCode,
                OpportunityId, 
                Opportunity.id, 
                Opportunity.Amount,
                Opportunity.AccountId, 
                Opportunity.Account.id, 
                Opportunity.Account.name, 
                PricebookEntryId, 
                PricebookEntry.Product2Id, 
                PricebookEntry.Product2.Reporting_Price__c, 
                PricebookEntry.Product2.CurrencyIsoCode, 
                PricebookEntry.Product2.name,
                PricebookEntry.Product2.description,
                PricebookEntry.Product2.Image_Source__c
            From OpportunityLineItem
            Where Opportunity.id =: Opportunity.id
        ];
        return products;
    }

    public string getOppId() {
        return string.valueOf(Opportunity.id);
    }
    
    public static testMethod void testThis() {
    	EmailClosedSales ecs = new EmailClosedSales();
    	ecs.apiUrl = '/apex/qwegfdjsgkl;jdfjghskd/fgh;';
    	ecs.getserverUrl();
    	List<OpportunityLineItem> p = ecs.getproducts();
    	ecs.getOppId();
    }
}