public with sharing class EventTrigger 
{
 /* Start - global variables*/
	list<Event> listOfEvent = new list<Event>(); // Store the new event records which has the subject as 'Home Appointment'.
	set<Id> setOfAccountId = new set<Id>(); // Store the AccountIds of new event records.
	map<Id, Account> mapAccountId_Account = new map<Id, Account>(); // Store the Id with Account records whose event is going to be created.
	 /* End - global variables*/
	
	/*
		@MethodName : onBeforeInsert 
		@param 	    : list of the Event
		@Description: This method will be called before insertion of Event records.
	*/ 
	public void onBeforeInsert(list<Event> listOfNewRecords)
	{
		//Iterating through the list of new records.
		for(Event obj : listOfNewRecords)
		{
			//check whether the subject of record is 'Home Appointment' or not and the object is 'Account', If yes then store in list.
			if(obj.Subject == 'Home Appointment' && String.valueOf(obj.WhatId).subString(0,3) == '001')
			{
				setOfAccountId.add(obj.WhatId);
				listOfEvent.add(obj);
			}
			
		}
		
		//If the set of AccountId is not empty then query the Account record to get the map of AccountId with Account.
		if(setOfAccountId.size() > 0)
		{
			for(Account objAccount : [Select Id, Phone, Name, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity 
			                          From Account 
			                          where Id IN : setOfAccountId ])
			{
				mapAccountId_Account.put(objAccount.Id , objAccount);
			}
		}
		
		//Iterating through list of event with the Subject 'Home Appointment' and WhatId is AccountId.
		for(Event obj : listOfEvent)
		{
			obj.Location = mapAccountId_Account.get(obj.WhatId).Name;
			obj.WhatId = mapAccountId_Account.get(obj.WhatId).Id;
			obj.Description = mapAccountId_Account.get(obj.whatId).BillingStreet + '\n ' + mapAccountId_Account.get(obj.whatId).BillingCity + '\n '
							 + mapAccountId_Account.get(obj.whatId).BillingState + '\n' +  mapAccountId_Account.get(obj.whatId).BillingCountry + '\n' 
							 + mapAccountId_Account.get(obj.whatId).BillingPostalCode + '\n' + mapAccountId_Account.get(obj.whatId).Phone;
			
		}
		
	}
	
	/*
		@MethodName : test_EventTriggerHandler 
		@param 	    : NA
		@Description: Test method for class EventTriggerHandler.
	*/ 
	@isTest()
	private static void test_EventTriggerHandler()
	{
		Account objAccount = new Account();
		objAccount.Name = 'Test';
		objAccount.BillingCity = 'test_city';
		objAccount.BillingCountry = 'test_country';
		objAccount.BillingPostalCode = '520063';
		objAccount.BillingState = 'test_state';
		objAccount.BillingStreet = 'test_street';
		objAccount.Phone = '9999999999';
		
		insert objAccount;
		
		Event objEvent = new Event();
		objEvent.WhatId = objAccount.Id;
		objEvent.Subject = 'Home Appointment';
		objEvent.DurationInMinutes = 60;
		objEvent.ActivityDateTime = dateTime.now() ;
		insert objEvent;
		
		Event objEvent1 = new Event();
		objEvent1 = [select Location , Description from Event where Id =: objEvent.Id];
		system.assertEquals(objEvent1.Location , 'Test');
		
		
	}	

}