public class HelperMessage {

	public static void info(String message) {
		message(ApexPages.Severity.INFO, message);
	}

	public static void error(String message) {
		message(ApexPages.Severity.ERROR, message);
	}

	private static void message(ApexPages.Severity severity, String message) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
	}

}