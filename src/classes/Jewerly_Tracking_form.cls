public with sharing class Jewerly_Tracking_form {

	String errorAlertMessage;
	public String getErrorAlertMessage() {return errorAlertMessage;}
	public void setErrorAlertMessage(String val) {errorAlertMessage = val;}
	
	public Jewerly_Tracking__c JTCheckOutObj = new Jewerly_Tracking__c();
	public Jewerly_Tracking__c JTCheckInObj = new Jewerly_Tracking__c();
	
	public Boolean isTest = false;
	
	public Boolean isCheckInClick {get;set;}
	
	public Jewerly_Tracking_form() {
		isCheckInClick = false;
	}

	public Boolean getIsShowDailyCloseReportButton() {
		Boolean result = true;

		datetime oDTBeg = datetime.newInstance(date.today(), Time.newInstance(0, 0, 0, 0));
		datetime oDTEnd = datetime.newInstance(date.today(), Time.newInstance(23, 59, 59, 50));
		
		User userObj = [Select u.Id, u.Division From User u where id=:UserInfo.getUserId() limit 1];
		List<Jewerly_Tracking__c> lstJT = [
			SELECT Status__c  
			FROM Jewerly_Tracking__c j 
			WHERE j.check_out_date__c>:oDTBeg 
					and j.check_out_date__c<:oDTEnd 
					and Division__c=:userObj.Division 
					and Status__c='Out']; 
		system.debug('lstJT.size===' + lstJT.size());
		if (lstJT.size() > 0)  result = false;
		
		return result;
	}
	
	public Jewerly_Tracking__c getJTCheckOutObj()
	{
		//System.currentPageReference().getParameters().get('did')
		if(System.currentPageReference().getParameters().get('req')!=null) JTCheckOutObj.Requestor__c = System.currentPageReference().getParameters().get('req');
		if(System.currentPageReference().getParameters().get('reas')!=null) JTCheckOutObj.Reason_for_check_out__c = System.currentPageReference().getParameters().get('reas');
		return JTCheckOutObj;
	}
	public Jewerly_Tracking__c getJTCheckInObj(){ return JTCheckInObj;}
	public void setJTCheckOutObj(Jewerly_Tracking__c inVal){ JTCheckOutObj = inVal;}
	public void setJTCheckInObj(Jewerly_Tracking__c inVal){ JTCheckInObj = inVal;}
	
	
	/*
	public Jewerly_Tracking_form(ApexPages.StandardController stdController)
	{
	}
	*/
	public PageReference goToTrackingStatus()
    {
    	isCheckInClick = false;
    	PageReference pageRef = new PageReference('/apex/Tracking_Status');   
        pageRef.setRedirect(true);   
        return pageRef;   
    }
	public PageReference goToTrackingHistory()
    {
    	isCheckInClick = false;
    	PageReference pageRef = new PageReference('/apex/Report_Tracking_History_SelDates');   
        pageRef.setRedirect(true);   
        return pageRef;   
    }
	public PageReference goToDailyClose() 
    {
    	isCheckInClick = false;
    	PageReference pageRef = new PageReference('/apex/Daily_Close');   
        pageRef.setRedirect(true);   
        return pageRef;   
    }
	public PageReference checkout()
    {
    	errorAlertMessage = '';
    	isCheckInClick = false;
    	Product2 prodObj = [Select IsCheckOut__c, ProductCode, JMS_Status__c, Item_Location__c, Id From Product2 where id=:JTCheckOutObj.Product__c limit 1];
    	system.debug('prodObj===' + prodObj);
    	User userObj = [Select u.Id, u.Division From User u where id=:UserInfo.getUserId() limit 1];
    	system.debug('userObj.Division===' + userObj.Division);
    	
    	if(prodObj==null || prodObj.IsCheckOut__c)
    	{
    		errorAlertMessage = prodObj.ProductCode + ' is already checked out.';
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
			return null;
    	}
    	if(userObj.Division != prodObj.Item_Location__c) {
			errorAlertMessage = prodObj.ProductCode + ' is located at another division.';
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
			return null;
		}
		
		if(prodObj.JMS_Status__c == 'SD' || prodObj.JMS_Status__c == 'RT') {
			errorAlertMessage = prodObj.ProductCode + ' has status ' + prodObj.JMS_Status__c +
				'. Items with such status can not be checked out.';
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
			return null;
		}    		
    	
    	prodObj.IsCheckOut__c = true;
    	update prodObj;
    	
    	
    	
    	JTCheckOutObj.check_out_date__c = datetime.now();
    	JTCheckOutObj.Division__c = userObj.Division;
    	system.debug('checkout::JTCheckOutObj.Division__c===' + JTCheckOutObj.Division__c);
    	//JTCheckOutObj.check_out_date__c = datetime.newInstanceGMT(date.today(), Time.newInstance(0, 0, 0, 0));
    	JTCheckOutObj.check_out_user__c = UserInfo.getUserId();
    	insert JTCheckOutObj;
    	
     	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, prodObj.ProductCode + ' checked out successfully.'));

    	Jewerly_Tracking__c newJT = new Jewerly_Tracking__c();
    	newJT.Division__c = userObj.Division;
    	newJT.Reason_for_check_out__c = JTCheckOutObj.Reason_for_check_out__c;
    	newJT.Requestor__c = JTCheckOutObj.Requestor__c;
    	JTCheckOutObj = newJT;
     	
     	/*
    	PageReference pageRef = new PageReference('/apex/Jewelry_Tracking_form?req='+JTCheckOutObj.Requestor__c+'&reas='+JTCheckOutObj.Reason_for_check_out__c);   
        pageRef.setRedirect(true);   
        return pageRef;
        */
        return null;   
    }

	public PageReference checkin()
    {
    	errorAlertMessage = '';
    	isCheckInClick = true;
    	Product2 prodObj = new Product2();
    	User userObj = [Select u.Id, u.Division From User u where id=:UserInfo.getUserId() limit 1];
    	
    	system.debug('checkin::JTCheckInObj.Product__c===' + JTCheckInObj.Product__c);
    	
    	if(JTCheckInObj != null && JTCheckInObj.Product__c != null)	
    		prodObj = [Select IsCheckOut__c, ProductCode, JMS_Status__c, Item_Location__c, Id From Product2 where id=:JTCheckInObj.Product__c limit 1];
    		
    	if(prodObj==null || prodObj.IsCheckOut__c==false)
    	{
    		for(Jewerly_Tracking__c JTObj : [
    			SELECT ID, CreatedDate
    			FROM Jewerly_Tracking__c 
    			WHERE Product__c=:JTCheckInObj.Product__c and check_in_user__c!=null and Division__c=:userObj.Division 
    			ORDER BY createddate desc
    			limit 1])
    			
    		{
    			if (JTObj.CreatedDate.date() == date.today()) {
    				errorAlertMessage = prodObj.ProductCode +  ' has already been checked in. You may not check it in again.';
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
					return null;
    			}
    		}

    		errorAlertMessage = prodObj.ProductCode + ' is not checked out.';
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
			return null;
    	}
    	else
    	{
    		if(userObj.Division != prodObj.Item_Location__c) {
    			errorAlertMessage = prodObj.ProductCode + ' is located at another division.';
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
				return null;
    		}
    		
    		if(prodObj.JMS_Status__c == 'SD' || prodObj.JMS_Status__c == 'RT') {
    			errorAlertMessage = prodObj.ProductCode + ' has status ' + prodObj.JMS_Status__c +
    				'. Items with such status can not be checked in.';
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
				return null;
    		}    		
    		
    		for(Jewerly_Tracking__c JTObj : [
    			SELECT ID, Division__c 
    			FROM Jewerly_Tracking__c 
    			WHERE Product__c=:JTCheckInObj.Product__c and check_in_user__c=null
    			ORDER BY createddate desc  
    			limit 1])
    		{
    			if(JTObj.Division__c != userObj.Division)
    			{
    				errorAlertMessage = prodObj.ProductCode + ' has been checked out in other division.';
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorAlertMessage));
					return null;
    			}
    		}
    	}
    	
    	
    	
    	
    	prodObj.IsCheckOut__c = false;
    	update prodObj;    	
    	
    	//UserInfo.getUserId()
    	
    	
    	for(Jewerly_Tracking__c JTObj : [Select check_in_user__c, check_in_date__c, Product__c, Id, Exception__c, Description__c From Jewerly_Tracking__c where Product__c=:JTCheckInObj.Product__c and check_in_user__c=null and Division__c=:userObj.Division limit 1])
    	{
	    	JTObj.check_in_date__c = datetime.now();
	    	JTObj.check_in_user__c = UserInfo.getUserId();
	    	
	    	JTObj.Exception__c = JTCheckInObj.Exception__c;
	    	JTObj.Description__c = JTCheckInObj.Description__c;
	    	update JTObj;
	    	
	    	JTCheckInObj = new Jewerly_Tracking__c();
	    	
	    	/*
	    	PageReference pageRef = new PageReference('/apex/Jewelry_Tracking_form');   
	        pageRef.setRedirect(true);   
	        return pageRef;
	        */
	        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, prodObj.ProductCode + ' checked in successfully.'));
    	}
    	//ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Item is not checked out'));
		 return null;    		
    	
    }

	
	
	public static testmethod void doTest(){
		Jewerly_Tracking__c jt_in = null;
		Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ'); 
   		insert pr;
   		
   		/*List <Jewerly_Tracking__c> JewTrack = new List<Jewerly_Tracking__c>();
        Jewerly_Tracking__c jt2 = new Jewerly_Tracking__c(Product__c=pr.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
        JewTrack.add(jt2);
   		insert JewTrack;*/
   		
   		User us = new User(Username='levievtest1234@t.com', LastName='levievtest1234', Email='levievtest1234@t.com', Alias='QQQ', 
   		TimeZoneSidKey='GMT', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', ProfileId='00e30000000es7bAAA', LanguageLocaleKey='en_US', CommunityNickname = 'QQQ'); 
   		insert us;
		
		/*List<Jewerly_Tracking__c> jt_in_0 = [SELECT id, name, Product__c, Exception__c, Description__c FROM Jewerly_Tracking__c WHERE Product__c!=null AND Product__r.IsCheckOut__c=true LIMIT 1];
		if(jt_in_0 != null && jt_in_0.size() > 0)jt_in = jt_in_0.get(0);
				 
		System.debug('-------------jt_in_0='+jt_in_0.get(0)); */
		
		Product2 prod_out = [Select IsCheckOut__c, Id From Product2 where IsCheckOut__c=false limit 1];
		
		list<Product2> prods = new list<Product2>();
		
		prods.add(new Product2(Name = 'Test1',IsCheckOut__c = true));
		prods.add(new Product2(Name = 'Test2', IsCheckOut__c = false));
		insert prods;
		
		/*Jewerly_Tracking__c jt_out = new Jewerly_Tracking__c(Division__c='New York');
		jt_out.Product__c = prod_out.Id;
		insert jt_out;*/
		
		/*List<Jewerly_Tracking__c> jt_in_0 = [SELECT id, name, Product__c, Exception__c, Description__c FROM Jewerly_Tracking__c WHERE Product__c!=null AND Product__r.IsCheckOut__c=true LIMIT 1];
		
		
		Jewerly_Tracking__c jt_out1 = new Jewerly_Tracking__c(Division__c='New York', Product__c = prods[0].Id, Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
		insert jt_out1;
		Jewerly_Tracking__c jt_out2 = new Jewerly_Tracking__c(Division__c='New York', Product__c = prods[1].Id, Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
		insert jt_out2;*/
		
		List <Jewerly_Tracking__c> JewTrack = new List<Jewerly_Tracking__c>();
        Jewerly_Tracking__c jt = new Jewerly_Tracking__c(check_in_user__c = us.id,Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
        JewTrack.add(jt);
   		Jewerly_Tracking__c jt1 = new Jewerly_Tracking__c(Reason_for_check_out__c = 'Showcase', Division__c='New York', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
        JewTrack.add(jt1);
        Jewerly_Tracking__c jt2 = new Jewerly_Tracking__c(Product__c=pr.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
        JewTrack.add(jt2);
        
   		insert JewTrack;		
		
	
//		Jewerly_Tracking__c jt_out = [SELECT id, name, Product__c, Exception__c, Description__c FROM Jewerly_Tracking__c WHERE Product__c!=null AND Product__r.IsCheckOut__c= false LIMIT 1].get(0);
//		Jewerly_Tracking__c jt_out1 = [SELECT id, name, Product__c, Exception__c, Description__c FROM Jewerly_Tracking__c WHERE Product__c!=null AND Product__r.IsCheckOut__c= true LIMIT 1].get(0);





		Product2 pr_out1 = new Product2(name='qqq1', IsCheckOut__c=true);
		Product2 pr_out2 = new Product2(name='qqq2', IsCheckOut__c=false, Item_Location__c='New York');
		Product2 pr_out3 = new Product2(name='qqq3', IsCheckOut__c=false, Item_Location__c='Dubai', JMS_Status__c='SD');
		Product2 pr_out4 = new Product2(name='qqq3', IsCheckOut__c=false, Item_Location__c='Dubai', JMS_Status__c='SS');
		insert pr_out1;
		insert pr_out2;
		insert pr_out3;
		insert pr_out4;
		Jewerly_Tracking__c jt_out1 = new Jewerly_Tracking__c(Product__c=pr_out1.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
		Jewerly_Tracking__c jt_out2 = new Jewerly_Tracking__c(Product__c=pr_out2.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
		Jewerly_Tracking__c jt_out3 = new Jewerly_Tracking__c(Product__c=pr_out3.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
		Jewerly_Tracking__c jt_out4 = new Jewerly_Tracking__c(Product__c=pr_out4.Id, Division__c='New York', Reason_for_check_out__c = 'Showcase', check_out_date__c=datetime.newInstance(date.today(), Time.newInstance(15, 59, 59, 50)));
//		insert jt_out1;
//		insert jt_out2;
//		insert jt_out3;
//		insert jt_out4;

		








		Test.startTest();
			Jewerly_Tracking_form cls = new Jewerly_Tracking_form();
			
			cls.getErrorAlertMessage();
			cls.setErrorAlertMessage('test');
			cls.getJTCheckInObj();
			cls.getJTCheckOutObj();
			cls.goToTrackingStatus();
			cls.goToTrackingHistory();
			cls.goToDailyClose();
			cls.getIsShowDailyCloseReportButton();
			
			/*cls.setJTCheckInObj(jt_in);
			cls.checkin();*/
			cls.setJTCheckInObj(JewTrack[0]);
			cls.checkin();
			cls.setJTCheckInObj(JewTrack[1]);
			cls.checkin();
			cls.setJTCheckInObj(JewTrack[2]);
			cls.checkin();
			
			cls.setJTCheckOutObj(JewTrack[0]);
			cls.checkin();
			cls.setJTCheckOutObj(JewTrack[1]);
			cls.checkin();
			cls.setJTCheckOutObj(JewTrack[2]);
			cls.checkin();
			
			/*cls.setJTCheckOutObj(jt_out);
			cls.checkout();
			cls.setJTCheckOutObj(jt_out1);
			cls.checkout();
			cls.setJTCheckOutObj(jt_out2);
			cls.checkout();*/
			
			prods[0].JMS_Status__c = 'SD';
			prods[1].JMS_Status__c = 'SD';
			update prods;

//			cls.setJTCheckOutObj(jt_out);
//			cls.checkout();
			cls.setJTCheckOutObj(jt_out1);
			cls.checkout();
			cls.setJTCheckOutObj(jt_out2);
			cls.checkout();
			cls.setJTCheckOutObj(jt_out3);
			cls.checkout();
			cls.setJTCheckOutObj(jt_out4);
			cls.checkout();
			 
			
		Test.stopTest();
	}
}