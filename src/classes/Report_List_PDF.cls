public with sharing class Report_List_PDF {
	public User currUserObj = null;
	
	public Report_List_PDF() {
		
	}

    public User getCurUserObj() {
    	if (currUserObj == null) 
    		currUserObj = [Select ID,Division from User where Id =:UserInfo.getUserId() limit 1];
    	return currUserObj;
    }
	
	public List<RowClass> getDataList() {
			User loggedUser = getCurUserObj();
		List<RowClass> resultList = new List<RowClass>();
		/*
		Set<Id> jewelryListIds = new Set<Id>();
		for(Jewelry_List__c item : [SELECT Id FROM Jewelry_List__c WHERE Active__c=true ORDER BY Name]) {
			jewelryListIds.add(item.Id);
		}
		*/
		Map<String, List<Jewelry_List_Entry__c>> Map_window_JewelryListEntryList = new Map<String, List<Jewelry_List_Entry__c>>();
		for(Jewelry_List_Entry__c item : [
			SELECT Id, Name, Jewelry_List__r.Name, Jewelry__r.Name, Price__c, Jewelry__r.Description, Jewelry__r.Item_Number__c,
					Jewelry__r.Singapore_Retail_Price__c, Jewelry__r.Levant_Suggested_Retail__c, Jewelry__r.GBP_Search_Price__c, 
					Jewelry__r.Moscow_Retail_Price__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Discount_Code__c, Jewelry__r.Technical_Description__c 
			FROM Jewelry_List_Entry__c 
			WHERE Jewelry_List__r.Active__c=true ORDER BY Jewelry_List__r.Name, Name limit 20])
		{
			if (!Map_window_JewelryListEntryList.containsKey(item.Jewelry_List__r.Name))
				Map_window_JewelryListEntryList.put(item.Jewelry_List__r.Name, new List<Jewelry_List_Entry__c>());
			Map_window_JewelryListEntryList.get(item.Jewelry_List__r.Name).add(item);
		} 
		
		List<String> windowNameSortList = new List<String>();
		windowNameSortList.addAll(Map_window_JewelryListEntryList.keySet());
		windowNameSortList.sort();
		Map<String, Integer> Map_jType_count = new Map<String, Integer>();
		Integer tmpCount = 0;
		for(String winName : windowNameSortList) {
			RowClass newRC = new RowClass();
			newRC.windowName = '- ' + winName;
			for(Jewelry_List_Entry__c item : Map_window_JewelryListEntryList.get(winName)) {
				ItemClass newIC = new ItemClass();
				newIC.itemName = item.Name;

		    	if (loggedUser.Division=='Singapore') { newIC.price = item.Jewelry__r.Singapore_Retail_Price__c;  }
		    	if (loggedUser.Division=='Dubai')     { newIC.price = item.Jewelry__r.Levant_Suggested_Retail__c; }
		    	if (loggedUser.Division=='London')    { newIC.price = item.Jewelry__r.GBP_Search_Price__c;        }
		    	if (loggedUser.Division=='Moscow')    { newIC.price = item.Jewelry__r.Moscow_Retail_Price__c;     }

		  		if (loggedUser.Division == 'Singapore') newIC.priceCurrency = 'SGD';
		  		if (loggedUser.Division == 'New York')  newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Moscow')    newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Dubai')     newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Europe')    newIC.priceCurrency = '€';
		  		if (loggedUser.Division == 'London')    newIC.priceCurrency = '£';

				//newIC.description   = item.Jewelry__r.Description;
				newIC.description   = item.Jewelry__r.Technical_Description__c;
				newIC.image         = item.Jewelry__r.Item_Number__c;
				newIC.j_type        = item.Jewelry__r.Jewelry_Type__c;
				newIC.discountCode  = item.Jewelry__r.Discount_Code__c;
				
				if(!Map_jType_count.containsKey(newIC.j_type))
					Map_jType_count.put(newIC.j_type, 0);
				tmpCount = Map_jType_count.get(newIC.j_type);
				tmpCount++;
				Map_jType_count.put(newIC.j_type, tmpCount);
				
				newRC.itemList.add(newIC);
				newRC.typeList = getTypeClassList(Map_jType_count);
			}
			
			
			for(TypeClass item : newRC.typeList) {
				newRC.totalCount += item.count;
			}
			
			resultList.add(newRC);
		}
		return resultList;
	}
	
	private List<TypeClass> getTypeClassList(Map<String, Integer> Map_jType_count) {
		List<TypeClass> resultList = new List<TypeClass>();
		List<String> typeSortList = new List<String>();
		typeSortList.addAll(Map_jType_count.keySet());
		typeSortList.sort();
		for(String item : typeSortList) {
			if(item!=null)
			{
				TypeClass newTC = new TypeClass();
				newTC.j_type = item.toUpperCase();
				newTC.count  = Map_jType_count.get(item);
				resultList.add(newTC);
			}
		}
		return resultList;
	}

	public class RowClass {
		public String windowName {get;set;}
		public List<ItemClass> itemList {get;set;}
		public List<TypeClass> typeList {get;set;}
		public Integer totalCount       {get;set;}
		public RowClass() {
			itemList = new List<ItemClass>();
			typeList = new List<TypeClass>();
			totalCount = 0;
		}
	}
	
	public class ItemClass {
		public String itemName      {get;set;}
		public Double price         {get;set;}
		public String priceCurrency {get;set;}
		public String description   {get;set;}
		public String image         {get;set;}
		public String j_type        {get;set;}
		public String discountCode  {get;set;}
	}
	
	public class TypeClass {
		public String j_type {get;set;}
		public Integer count {get;set;}
	}
	
	public static testMethod void testThis() {
		
		Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ'); 
   		insert pr;

        Test.startTest();
        
        Jewelry_List__c testObj = new Jewelry_List__c(Name='test Name');
        testObj.Active__c = false;
        insert testObj;
        
        Jewelry_List__c testObj2 = new Jewelry_List__c(Name='test Name - 3');
        testObj2.Active__c = true;
        insert testObj2;
        
        Jewelry_List_Entry__c le1 = new Jewelry_List_Entry__c();
        le1.Jewelry_List__c = testObj.ID;
        le1.Jewelry__c = pr.ID;
        insert le1;
        
        Jewelry_List_Entry__c le2 = new Jewelry_List_Entry__c();
        le2.Jewelry_List__c = testObj2.ID;
        le2.Jewelry__c = pr.ID;
        insert le2;
        
        
        List<Jewelry_List__c> listJL = new List<Jewelry_List__c>();
        listJL.add(testObj);
        listJL.add(testObj2);
        
		//ApexPages.currentPage().getParameters().put('jlist', testObj2.Id);
		//ApexPages.currentPage().getParameters().put('jlists', testObj.Id+':'+testObj2.Id);
		Report_List_PDF con = new Report_List_PDF();
		con.getDataList();
	} 	
	
}