public with sharing class Report_WindowList_PDF {
	public User currUserObj = null;
	private String jList;
	private List<RowClass> dataList = new List<RowClass>();
	public List<Jewelry_List__c> selectedJLists = new List<Jewelry_List__c>();
	public boolean isTest = false;
	
	
	
	public Report_WindowList_PDF() {
		jList = System.currentPageReference().getParameters().get('jlist');
		String jLists = System.currentPageReference().getParameters().get('jlists');
		if (jList == null && jLists != null) {
			List<String> listIds = jLists.split(':');
			this.selectedJLists = [SELECT Id FROM Jewelry_List__c WHERE Id in :listIds];
		}
		
	}
	
	public Report_WindowList_PDF(ApexPages.StandardSetController controller) {
		jList = System.currentPageReference().getParameters().get('jlist');
		this.selectedJLists = (list<Jewelry_List__c>) controller.getSelected();
		String jLists = System.currentPageReference().getParameters().get('jlists');
		if (jList == null && jLists != null) {
			List<String> listIds = jLists.split(':');
			this.selectedJLists = [SELECT Id FROM Jewelry_List__c WHERE Id in :listIds];
		}
		system.debug('selectedJLists.size===' + selectedJLists.size());
	}

	public Pagereference doViewPrintableVersion() {
		if (jList != null && jList != '') {
			Pagereference pgr = new Pagereference('/apex/Report_WindowList_Printable?jlist=' + jList);
			pgr.setRedirect(true);
			return pgr;
		} else {
			String sList = '';
			for(Jewelry_List__c item : this.selectedJLists) { sList += ':' + item.Id; }
			if (sList!='')
				sList = sList.substring(1);
			Pagereference pgr = new Pagereference('/apex/Report_WindowList_Printable?jlists=' + sList);
			pgr.setRedirect(true);
			return pgr; 
		}
	}
	
    public User getCurUserObj() {
    	if (currUserObj == null) 
    		currUserObj = [Select ID,Division from User where Id =:UserInfo.getUserId() limit 1];
    	return currUserObj;
    }
	
	public Double getGrandTotalPrice() {
		Double grandTotal = 0;
		for(RowClass item : dataList)
			for(ItemClass item2 : item.itemList)
				grandTotal += item2.price;
		return grandTotal;
	}

	public Integer getGrandTotalCount() {
		Integer grandTotal = 0;
		for(RowClass item : dataList)
			for(ItemClass item2 : item.itemList)
				grandTotal++;
		return grandTotal;
	}

	public String getGrandTotalCurrency() {
		String result = '';
		if (dataList.size() > 0 && dataList[0].itemList.size() > 0) {
			result = dataList[0].itemList[0].priceCurrency;
		}
		return result;
	}
	
	public List<RowClass> getDataList() {
		User loggedUser = getCurUserObj();
		dataList = new List<RowClass>();
		Map<String, List<Jewelry_List_Entry__c>> Map_window_JewelryListEntryList = new Map<String, List<Jewelry_List_Entry__c>>();
		
		
		// Jewelry_List__r.Active__c=true AND
		if (jList != null && jList != '') {
			for(Jewelry_List_Entry__c item : [
				SELECT Id, Name, Jewelry_List__r.Name, Jewelry__r.Name, Price__c, Jewelry__r.Description, Jewelry__r.Item_Number__c,
						Jewelry__r.Singapore_Retail_Price__c, Jewelry__r.Levant_Suggested_Retail__c, Jewelry__r.GBP_Search_Price__c, 
						Jewelry__r.Moscow_Retail_Price__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Discount_Code__c, Jewelry__r.Technical_Description__c 
				FROM Jewelry_List_Entry__c 
				WHERE  Jewelry_List__c=:jList ORDER BY Jewelry_List__r.Name, Name])
			{
				if(!Map_window_JewelryListEntryList.containsKey(item.Jewelry_List__r.Name))
					Map_window_JewelryListEntryList.put(item.Jewelry_List__r.Name, new List<Jewelry_List_Entry__c>());
				Map_window_JewelryListEntryList.get(item.Jewelry_List__r.Name).add(item);
			}
		} else {
			for(Jewelry_List_Entry__c item : [
				SELECT Id, Name, Jewelry_List__r.Name, Jewelry__r.Name, Price__c, Jewelry__r.Description, Jewelry__r.Item_Number__c,
						Jewelry__r.Singapore_Retail_Price__c, Jewelry__r.Levant_Suggested_Retail__c, Jewelry__r.GBP_Search_Price__c, 
						Jewelry__r.Moscow_Retail_Price__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Discount_Code__c, Jewelry__r.Technical_Description__c 
				FROM Jewelry_List_Entry__c 
				WHERE Jewelry_List__c in :selectedJLists ORDER BY Jewelry_List__r.Name, Name])
			{
				if(!Map_window_JewelryListEntryList.containsKey(item.Jewelry_List__r.Name))
					Map_window_JewelryListEntryList.put(item.Jewelry_List__r.Name, new List<Jewelry_List_Entry__c>());
				Map_window_JewelryListEntryList.get(item.Jewelry_List__r.Name).add(item);
			}
		}
		
		
		
		List<String> windowNameSortList = new List<String>();
		windowNameSortList.addAll(Map_window_JewelryListEntryList.keySet());
		windowNameSortList.sort();
		Map<String, Integer> Map_jType_count = new Map<String, Integer>();
		Map<String, Double> Map_jType_price = new Map<String, Double>();
		//Integer tmpCount = 0;
		for(String winName : windowNameSortList) {
			RowClass newRC = new RowClass();
			newRC.windowName = winName;
			Map_jType_count.clear();
			Map_jType_price.clear();
			for(Jewelry_List_Entry__c item : Map_window_JewelryListEntryList.get(winName)) {
				if(item.Jewelry__c == null){
				/*	ItemClass newIC = new ItemClass();
					newIC.itemName = item.Name;
					newRC.itemList.add(newIC);*/
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is Jewelry List Entry with empty Jewelry field. It is not included in report.'));
					
					continue;
				}
				
				ItemClass newIC = new ItemClass();
				newIC.itemName = item.Name;
					
				if(item.Jewelry__r != null){
			    	if (loggedUser.Division=='Singapore') { newIC.price = item.Jewelry__r.Singapore_Retail_Price__c;  }
			    	if (loggedUser.Division=='Dubai')     { newIC.price = item.Jewelry__r.Levant_Suggested_Retail__c; }
			    	if (loggedUser.Division=='London')    { newIC.price = item.Jewelry__r.GBP_Search_Price__c;        }
			    	if (loggedUser.Division=='Moscow')    { newIC.price = item.Jewelry__r.Moscow_Retail_Price__c;     }
				}

		  		if (loggedUser.Division == 'Singapore') newIC.priceCurrency = 'SGD';
		  		if (loggedUser.Division == 'New York')  newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Moscow')    newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Dubai')     newIC.priceCurrency = '$';
		  		if (loggedUser.Division == 'Europe')    newIC.priceCurrency = '€';
		  		if (loggedUser.Division == 'London')    newIC.priceCurrency = '£';

				//newIC.description   = item.Jewelry__r.Description;
				if(item.Jewelry__r != null){
					newIC.description   = item.Jewelry__r.Technical_Description__c;
					newIC.image         = item.Jewelry__r.Item_Number__c;
					newIC.j_type        = item.Jewelry__r.Jewelry_Type__c;
					newIC.discountCode  = item.Jewelry__r.Discount_Code__c;
				}
				if(!Map_jType_count.containsKey(newIC.j_type))
					Map_jType_count.put(newIC.j_type, 0);
				Map_jType_count.put(newIC.j_type, Map_jType_count.get(newIC.j_type) + 1);

				if(!Map_jType_price.containsKey(newIC.j_type))
					Map_jType_price.put(newIC.j_type, 0);
				Map_jType_price.put(newIC.j_type, Map_jType_price.get(newIC.j_type) + newIC.price);
				
				system.debug('Map_jType_count===' + Map_jType_count);
				
				newRC.itemList.add(newIC);
				newRC.typeList = getTypeClassList(Map_jType_count, Map_jType_price);
			}
			
			
			for(TypeClass item : newRC.typeList) {
				newRC.totalCount += item.count;
				newRC.totalPrice += item.price;
			}
			
			dataList.add(newRC);
			//system.debug('newRC.itemList===' + newRC.itemList);
			//system.debug('newRC.typeList===' + newRC.typeList);
		}
		
		if (dataList.size() > 0)
			dataList[dataList.size() - 1].isLast = true;
		return dataList;
	}
	
	private List<TypeClass> getTypeClassList(Map<String, Integer> Map_jType_count, Map<String, Double> Map_jType_price) {
		List<TypeClass> resultList = new List<TypeClass>();
		List<String> typeSortList = new List<String>();
		typeSortList.addAll(Map_jType_count.keySet());
		typeSortList.sort();
		for(String item : typeSortList) {
			TypeClass newTC = new TypeClass();
			newTC.j_type = (item==null) ? '' : item.toUpperCase();
			newTC.count  = Map_jType_count.get(item);
			newTC.price  = Map_jType_price.get(item);
			resultList.add(newTC);
		}
		return resultList;
	}

	public class RowClass {
		public String windowName {get;set;}
		public List<ItemClass> itemList {get;set;}
		public List<TypeClass> typeList {get;set;}
		public Integer totalCount       {get;set;}
		public Double  totalPrice       {get;set;}
		public Boolean isLast           {get;set;}
		public RowClass() {
			itemList = new List<ItemClass>();
			typeList = new List<TypeClass>();
			totalCount = 0;
			totalPrice = 0.0;
			isLast = false;
		}
	}
	
	public class ItemClass {
		public String itemName      {get;set;}
		public Double price         {get;set;}
		public String priceCurrency {get;set;}
		public String description   {get;set;}
		public String image         {get;set;}
		public String j_type        {get;set;}
		public String discountCode  {get;set;}
	}
	
	public class TypeClass {
		public String j_type {get;set;}
		public Integer count {get;set;}
		public double  price {get;set;}
	}
	
	public static testMethod void testThis() {
		
Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ'); 
   		insert pr;

        Test.startTest();
        
        Jewelry_List__c testObj = new Jewelry_List__c(Name='test Name');
        testObj.Active__c = false;
        insert testObj;
        
        Jewelry_List__c testObj2 = new Jewelry_List__c(Name='test Name - 3');
        testObj2.Active__c = true;
        insert testObj2;
        
        Jewelry_List_Entry__c le1 = new Jewelry_List_Entry__c();
        le1.Jewelry_List__c = testObj.ID;
        le1.Jewelry__c = pr.ID;
        insert le1;
        
        Jewelry_List_Entry__c le2 = new Jewelry_List_Entry__c();
        le2.Jewelry_List__c = testObj2.ID;
        le2.Jewelry__c = pr.ID;
        insert le2;
        
        
        List<Jewelry_List__c> listJL = new List<Jewelry_List__c>();
        listJL.add(testObj);
        listJL.add(testObj2);
        
        Report_WindowList_PDF con3 = new Report_WindowList_PDF();
        con3.doViewPrintableVersion();
		
		ApexPages.currentPage().getParameters().put('jlist', testObj2.Id);
		ApexPages.currentPage().getParameters().put('jlists', testObj.Id+':'+testObj2.Id);
		Report_WindowList_PDF con = new Report_WindowList_PDF();
		//con.getDataList();
		
		ApexPages.currentPage().getParameters().put('jlist', testObj2.Id);
		ApexPages.currentPage().getParameters().put('jlists', testObj.Id+':'+testObj2.Id);
		ApexPages.StandardSetController sc = new ApexPages.StandardSetController(listJL);
		Report_WindowList_PDF con2 = new Report_WindowList_PDF(sc);
		
		con2.getGrandTotalCurrency();
		con2.doViewPrintableVersion();
	} 
}