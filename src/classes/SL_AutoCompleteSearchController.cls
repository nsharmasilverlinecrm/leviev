global class SL_AutoCompleteSearchController 
{
	static List<String> lstObj = new List<String>();
	static List<sObject> lstsObjectStone = new List<sObject>();
    static List<sObject> lstsObjectProduct = new List<sObject>();
    static List<sObject> lstsObjectStoneAndProduct = new List<sObject>();

    @RemoteAction
    global static SObject[] findSObjects(string obj, string qry) {
    	System.debug('---test----------');
    	lstObj = obj.split(',');
    
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        //Check For Object1
        Schema.SObjectType sot = gd.get(lstObj[0]);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        //Check For Object2
        Schema.SObjectType sot2 = gd.get(lstObj[1]);
	    if (sot2 == null) {
	        // Object name not valid
            return null;
        }
        // create the filter text
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        
        //1 - begin building the dynamic soql query
        String soql = 'select id, Name';
        // add the object and filter by name to the soql
        soql += ' from ' + lstObj[0] + ' where name' + filter;
        // add the filter by additional fields to the soql
        soql += ' order by Name limit 5';
        
        //2 - begin building the dynamic soql query
        String soql2 = 'select id, Name';
        // add the object and filter by name to the soql
        soql2 += ' from ' + lstObj[1] + ' where name' + filter;
        // add the filter by additional fields to the soql
        soql2 += ' order by Name limit 5';
        /* List of sObjects */
        try {
        	/* Adding result in list of sObjects */
            lstsObjectStone = Database.query(soql);
            lstsObjectProduct = Database.query(soql2);
            lstsObjectStoneAndProduct.addAll(lstsObjectStone);
            lstsObjectStoneAndProduct.addAll(lstsObjectProduct);
        }
        catch (QueryException e) {
            return null;
        }
        return lstsObjectStoneAndProduct;
   }
   
     @isTest
	 private static void test_SL_AutoCompleteSearchController()
	 {
	 	// Insert Stone
        Stone__c objStone = new Stone__c();
        objStone.Name = 'Test Stone Name';
        insert objStone;
        
        // Insert Jewelry
        Product2 objProduct2 = new Product2();
        objProduct2.Name = 'Test Product';
        objProduct2.ProductCode = 'Test Code';
        objProduct2.Family = 'Pendant';
        objProduct2.Reporting_Price__c = 10000;
        objProduct2.Total_Cost__c = 10000;
        objProduct2.Market_Price__c = 10000;
        objProduct2.Insurance_Price__c = 10000;
        insert objProduct2;
	    
	    SL_AutoCompleteSearchController objTest = new SL_AutoCompleteSearchController();
	    
	    findSObjects('Stone__c','test');
	    
	    findSObjects('Product2','Test\'s');
	 }
   
}