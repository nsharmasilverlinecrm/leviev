/**
* @ClassName    : SL_BatchScheduleCaller_RapNetPriceUpdate 
* @JIRATicket   : LEVIEV-42
* @CreatedOn    : 26/July/12
* @ModifiedBy   : Vishal
* @Description  : This class will schedule the class SL_BatchSchedule_RapNetPriceUpdate, which in turn invokes the batch. 
*/


/**
@Developer Name							: Vishal
Percentage of best practices followed	: 100%
No of SOQL queries used					: 0
No of collections used					: 0
Exception Handling implemented			: Yes
Coding standards followed				: Yes
Naming conventions followed				: Yes
Third party integrations				: No
Maximum of No of records tested with	: 
Customer Approval						: 1/Aug/2012
Last Modified Date						: 26/July/2012
Approved by				  				: a

*/
public with sharing class SL_BatchScheduleCaller_RapNetPriceUpdate 
{ 
	
	//	Default constructor
	public SL_BatchScheduleCaller_RapNetPriceUpdate()
	{
		//	Setting the cronExpression.
		string strCronExpression = '0 0 10 ? * SUN';
		
		//	Scheduling the Class SL_BatchSchedule_RapNetPriceUpdate, which runs the batch weekly.
		system.schedule('Updates Price of Stones depending on RapNet', strCronExpression, new SL_BatchSchedule_RapNetPriceUpdate()); 
	}

}