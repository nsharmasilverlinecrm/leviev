/**
* @ClassName    : SL_BatchSchedule_RapNetPriceUpdate 
* @JIRATicket   : LEVIEV-42
* @CreatedOn    : 26/July/12
* @ModifiedBy   : Vishal
* @Description  : This class implements the Global interface Schedulable and calls the batch class SL_RapNetPriceUpdate from it. 
*/


/**
@Developer Name							: Vishal
Percentage of best practices followed	: 100%
No of SOQL queries used					: 0
No of collections used					: 0
Exception Handling implemented			: Yes
Coding standards followed				: Yes
Naming conventions followed				: Yes
Third party integrations				: No
Maximum of No of records tested with	: 
Customer Approval						: 
Last Modified Date						: 26/July/2012
Approved by								: 

*/
global with sharing class SL_BatchSchedule_RapNetPriceUpdate implements Schedulable
{
	//Method of the Schedulable interface...
	global void execute(SchedulableContext SC) 
	{ 
		//	Creates an instance of the Batch Class.
		SL_Batch_RapNetPriceUpdate batch = new SL_Batch_RapNetPriceUpdate();
		
		// Calls the Batch.
		database.executebatch(batch);
	}

}