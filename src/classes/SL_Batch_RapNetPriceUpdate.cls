/**
* @ClassName    : SL_Batch_RapNetPriceUpdate 
* @JIRATicket   : LEVIEV-42
* @CreatedOn    : 26/July/12
* @ModifiedBy   : Vishal
* @Description  : The Batch which updates the Price__c field of Stone__c object weekly according to the changes made in the Rap_Per_Carat__c field of RapNet__c object,
					 based on some mapping conditions. Here we have check that the update_date__c field of the RapNet__c object should be >= the previous runtime of 
					 the batch class, which is stored in the custom setting object named SL_BatchMonitor__c.
*/


/**
@Developer Name							: Vishal 
Percentage of best practices followed	: 100%
No of SOQL queries used					: 3
No of collections used					: 2
Exception Handling implemented			: Yes
Coding standards followed				: Yes
Naming conventions followed				: Yes
Third party integrations				: No
Maximum of No of records tested with	: 
Customer Approval						: 
Last Modified Date						: 31/July/2012
Approved by								: 

*/

global with sharing class SL_Batch_RapNetPriceUpdate implements Database.Batchable<sObject>
{
	/* Class Variable */
	public string strQuery = '';
	
	public SL_Batch_RapNetPriceUpdate()
	{
		//Select all those RapNet__c records whose update_time__c >= last run time of the batch.
		strQuery =  'Select Id, Clarity__c, Color_new__c, Price__c, HighSize__c, LowSize__c, Update_Date__c, Shape__c '; 
		strQuery += 'from RapNet__c ';
		strQuery += 'where Update_Date__c >=: strPrevBatchRunTime';
	}
	
	//	start method of the batch, which will return the List<sObject> of size=lstRapNet to the execute method
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		try
		{	
			//Gets the last run time of the batch class.
			SL_BatchMonitor__c objCustomSetting = SL_BatchMonitor__c.getOrgDefaults();
		
			//Stores Previous run time of the batch
			Date strPrevBatchRunTime = objCustomSetting.PrevBatchRunTime__c;
		}	
		catch(exception e)
		{
			//Debug Exception if any
			system.debug('### Exception: '+e);
		}
		
		//Returns the RapNet__c rcords those matched the condition to the Execute method.
		return Database.getQueryLocator(strQuery);
	}
	
	//Execute method, which will recieve the lstRapNet from the start method of the batch
	global void execute(Database.BatchableContext BC, List<RapNet__c> lstRapNet) 
	{
		/* Local Variables */
		List<Stone__c> lstStoneUpdate = new List<Stone__c>();
		
		//Updating the custom setting's value with the current Datetime value
		SL_BatchMonitor__c objCustomSetting = SL_BatchMonitor__c.getOrgDefaults();
		try
		{	
			//Putting current date in Custom Setting
			objCustomSetting.PrevBatchRunTime__c = Date.Today();
			
			//Update Custom Setting
			update objCustomSetting; 
		}	
		catch(exception e)
		{
			//Debug the Exceptions if any
			System.debug('### Exception: '+e);
		}
		
		//Iterate the lstRapNet
		for(RapNet__c objRapnet : lstRapNet)
		{
			//Stores the Stone__c records that matches the conditions, in the strQuery
			List<Stone__c> lstStone = new List<Stone__c>();
			
			//Checking if the Shape field of Rapnet object is "Round"
			//and if so we will match to Shape field of Stone object with "BR"
			//else we will match the same with "Pear"
			if(objRapnet.Shape__c == 'Round')
			{
				//Iterates the Stone__c records those match the criteria.
				for(Stone__c objStone : [select Id, Price__c 
										 from Stone__c 
										 where Quantity__c = 1
										 	AND Clarity__c =: objRapnet.Clarity__c 
											AND Color_new__c =: objRapnet.Color__c 
											AND Carats__c <=: objRapnet.HighSize__c 
											AND Carats__c >=: objRapnet.LowSize__c 
											AND Shape__c = 'BR'])
				{
					//Price__c fields of all the records of Stone__c object will be updated with the Rap_Per_Carat_price__c value of te RapNet__c object.
					objStone.Rap_Price__c = objRapnet.Price__c;
					
					//The corresponding stone object records are added to the List to be updated
					lstStoneUpdate.add(objStone);
				}
			}
			else
			{
				//Iterates the Stone__c records those match the criteria.
				for(Stone__c objStone : [select Id,Rap_Price__c 
										 from Stone__c 
										 where Quantity__c = 1
										 	AND Clarity__c =: objRapnet.Clarity__c 
											AND Color_new__c =: objRapnet.Color__c 
											AND Carats__c <=: objRapnet.HighSize__c 
											AND Carats__c >=: objRapnet.LowSize__c 
											AND Shape__c != 'BR'])
				{
					//Price__c fields of all the records of Stone__c object will be updated with the Rap_Per_Carat_price__c value of te RapNet__c object.
					objStone.Rap_Price__c = objRapnet.Price__c;
					
					//The corresponding stone object records are added to the List to be updated
					lstStoneUpdate.add(objStone);
				}
			}			 
		}
		//update the List of Stone__c records
		update lstStoneUpdate;			
	}
	
	//Finish method, that will perform required post batch operations
	global void finish(Database.BatchableContext BC)
	{
		//do nothing
	}
}