public with sharing class SL_Bugs
{
	
	public Map<String, Schema.SObjectType> m_objects = null; 
	public Transient Map<String, Schema.DescribeSObjectResult> m_described_objects = null;
	public Transient Map<String, Map<String, Schema.DescribeFieldResult>> m_described_fields_by_object = null;
	
	
	public string getbug1()
	{
		String RetVal = '';
		Date dt = Date.today();
		RetVal += 'code: Date dt = Date.today();<br/>';

		Datetime dttm = (DateTime)dt; //Why??? after this we have day-1 or -8h
		RetVal += 'code: Datetime dttm = (DateTime)dt;<br/>';

		RetVal += 'result ( String.ValueOf(dt) ), Date: <b>' + String.ValueOf(dt)+'</b><br/>';
		RetVal += 'result ( String.ValueOf(dttm) ), date after casting to datetime : <b>' + String.ValueOf(dttm)+'</b><br/><br/><br/>';
  
		List<Sobject> objectList = [Select CloseDate From Opportunity Where CloseDate != null LIMIT 1];
		RetVal += 'code: List<Sobject> objectList = [Select CloseDate From Opportunity Where CloseDate != null LIMIT 1]; // Should be created 1 Opportunity record!<br/>';
  		if(!objectList.isEmpty()) {
			DateTime closeDate = (DateTime)objectList.get(0).get('closedate');
			RetVal += 'code: DateTime closeDate = (DateTime)objectList.get(0).get(\'closedate\');<br/>';
			
			RetVal += 'result ( String.ValueOf(closeDate) ), [closeDate] date manualy cast to datetime: <b>' + String.ValueOf(closeDate)+'</b><br/>';
			RetVal += 'result ( String.ValueOf(objectList.get(0).get(\'closedate\')) ), [closeDate] date automatic cast to datetime: <b>' + String.ValueOf(objectList.get(0).get('closedate'))+'</b><br/>';
		}
		
		return RetVal;
		
	}
	
	
	public string getbug2()
	{
		String RetVal = '';
		Schema.DescribeSObjectResult r = Lead.sObjectType.getDescribe();
		RetVal += 'code: Schema.DescribeSObjectResult r = Lead.sObjectType.getDescribe();<br/>';
		RetVal += 'result ( r.getKeyPrefix() ): <b>'+r.getKeyPrefix()+'</b><br/><br/>';

		try
		{
			Schema.DescribeSObjectResult otm = OpportunityTeamMember.sObjectType.getDescribe();
			RetVal += 'code: Schema.DescribeSObjectResult otm = OpportunityTeamMember.sObjectType.getDescribe(); // Represents a User on the sales team of an Opportunity. The sales team includes other users that are working on the opportunity with them. This object is available only in organizations that have enabled the team selling functionality.<br/>';
			RetVal += 'result ( otm.getKeyPrefix() ): <b>'+otm.getKeyPrefix()+'</b>';
		}
		catch(Exception ex)
		{
			RetVal += 'OpportunityTeamMember is not exist!';
		}
		
		return RetVal;
	}
	
	public string getbug3()
	{
		String object_name = 'Lead';
		m_described_objects = new Map<String, Schema.DescribeSObjectResult>();
		m_described_fields_by_object = null;

		m_objects = Schema.getGlobalDescribe();
		Schema.SObjectType object_type = m_objects.get(object_name);
		System.debug('----------:'+object_type);
        Schema.DescribeSObjectResult object_describe_result = object_type.getDescribe();
        System.debug('+++++++++++:'+object_describe_result);
        m_described_objects.put(object_name, object_describe_result);
		
		Map<String, Schema.DescribeFieldResult> result = new Map<String, Schema.DescribeFieldResult>();
        Map<String, Schema.SObjectField> object_fields = object_describe_result.fields.getMap();
        for(String field_name : object_fields.keySet()) {
        	Schema.DescribeFieldResult field_describe_result = object_fields.get(field_name).getDescribe();
            result.put(field_name, field_describe_result);
        }
        
        System.debug('============:'+result.keySet());
        
       
		
		
		String RetVal = '';
		Schema.DescribeFieldResult F1 = Schema.SObjectType.Lead.fields.CreatedDate;
		RetVal += 'code: Schema.DescribeFieldResult F1 = Schema.SObjectType.Lead.fields.CreatedDate;<br/>';
		RetVal += 'result ( F1.isAccessible() ): <b>'+ result.get('createddate').isAccessible()+'</b><br/><br/>';

		Schema.DescribeFieldResult F2 = Schema.SObjectType.Lead.fields.FirstName;
		RetVal += 'code: Schema.DescribeFieldResult F2 = Schema.SObjectType.Lead.fields.FirstName;<br/>';
		RetVal += 'result ( F2.isAccessible() ): <b>'+result.get('firstname').isAccessible()+'</b><br/>';

		
		return RetVal;
	}
	
	public static testMethod void testThis()
	{
		SL_Bugs cls = new SL_Bugs();
		String strTest = '';
		strTest = cls.getbug1();
		strTest = cls.getbug2();
		strTest = cls.getbug3();
	}
	
}