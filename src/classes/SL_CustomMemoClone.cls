public class SL_CustomMemoClone
{
	
	public List<MemoLineItem> lstMemoLineItem {get;set;}
	public List<MemoLineItem> lstMemoLineItemToBeDeleted{get;set;}
	public Memo_Line__c objMemoLine	{get;set;}
	public Boolean isNoMemoLineItem	{get;set;}
	public Boolean isExpirationDate	{get;set;}
	private String strMemoId;	
    //public List<Memo_Line__c> lstMemoLine;
    
    
   
    
    //Inner Class
    public class MemoLineItem
    {
    	public Memo_Line__c objMemoLine	{get;set;}
		public Memo__c objMemo					{get;set;}
		public Stone__c objStone				{get;set;}
		public Boolean isSaveOrCancelRecord		{get;set;} 
		public Boolean isEditOrRemoveRecord		{get;set;} 
		public Boolean isNewRecord				{get;set;} 
		public Boolean isRemove					{get;set;} 
		public Boolean isSave					{get;set;} 
		public Boolean isCancel					{get;set;} 
		public Boolean isEdit					{get;set;} 	
		public List<MemoLineItem> lstMemoLineItem{get;set;}
		// Inner Class Constructor
		public MemoLineItem(Memo_Line__c paramMemoLine)
		{
			objMemoLine = new Memo_Line__c();
			objMemo= new Memo__c();
			if(paramMemoLine.Id == null)
			
				if(paramMemoLine.Id != null)	
				objMemoLine = new Memo_Line__c(Id = paramMemoLine.Id,
												 //Name = paramMemoLine.Name,
												 Quantity__c = paramMemoLine.Quantity__c,
												 Carat__c = paramMemoLine.Carat__c,
												 Price__c = paramMemoLine.Price__c,
												 Status__c = paramMemoLine.Status__c);
			/*
			else 
				objMemoLine = new Memo_Line__c(UserId = paramAccountTeamMember.UserId, 
														 AccountId = paramAccountTeamMember.AccountId, 
														 TeamMemberRole = paramAccountTeamMember.TeamMemberRole);
														 
			if(paramAccountShare != null && paramTempAccountTeam != null )
			{
				objAccountShare = paramAccountShare;
				objTempAccountTeam = paramTempAccountTeam;	
				strAccountAccessLevel = paramAccountShare.AccountAccessLevel;			
			}
			else
			{
				objAccountShare = new AccountShare(UserOrGroupId = objAccountTeamMember.UserId, AccountId = objAccountTeamMember.AccountId, AccountAccessLevel= strAccountAccessLevel);
				objTempAccountTeam = new Temp_Account_Team__c(User__c = objAccountTeamMember.UserId, Account__c = objAccountTeamMember.AccountId);
			}
			*/
			
			isNewRecord = false;
			isSaveOrCancelRecord = false;
			isEditOrRemoveRecord = false;		
			isEdit = isCancel = isRemove = isSave = false;
			
		}
    } 
	
	public SL_CustomMemoClone(ApexPages.standardController ctrl)
	{
		System.debug('---a----');
		// Initializing all the global variables.
		initializeVariables();
		strMemoId = ctrl.getId();
        Memo__c objMemoId = [Select id FROM Memo__c where id = :strMemoId];
        
        for(Memo_Line__c objMemoLineItem : [Select id, Name, Memo__c, Quantity__c, Carat__c, Price__c, Status__c 
											from Memo_Line__c
											where Memo__c = :objMemoId.id])
		{
			MemoLineItem objInnerClass = new MemoLineItem(objMemoLineItem);
			objInnerClass.isSaveOrCancelRecord = false;
			objInnerClass.isEditOrRemoveRecord = true;
			objInnerClass.isNewRecord = false;
			lstMemoLineItem.add(objInnerClass);
		}
		
		
	}
	
	//	Initializing Variables
	public void initializeVariables()
	{
		
		strMemoId = '';
		isNoMemoLineItem = false;
		lstMemoLineItem = new List<MemoLineItem>();	
		lstMemoLineItemToBeDeleted = new List<MemoLineItem>();	
	}
	
	
	//	Add Memo Line Item
	public void addMemoLine()
	{
		Memo_Line__c objMemoLine = new Memo_Line__c();
		Memo__c objMemo = new Memo__c();
		Stone__c objStone = new Stone__c();
		
		MemoLineItem objMemoLineItem = new MemoLineItem(objMemoLine);
		
		objMemoLineItem.isSaveOrCancelRecord = true;
		objMemoLineItem.isEditOrRemoveRecord = false;
		objMemoLineItem.isNewRecord = true;
		lstMemoLineItem.add(objMemoLineItem);

		isNoMemoLineItem = false;
	}
	
	//	Save Functionality
	public void saveMemoLine()
	{
		system.debug('====save===');
	}
	
	//
	public void editMemoLine()
	{
		system.debug('====edit===');
	}
	
    // 
	public void removeMemoLine()
	{
		system.debug('====delete==');
	}

}