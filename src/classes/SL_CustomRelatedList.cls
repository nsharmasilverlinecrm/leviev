public with sharing class SL_CustomRelatedList 
{
	// Global variables 
	public List<customMemoRelated> lstCustomMemoRelated {get;set;}
	
	
	// Inner Class
	public class customMemoRelated
	{
		public String selectedRelatedId {get;set;}
		public String selectedRelatedName {get;set;}
		
		public customMemoRelated(Memo__c paramobjMemo)
		{
			
			selectedRelatedId = selectedRelatedName = '';
			selectedRelatedName = paramobjMemo.Name;
			selectedRelatedId = paramobjMemo.Id;
		}
	}
	
	// Standard Controller
	public SL_CustomRelatedList(ApexPages.standardController std)
	{
		initialise();
		fetchData();
	}
	
	public void initialise()
	{
		lstCustomMemoRelated = new List<customMemoRelated>();
	}
	
	public void fetchData()
	{
		for(Memo__c objMemo : [Select Id, Name from Memo__c where Name != NULL ORDER BY Name ASC Limit 1])
		{
			customMemoRelated objCRL = new customMemoRelated(objMemo);
			lstCustomMemoRelated.add(objCRL);
		}
	}
}