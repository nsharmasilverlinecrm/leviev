// http://datatables.net/usage/server-side

global with sharing class SL_DataTables_Remote {

	public ID curRecordID {get; set;}

	public SL_DataTables_Remote() {
		
	}

	public SL_DataTables_Remote(ApexPages.StandardController controller){
		try{
			sObject inRecord = (sObject)controller.getRecord();
			curRecordID = inRecord.ID;
		}
		catch (Exception e) {
		}
	}  


	// @RemoteAction
	private static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
	{
	    Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
	    Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
	    Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
 
	    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
 
	    return fieldSetObj.getFields(); 
	}  
	@RemoteAction
	global static List<DTFields> getFieldSetFields(String fieldSetName, String objectName) {
		List<DTFields> fields = new List<DTFields>();
		for(Schema.FieldSetMember fieldSetMemberObj : readFieldSet(fieldSetName,objectName))
		{
		    // system.debug('API Name ====>' + fieldSetMemberObj.getFieldPath()); //api name
		    // system.debug('Label ====>' + fieldSetMemberObj.getLabel());
		    // system.debug('Required ====>' + fieldSetMemberObj.getRequired());
		    // system.debug('DbRequired ====>' + fieldSetMemberObj.getDbRequired());
		    // system.debug('Type ====>' + fieldSetMemberObj.getType());   //type - STRING,PICKLIST
		    DTFields dt = new DTFields();
		    dt.APIName = fieldSetMemberObj.getFieldPath();
			dt.Label = fieldSetMemberObj.getLabel();
			dt.Required = fieldSetMemberObj.getRequired();
			dt.DBRequired = fieldSetMemberObj.getDbRequired();
			dt.Type = fieldSetMemberObj.getType().name();
									
			fields.add(dt);
		}
		return fields;
	}


	@RemoteAction
	global static ServerData getData(String sObjectType, List<DTFields> dtFields, String filterExp, String lookupNameFields, List<KVP> aoData)
	{
		Map<String,String> aoDataMap = new Map<String,String>();
		Map<String,String> mapNames = new Map<String,String>();

		for(String s : lookupNameFields.split(';'))
		{
			if(s.split(':').size() == 2)
			{
				mapNames.put(s.split(':')[0], s.split(':')[1]);
			}
		}

		for(KVP k : aoData)
		{
			aoDataMap.put(k.name, k.value);
		}
 		String addlFields = '';
 		String fieldsCSV = '';
 		Set<String> fields = new Set<String>();

 		for(DTFields s : dtFields)
 		{
 			fields.add(s.APIName);
 			
 			if( mapNames.get(s.APIName) != null )
 			{
 				fields.add(mapNames.get(s.APIName));
 			}
 			
 			// if(s.APIName == 'Id')
 			// {
 			// 	fields.add('Name');
 			// }
 			// else if(s.APIName.substring(s.APIName.length() - 3, s.APIName.length()) == '.Id')
 			// {
 			// 	// fieldsCSV += s.APIName.substring(0,s.APIName.length() - 3) + '.Name,';
 			// 	fields.add(s.APIName.substring(0, s.APIName.length() - 3) + '.Name');
 			// }

 		}
 		for(String s : fields)
 		{
 			fieldsCSV += s + ',';
 		}
 		
 		if(fieldsCSV.length() > 0) fieldsCSV = fieldsCSV.substring(0,fieldsCSV.length() - 1);
 
 		if(aoDataMap.get('sSearch').length() > 0)
 		{
 			ServerData sd = new ServerData();
			sd.sEcho = aoDataMap.get('sEcho');
			sd.iTotalRecords = 200;
			sd.iTotalDisplayRecords = 200;
			sd.sColumns = aoDataMap.get('sColumns');
			sd.aaData = sosl(sObjectType, fieldsCSV, filterExp, aoDataMap.get('sSearch'), dtFields[Integer.valueOf(aoDataMap.get('iSortCol_0'))].APIName);
			return sd;	
 		}

		String query = 'SELECT ' + fieldsCSV + ' FROM ' + sObjectType  + ' ' + filterExp + ' ORDER BY ' + dtFields[Integer.valueOf(aoDataMap.get('iSortCol_0'))].APIName + ' ' + aoDataMap.get('sSortDir_0') + ' LIMIT ' + aoDataMap.get('iDisplayLength') + ' OFFSET ' + aoDataMap.get('iDisplayStart');
		String countQuery = 'SELECT COUNT() FROM ' + sObjectType  + ' ' + filterExp;
		system.debug(query);
		Integer cnt = Database.countQuery(countQuery);

		List<sObject> objList = Database.query(query);

		ServerData sd = new ServerData();
		sd.sEcho = aoDataMap.get('sEcho');
		sd.iTotalRecords = cnt;
		sd.iTotalDisplayRecords = cnt;
		sd.sColumns = aoDataMap.get('sColumns');
		sd.aaData = objList;
		return sd;

	}

	global static List<sObject> sosl(String sObjectType, String fieldCSV, String filterExp, String what, String orderBy )
	{
		String sosl = 'FIND \'' +  what + '\' IN ALL FIELDS RETURNING ' + sObjectType + ' (' + fieldCSV + ' ' + filterExp + ' ORDER BY ' + orderBy + ' )' + ' LIMIT 200';
		List<List<sObject>> objList = search.query(sosl);

		return objList[0];	
	}

	global class KVP {
		String name;
		String value;
	}
	global class ServerData {
		Integer iTotalRecords;
		Integer iTotalDisplayRecords;
		String sEcho;
		String sColumns; // optional
		List<sObject> aaData;

	}
	global class DTFields
	{
		String APIName;
		String Label;
		Boolean Required;
		Boolean DBRequired;
		String Type;
	}
}