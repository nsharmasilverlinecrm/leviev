/*
	Date: 6 Jul 2011
	Author: Liza Romanenko
	Task: LEVIEV-29
	Description: This new visualforce page should override the 'new' Standard page for this object. 
	This page should directly inherit the detail component of the standard page. Here are the 
	functions for the page:

	-When page opens, focus should be on the 'jewelry' field. 
	-user should be able to enter just 'jewelry code' number (like in jewelry tracking form)
	-when user hits 'enter', the form should save the new entry, then show a blank page again 
		(just like clicking 'save & New' ) 
	-focus should return to jewelry field after every save and new.
*/

public with sharing class SL_JewelryListEntryNew {
	
	private final Jewelry_List_Entry__c jewelryListEntry;
	
	public SL_JewelryListEntryNew(ApexPages.StandardController stdController) {
        this.jewelryListEntry = (Jewelry_List_Entry__c)stdController.getRecord();
    }
  
     public PageReference saveAndNew(){
    	ApexPages.StandardController sc = new ApexPages.StandardController(jewelryListEntry);
    	Pagereference prs = sc.save();
    	ApexPages.Message[] mes = ApexPages.getMessages();
    	if(mes.size()>0)
    		return prs;
    	Pagereference pr = ApexPages.currentPage();
    	pr.setRedirect(true);
    	return pr;
    }
    
    public PageReference backJewelryList(){
    	if(jewelryListEntry.Jewelry_List__c != null) {
    		PageReference retPg = new PageReference('/'+jewelryListEntry.Jewelry_List__c);
    		return retPg;
    	}
    	Pagereference pr = ApexPages.currentPage();
    	pr.setRedirect(true);
    	return pr;
    }
    
    public static testmethod void doTest(){
		
		Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ'); 
   		insert pr;
        
        Jewelry_List__c testObj = new Jewelry_List__c(Name='test Name');
        testObj.Active__c = false;
        insert testObj;
        
        Jewelry_List_Entry__c le1 = new Jewelry_List_Entry__c();
        le1.Jewelry_List__c = testObj.ID;
        le1.Jewelry__c = pr.ID;
        insert le1;
		
		
		Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(le1);
		SL_JewelryListEntryNew cls = new SL_JewelryListEntryNew(sc);
		cls.saveAndNew();
		cls.backJewelryList();
		Test.stopTest();
	}
	
}