public without sharing class SL_Jewelry_Partner_Handler {
	
	private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    private Account defaultPartner;
    public static boolean isBeforeUpdateHasRun = false;
    public static boolean isAfterUpdateHasRun = false;
    
    public SL_Jewelry_Partner_Handler(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
		for(Account a : [select Id from Account where MainPartner__c = true limit 1])
		{
			this.defaultPartner = a;
			break;
		}
	}
	
	public void OnBeforeInsert(list<Jewelry_Partner__c> newPartnerList) {
		checkSumPercent(newPartnerList, null);
	}
	
	public void OnAfterInsert(map<Id,Jewelry_Partner__c> newPartners) {
		if(!isAfterUpdateHasRun)
		{
			isAfterUpdateHasRun = true;
			createMissingPartners(newPartners);
		}
	}
	
	public void OnBeforeUpdate(map<Id,Jewelry_Partner__c> newPartners, map<Id,Jewelry_Partner__c> oldPartners) {
		
		checkSumPercent(newPartners.values(), oldPartners);
	}
	
	public void OnAfterUpdate(map<Id,Jewelry_Partner__c> newPartners, map<Id,Jewelry_Partner__c> oldPartners) {

		if(!isAfterUpdateHasRun)
		{
			isAfterUpdateHasRun = true;
			createMissingPartners(newPartners);
		}
		
	}
	
	public void OnBeforeDelete(map<Id,Jewelry_Partner__c> oldPartnerMap) {}
     
    public void OnAfterDelete(map<Id,Jewelry_Partner__c> oldPartnerMap) {

		if(!isAfterUpdateHasRun)
		{
			isAfterUpdateHasRun = true;
			createMissingPartners(oldPartnerMap);
		}    	
    }
    
    public void OnUndelete(list<Jewelry_Partner__c> restoredPartners) {}

    private void checkSumPercent(List<Jewelry_Partner__c> newPartners, map<Id,Jewelry_Partner__c> mapoldPartners)
    {
		Set<Id> errors = new Set<Id>();

		for(Product2 j : getAllPartners(newPartners))
		{
			if(calculateSum(j.Jewelry_Partner__r) > 100)
			{
				errors.add(j.Id);
			}
		}
		Jewelry_Partner__c errorRecord;
		for(Jewelry_Partner__c jp : newPartners)
		{
			if(errors.contains(jp.Jewelry__c))
			{
				//if(mapoldPartners == null) 
				//{
					jp.Partner_Percent__c.addError('The sum of all the partners exceeds 100%');
				//}
				//else
				//{
				//	errorRecord = mapoldPartners.get(jp.id);
				//	errorRecord.Partner_Percent__c.addError('The sum of all the partners exceeds 100%');
				//}
			}
		}    	
    }

    private List<Product2> getAllPartners(List<Jewelry_Partner__c> newPartners)
    {
    	List<Product2> allPartners;
    	List<Id> setJewelry = new List<Id>();
    	for(Jewelry_Partner__c j : newPartners)
    	{
    		setJewelry.add(j.Jewelry__c);
    	}

    	if(!setJewelry.isEmpty())
    	{
    		allPartners = [select Id, (select Id, Client__c, Jewelry__c, Partner_Percent__c from Jewelry_Partner__r) from Product2 where Id in :setJewelry];
    	}   
    	return allPartners; 	
    } 

    private void createMissingPartners(map<Id,Jewelry_Partner__c> newPartners) 
    {
    	if(defaultPartner == null) return;

    	Set<Id> setJewelry = new Set<Id>();
		List<Product2> allPartners;
		Decimal sumPercent = 0;
		List<Jewelry_Partner__c> createPartners = new List<Jewelry_Partner__c>();
		List<Jewelry_Partner__c> updatePartners = new List<Jewelry_Partner__c>();
    	Decimal sum;
    	Id foundId;

    	allPartners = getAllPartners(newPartners.values());

    	for(Product2 j : allPartners)
    	{
    		foundId = null;
	    	for(Jewelry_Partner__c jp : j.Jewelry_Partner__r)
	    	{    		
	    		if(jp.Client__c == this.defaultPartner.id)
	    		{
	    			foundId = jp.Id;
	    		}
	    	}
	    	if(foundId == null) 
	    		createPartners.add(createPartner(null,j.Id, 100 - calculateSum(j.Jewelry_Partner__r)));
	    	else
	    		updatePartners.add(createPartner(foundId, j.Id, 100 - calculateSum(j.Jewelry_Partner__r)));
	    }

		insert createPartners;
		update updatePartners;

    }

    private Decimal calculateSum(List<Jewelry_Partner__c> lstJ)
    {
    	Decimal sum = 0;
    	for(Jewelry_Partner__c j : lstJ)
    	{
    		if(j.Client__c != this.defaultPartner.id)
    			sum += j.Partner_Percent__c == null ? 0 :j.Partner_Percent__c;
    	}
    	return sum;
    }

    private Jewelry_Partner__c createPartner(Id partnerId, Id jId,  Decimal sum)
    {
 
 		return new Jewelry_Partner__c(Id = partnerId, Jewelry__c = jId, Client__c = this.defaultPartner.id, Partner_Percent__c = sum);
    }

}