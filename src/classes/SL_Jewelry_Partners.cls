public with sharing class SL_Jewelry_Partners {

	private final sObject mysObject;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SL_Jewelry_Partners(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
    }

    public String getRecordName() {
        return 'Hello ' + (String)mysObject.get('name') + ' (' + (Id)mysObject.get('Id') + ')';
    }

    public String getData() {
        List<Stone_Partner__c> stpartners = new List<Stone_Partner__c>();
        stpartners = [select Id, Clients__c, Partner_Percent__c, Stone__r.Total_Cost__c from Stone_Partner__c where Stone__r.Jewelry__c = :mysObject.Id];
        List<Jewelry_Partner__c> jpartners = new List<Jewelry_Partner__c>();
        jpartners = [select Id, Client__c, Client__r.Name, Partner_Percent__c, Jewelry__r.Mounting_Cost__c from Jewelry_Partner__c where Jewelry__c = :mysObject.Id];

        List<rData> rList = new List<rData>();
        rData rd;

        for(Jewelry_Partner__c jp : jpartners)
        {
            rd = new rData();
            rd.ClientId = jp.Client__c;
            rd.ClientName = jp.Client__r.Name;
            rd.Cost = jp.Jewelry__r.Mounting_Cost__c == null || jp.Partner_Percent__c == null ? 0 : jp.Jewelry__r.Mounting_Cost__c * jp.Partner_Percent__c / 100;
            rd.Stones = 0;
            for(Stone_Partner__c sp : stpartners)
            {
                if(sp.Clients__c == jp.Client__c)
                {
                    rd.Stones++;
                    rd.Cost = rd.Cost + (sp.Stone__r.Total_Cost__c == null || sp.Partner_Percent__c == null ? 0 : sp.Stone__r.Total_Cost__c * sp.Partner_Percent__c);
                }
            }
            rList.add(rd);
        }
        return JSON.serialize(rList);
    }

    public string getColumns() {

        List<DTFields> dtf = new List<DTFields>();
        DTFields dt = new DTFields();
        dt.APIName='ClientId';
        dt.Label='Client Name';
        dt.Type='ID';
        dtf.add(dt);

        dt = new DTFields();
        dt.APIName='Cost';
        dt.Label='Cost';
        dt.Type='MONEY'; 
        dtf.add(dt);

        dt = new DTFields();
        dt.APIName='Stones';
        dt.Label='Stones';
        dt.Type='INTEGER';    
        dtf.add(dt);

        return JSON.serialize(dtf);    
    }

    public class DTFields
    {
        String APIName;
        String Label;
        Boolean Required;
        Boolean DBRequired;
        String Type;
    }

    public class rData {
        Id ClientId;
        String ClientName;
        Decimal Cost;
        Integer Stones;
    }
}