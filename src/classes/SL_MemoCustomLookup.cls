public with sharing class SL_MemoCustomLookup 
{

	//public string strOppId{get;set;}
	public Boolean blnSearchAllAccount {get;set;}
	public Boolean isError {get;set;}
	public String strSearch {get;set;}
	public List<Account> lstAccount {get;set;}
	public List<Contact> lstContact {get;set;}
	public String checkRelatedTo {get;set;}
	//public Course__c objCourse {get;set;}
	public SL_MemoCustomLookup()
    {
        try
        {
        	checkRelatedTo = '';
        	isError = blnSearchAllAccount = false;
        	lstAccount = new List<Account>();
        	lstContact = new List<Contact>();
        	strSearch = System.currentPageReference().getParameters().get('Id');
			
			for(Contact objContact : [Select Id, Name from Contact ORDER BY Name Limit 10])
			{
				lstContact.add(objContact);
			}
			
        }
        catch(exception ex)
        {
        	// DO NOTHING
        }
    }
    
    public void searchRelatedTo()
    {
		lstContact = [Select Id, Name from Contact where Name =: strSearch];
    }

}