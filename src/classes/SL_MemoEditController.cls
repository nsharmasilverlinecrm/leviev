public with sharing class SL_MemoEditController
{
    public Boolean isError {get;set;}
    public String strSearchText {get;set;}
    public List<Stone__c> lstStone {get;set;}
	public List<Product2> lstProduct{get;set;}
    //Constructor
    public SL_MemoEditController(ApexPages.StandardController controller)
    {
        isError = false;
    	lstStone = new List<Stone__c>();
    	lstProduct=new List<Product2>();
    	strSearchText = System.currentPageReference().getParameters().get('likestring');
    	searchResult();
    }


    public void searchResult()
    {
    	
    	if(strSearchText != '' && strSearchText != null)
    	{
    		String strSearch = '%' + strSearchText + '%';
    		lstStone = [ Select Id, Name, Jewelry__c, Carats__c, Price__c from Stone__c where Name LIKE :strSearch ORDER BY Name limit 1000];
    		//lstStone = [ Select Id, Name, Jewelry__c, Carats__c, Price__c from Stone__c where Status__c = 'IN' AND Name LIKE :strSearch ORDER BY Name limit 1000];
    		lstProduct = [ Select Name, Id, Total_Weight__c, Reporting_Price__c, Status__c From Product2 where Status__c = 'IN' AND (Name LIKE :strSearch  Or Productcode LIKE :strSearch) ORDER BY Name limit 1000];
    	}
    	else
    	{
    		lstStone = [Select Id, Name,Jewelry__c, Carats__c, Price__c from Stone__c ORDER BY Name limit 10];
    		//lstStone = [Select Id, Name,Jewelry__c, Carats__c, Price__c from Stone__c where Status__c = 'IN' ORDER BY Name limit 10];
    		lstProduct = [Select Name, Id, Total_Weight__c, Reporting_Price__c, Status__c From Product2 where Status__c = 'IN' ORDER BY Name limit 10];
    	}
    	

    }

	
	@isTest
	 private static void test_SL_MemoEditController()
	 {
	 	// Insert Stone
        Stone__c objStone = new Stone__c();
        objStone.Name = 'Test Stone Name';
        insert objStone;
        
        // Insert Jewelry
        Product2 objProduct2 = new Product2();
        objProduct2.Name = 'Test Product';
        objProduct2.ProductCode = 'Test Code';
        objProduct2.Family = 'Pendant';
        objProduct2.Reporting_Price__c = 10000;
        objProduct2.Total_Cost__c = 10000;
        objProduct2.Market_Price__c = 10000;
        objProduct2.Insurance_Price__c = 10000;
        insert objProduct2;
        
        System.currentPageReference().getParameters().put('likestring','test');
        
        ApexPages.StandardController stdController1 = new ApexPages.StandardController(objStone);
        
        SL_MemoEditController objTest = new SL_MemoEditController(stdController1);
        
        System.currentPageReference().getParameters().put('likestring','');
        
        SL_MemoEditController objTest2 = new SL_MemoEditController(stdController1);
	    
	 }
	
}