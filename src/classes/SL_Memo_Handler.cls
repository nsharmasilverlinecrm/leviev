/* 
    Class Name  : SL_Memo_Handler
    Description : For all children Memo_Line_c, change Memo_Line_c.Status_c = 'Shipped' if Parent Memo has status field value 'Shipped''
    Author      : Shailendra                                                                                    
*/

/**
@Developer Name                         :   
Percentage of best practices followed   :   
No of SOQL queries used                 :   
No of collections used                  :   
Exception Handling implemented          :   
Coding standards followed               :   
Naming conventions followed             :   
Third party integrations                :   
Maximum of No of records tested with    :   
Customer Approval                       :   
Last Modified Date                      :   
Approved by                             :   

*/

public with sharing class SL_Memo_Handler 
{
	set<Id> setMemoId = new set<Id>(); //Collect the Id of Memo__c new records.
	list<Memo_Line__c> listMemoLine = new list<Memo_Line__c>(); //Collect the object to update Memo_Line__c records.
	
	public void onBeforeInsert(){}
	public void onBeforeUpdate(){}
	public User currentUser;
	
	public SL_Memo_Handler() {
		this.currentUser = [select Id, Division from User where Id = :UserInfo.getUserId()][0];
	}
	/*
        @MethodName     : onAfterInsert 
        @Params			: map<Id, Memo__c > mapId_MemoNew , map<Id, Memo__c> mapId_MemoOld
        @Description    : Called after insertion of records in Memo__c. 
    */
	public void onAfterInsert(List<Memo__c> lstMemoNew)
	{
		//Iterating through map values of new records which are inserted.
		for(Memo__c objMemo : lstMemoNew)
		{
			//Check if the status field has value 'shipped' then collect the ids of these records in a set.
			if(objMemo.Status__c == 'Shipped')
			{
				setMemoId.add(objMemo.Id);
			}
		}
		if(!setMemoId.isEmpty())
			updateChildMemoLineItemsWithStatusShipped(setMemoId);
	}
	
	/*
        @MethodName     : onAfterUpdate 
        @Params			: map<Id, Memo__c > mapId_MemoNew , map<Id, Memo__c> mapId_MemoOld
        @Description    : Called after updation of records in Memo__c.
    */
	public void onAfterUpdate(map<Id, Memo__c > mapId_MemoNew , map<Id, Memo__c> mapId_MemoOld)
	{
		map<Id, string> changeLocations = new map<Id, string>();
		string location;

		//Iterating through map values of new records which are inserted.
		for(Memo__c objMemo : mapId_MemoNew.values())
		{
			//Check whether the new value of Status field is 'Shipped' and updation is also done in status field.if yes then store Id in set.
			if((objMemo.Status__c != mapId_MemoOld.get(objMemo.Id).Status__c) && (objMemo.Status__c == 'Shipped'))
			{
				setMemoId.add(objMemo.Id);
			}
			if(objMemo.Location__c != null && (objMemo.Location__c != mapId_MemoOld.get(objMemo.Id).Location__c  || 
				(objMemo.Status__c != mapId_MemoOld.get(objMemo.Id).Status__c) && (objMemo.Status__c == 'Returned')))
			{
				if(objMemo.Status__c == 'Shipped')
				{
					changeLocations.put(objMemo.Id, objMemo.location__c);
				}

				if(objMemo.Status__c == 'Returned' && this.currentUser.Division != null)
				{
					changeLocations.put(objMemo.Id, this.currentUser.Division);
				}
			}

		}
		if(!setMemoId.isEmpty())
			updateChildMemoLineItemsWithStatusShipped(setMemoId);

		if(!changeLocations.isEmpty())
			changeLocation(changeLocations);
	}
	
	/*
        @MethodName     : updateChildMemoLineItemsWithStatusShipped 
        @Params			: set<Id> set_MemoId
        @Description    : Called in onAferInsert and onAfterUpdate method. Updates all the child Memo Line records' Status to 'Shipped' 
    */
	public void updateChildMemoLineItemsWithStatusShipped(set<Id> set_MemoId)
	{
		//Fetching and Iterating through Memo_Line__c records whose Parent Id present in set.
		for(Memo_Line__c objMemoLine : [select Id, Status__c from Memo_Line__c where Memo__c IN : set_MemoId])
		{
			objMemoLine.Status__c = 'Shipped';
			listMemoLine.add(objMemoLine);
		}
		if(!listMemoLine.isEmpty())
			update listMemoLine;
	}

	private void changeLocation(map<Id, string> mapMemos) 
	{
		List <Memo_Line__c> lstML = [select Id, Jewelry__r.Item_Location__c, Stone__r.Location__c, Memo__c from Memo_Line__c where Memo__c in :mapMemos.keySet()];
 		
 		List<sObject> updItems = new List<sObject>();

		for(Memo_Line__c m : lstML) 
		{
			if(m.Jewelry__c != null)
			{
				updItems.add(new Product2(Id = m.Jewelry__c, Item_Location__c = mapMemos.get(m.Memo__c)));
			}

			if(m.Stone__c != null)
			{
				updItems.add(new Stone__c(Id = m.Stone__c, Location__c = mapMemos.get(m.Memo__c)));
			}	
		}
		update updItems;
	}

}