/**
* @ClassName    : SL_Memo_Line_Handler
* @JIRATicket   : LEVIEV-62
* @CreatedOn    : 23/May/2013
* @ModifiedBy   : 
* @Description  : This is Handler class for SL_Memo_Line Trigger.
*/

/**
@Developer Name                         :  Shailendra 
Percentage of best practices followed   :  100% 
No of SOQL queries used                 :  4 
No of collections used                  :  3 
Exception Handling implemented          :   
Coding standards followed               :  yes 
Naming conventions followed             :  yes 
Third party integrations                :  No 
Maximum of No of records tested with    :   
Customer Approval                       :   
Last Modified Date                      :  24/May/2013  
Approved by                             :   

*/
public with sharing class SL_Memo_Line_Handler 

{


    /* Start - global variables*/
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	// set to maintain jewelry__c(productId) of Memo_Line which is not null
	set<Id> setProductIds = new set<Id>();
	set<Id> setStones = new set<Id>();
	list<Product2> lstProductToUpdate = new list<Product2>();
	list<Stone__c> lstStoneToUpdate = new list<Stone__c>();
	list<Stone__c> lstStoneToUpdate2 = new list<Stone__c>();
	/* End - global variables*/
	
	/* Start - Contructor */
	public SL_Memo_Line_Handler(boolean isExecuting, integer size)
	{
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	/* End - Contructor */
	
	
	/*
		@MethodName : onAfterInsert 
		@param 	    : newMap of the Memo_Line__c
		@Description: This method will call After insert of the Memo_Line__c.
	*/
	public void onAfterInsert(Map<Id,Memo_Line__c> mapMemoLineNew)
	{
		 // Iterating through Memo_Line and placing the Jewelry(ProductId) of Memo_Line which is not null into set
	    for( Memo_Line__c  objMemoLine :mapMemoLineNew.values() )
	    {
		    if( objMemoLine.Jewelry__c != null)
		       setProductIds.add(objMemoLine.Jewelry__c);

		  	if(objMemoLine.Stone__c != null)
		  		setStones.add(objMemoLine.Stone__c);		
	    }
		
		// Calling Method to Update Product and Stone records
		if(!setProductIds.isEmpty())	updateProductAndStoneStatus(setProductIds , 'isInsert');
		if(!setStones.isEmpty())	updateStoneStatus(setStones , 'isInsert');
	}
	
	/* End : onAfterInsert */
	
	
	/*
		@MethodName : onAfterDelete 
		@param 	    : oldMap of the Memo_Line__c
		@Description: This method will call After delete of the Memo_Line__c.
	*/
	public void onAfterDelete(Map<Id,Memo_Line__c> mapMemoLineOld)
	{
		for( Memo_Line__c  objMemoLine :mapMemoLineOld.values() )
	    {
		    if( objMemoLine.Jewelry__c!=null)
		      setProductIds.add(objMemoLine.Jewelry__c);

		  	if(objMemoLine.Stone__c != null)
		  		setStones.add(objMemoLine.Stone__c);
		
	    }
		
		// Calling Method to Update Product and Stone records
		if(!setProductIds.isEmpty())	updateProductAndStoneStatus(setProductIds ,'isDelete');
		if(!setStones.isEmpty())	updateStoneStatus(setStones , 'isDelete');
	}
	
	/* End : onAfterDelete */
	
	public void onAfterUpdate(map<id,Memo_Line__c> oldMemo, map<id,Memo_Line__c> newMemo)
	{
		set<id> memoIds = new set<id>();
		for(Memo_Line__c ml : newMemo.values())
		{
			memoIds.add(ml.Memo__c);
		    if( ml.Jewelry__c!=null)
		    {
		     	if(ml.Status__c == 'Returned')
		      		setProductIds.add(ml.Jewelry__c);			
		    }
		    if( ml.Stone__c!=null)
		    {
		     	if(ml.Status__c == 'Returned')
		      		setStones.add(ml.Stone__c);			
		    }		    
		}

		list<Memo_Line__c> memoLines;
		map<id, Memo__c> memosToUpdate = new map<id, Memo__c>();
		map<id, Memo__c> memosNotReturned = new map<id,memo__c>();

		if(memoIds.size() > 0 )
		{
			memoLines = [select id, Memo__r.Id, Memo__r.Status__c from Memo_Line__c where memo__c in :memoIds and status__c != 'Returned'];

			system.debug(memoLines);
			for(Memo_Line__c ml : memoLines)
			{
				memosNotReturned.put(ml.Memo__r.Id, ml.Memo__r);
			}
			for(Memo_Line__c ml : newMemo.values())
			{
				if(memosNotReturned.get(ml.Memo__c) == null)
					memosToUpdate.put(ml.Memo__c, new Memo__c(Id = ml.Memo__c, Status__c = 'Returned'));
			}
			system.debug(memosToUpdate);
			if(memosToUpdate.values().size() > 0)	
				update memosToUpdate.values();
		}
		if(!setProductIds.isEmpty()) updateProductAndStoneStatus(setProductIds, 'isUpdate');
		if(!setStones.isEmpty()) updateStoneStatus(setStones, 'isUpdate');
	}
	/*
		@MethodName : updateStoneStatus 
		@param 	    : set of Stone Ids and trigger event(string)
		@Description: This method is used to Update Stone Status__c .
	*/
	 private void updateStoneStatus(Set<Id> setProductIds, String strEvent)
	 {
		
		
		 if( strEvent == 'isInsert' )
		 {
		    
	         // updating the Stone whose Jewelry(productId) is present in setProductIds
	         for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Id In:setProductIds ])		
	         {
		
		          objStone.Status__c='M';
		          lstStoneToUpdate2.add(objStone);
	         }	
		 }
		
		if( strEvent =='isDelete' )
		{
 
	
	          // updating the Stone whose Jewelry(productId) is present in setProductIds and Status__c='M'
	        for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Id In:setProductIds and Status__c='M'])		
	        {
		
		       objStone.Status__c='IN';
		       lstStoneToUpdate2.add(objStone);
		
	        }	
		}

		if( strEvent =='isUpdate' )
		{
 
	        for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Id In:setProductIds])		
	        {
		
		       objStone.Status__c='IN';
		       lstStoneToUpdate2.add(objStone);
		
	        }		         	
		}		

		try
		{
			if(lstStoneToUpdate2.size()>0)   
				update lstStoneToUpdate2 ;
			
		}				   
		catch(Exception ex)
		{
		}					   			   
	}
	
	/*
		@MethodName : updateProductAndStoneStatus 
		@param 	    : set of productIds and trigger event(string)
		@Description: This method is used to Update product and Stone Status__c .
	*/
	 private void updateProductAndStoneStatus(Set<Id> setProductIds, String strEvent)
	 {
		
		
		 if( strEvent == 'isInsert' )
		 {
		    
	         // updating the Product whose id is present in setProductIds
	         for( Product2 objProduct : [select Id,Status__c 
							            from Product2
							            where Id In:setProductIds ])
	          {
		
		           objProduct.Status__c='M';
		           lstProductToUpdate.add(objProduct);
	          }	
	          
	         // updating the Stone whose Jewelry(productId) is present in setProductIds
	         for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Jewelry__c In:setProductIds ])		
	         {
		
		          objStone.Status__c='M';
		          lstStoneToUpdate.add(objStone);
	         }	
		 }
		
		if( strEvent =='isDelete' )
		{
	          // updating the Product whose id is present in setProductIds and Status__c='M'
	         for( Product2 objProduct : [select Id,Status__c 
							            from Product2
							            where Id In:setProductIds and Status__c='M'])
	         {
	 	
		         objProduct.Status__c='IN';
		         lstProductToUpdate.add(objProduct);
	         }	
	
	          // updating the Stone whose Jewelry(productId) is present in setProductIds and Status__c='M'
	        for(Stone__c objStone :[select Id,Jewelry__c,Status__c
						           from Stone__c
						           where Jewelry__c In:setProductIds and Status__c='M'])		
	        {
		
		       objStone.Status__c='IN';
		       lstStoneToUpdate.add(objStone);
		
	        }	
		}

		if( strEvent =='isUpdate' )
		{
			for( Product2 objProduct : [select Id,Status__c 
							            from Product2
							            where Id In:setProductIds])
	         {
	 	
		         objProduct.Status__c='IN';
		         lstProductToUpdate.add(objProduct);
	         }		
	        for(Stone__c objStone :[select Id,Jewelry__c,Status__c
						           from Stone__c
						           where Jewelry__c In:setProductIds])		
	        {
		
		       objStone.Status__c='IN';
		       lstStoneToUpdate.add(objStone);
		
	        }		         	
		}		

		try
		{
			if(lstProductToUpdate.size()>0 )
			   update lstProductToUpdate;
			   
			if(lstStoneToUpdate.size()>0)   
				update lstStoneToUpdate ;
			
		}				   
		catch(Exception ex)
		{
		}					   			   
	}
	
	
	/*
		@MethodName : test_SL_Memo_Line_Handler
		@param 	    : NA
		@Description: This is the test Method.
	*/
	@isTest()
	private static void test_SL_Memo_Line_Handler()
	{
		
		Product2 objProduct = new Product2( Name = 'TestProduct1' , ProductCode = 'TestCode1' , Jewelry_Type__c = 'Bracelet') ;
		insert objProduct ;
		
		Stone__c objStone =  new Stone__c( Name ='TestStone1', Jewelry__c=objProduct.id);
		insert objStone ;
		
		RecordType recordTypeId_Memo = [Select SobjectType, Name, Id From RecordType  where SobjectType ='Memo__c' LIMIT 1];
		RecordType recordTypeId_Account = [Select SobjectType, Name, Id From RecordType  where SobjectType = 'Account' and DeveloperName = 'PersonAccount' LIMIT 1];
		
		Account objOfAccount = new Account(RecordTypeId = recordTypeId_Account.Id,LastName = 'Testing',CurrencyIsoCode = 'USD');
		insert objOfAccount;
		
		Memo__c objOfMemo = new Memo__c(Clients__c = objOfAccount.Id,RecordTypeId = recordTypeId_Memo.Id,Shipped_By__c = 'Malca');
		insert objOfMemo;
		
		Memo_Line__c objMemoLine = new Memo_Line__c(Jewelry__c = objProduct.id , Quantity__c =2 , Memo__c =objOfMemo.id );
		insert objMemoLine;
		
		Product2 objProduct1 = [ select Id,Status__c 
								 from Product2
								 where Id=:objMemoLine.Jewelry__c];
		system.assert(objProduct1.Status__c=='M');							
		delete  objMemoLine;
	                    
	}
	
}