public with sharing class SL_Memo_Report_PDF {
	//private final Memo__c memo;
	private String strMemoId;
	private List<Memo_Line__c> MemoLineItems;
	private Integer MemoLineItemsCount {get;set;}
	private Decimal MemoPriceTotal {get;set;}

    //Constructor
	public SL_Memo_Report_PDF(ApexPages.StandardController controller) {
		MemoLineItemsCount = 0;
		MemoPriceTotal = 0;
		this.strMemoId = controller.getId();
		//this.memo = (Memo__c)controller.getRecord();
		MemoLineItems = new List<Memo_Line__c>();
        MemoLineItems = [Select id, Name, Memo__c, Quantity__c, Carat__c, Price__c, Status__c, Description__c, Jewelry__r.Item_Number__c, Jewelry__r.item_image_link__c, Jewelry__r.Image__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Description, Jewelry__r.Name, Price_Carat__c, Stone__c, Stone__r.Name 
											from Memo_Line__c
											where Memo__c = :this.strMemoId];
		
		if(MemoLineItems != null && MemoLineItems.size() > 0)
		{
			MemoLineItemsCount = MemoLineItems.size();
		    for(Memo_Line__c MemoLine : MemoLineItems) {
		    	if(MemoLine.Price__c != null)
			      MemoPriceTotal = MemoPriceTotal + MemoLine.Price__c; 
		    }
		}
	}

    public List<Memo_Line__c> getMemoLines() {
        return MemoLineItems;
    }

	public Integer getItemCount() {
        return MemoLineItemsCount;
	}
	
	public Decimal getMemoPriceTotal() {
        return MemoPriceTotal;
	}

    public String getMemoReportCustomSettings() {
    	String str_result = '';
        //List<SL_Report__c> MemoRepStr = [SELECT name,Text_Report_1__c,Text_Report_2__c,Text_Report_3__c,Text_Report_4__c,Text_Report_5__c,Text_Report_6__c,Text_Report_7__c,Text_Report_8__c,Text_Report_9__c FROM SL_Report__c WHERE Name = 'Memo Report'];
        //if(MemoRepStr != null && MemoRepStr.size()>0)
	    //  str_result = MemoRepStr[0].Text_Report_1__c+' '+MemoRepStr[0].Text_Report_2__c+' '+MemoRepStr[0].Text_Report_3__c+' '+MemoRepStr[0].Text_Report_4__c+' '+MemoRepStr[0].Text_Report_5__c+' '+MemoRepStr[0].Text_Report_6__c+' '+MemoRepStr[0].Text_Report_7__c+' '+MemoRepStr[0].Text_Report_8__c+' '+MemoRepStr[0].Text_Report_9__c;
	    for(SL_Report__c memo_rep : SL_Report__c.getall().values() ) {
	    	if (memo_rep.Name.equals('Memo Report') ) {
	           str_result = memo_rep.Text_Report_1__c+' '+memo_rep.Text_Report_2__c+' '+memo_rep.Text_Report_3__c+' '+memo_rep.Text_Report_4__c+' '+memo_rep.Text_Report_5__c+' '+memo_rep.Text_Report_6__c+' '+memo_rep.Text_Report_7__c+' '+memo_rep.Text_Report_8__c+' '+memo_rep.Text_Report_9__c;
	           break;
	    	}
	    }
        return str_result;
    }

}