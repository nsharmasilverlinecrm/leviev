public without sharing class SL_Product_Handler {
	
	private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    public static boolean isBeforeUpdateHasRun = false;
    public static boolean isAfterUpdateHasRun = false;
    
    public SL_Product_Handler(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	public void OnBeforeInsert(list<Product2> newProductList) {
		updateName(newProductList);
	}
	
	public void OnAfterInsert(map<Id,Product2> newProducts) {}
	
	public void OnBeforeUpdate(map<Id,Product2> newProducts, map<Id,Product2> oldProducts) {

	}
	
	public void OnAfterUpdate(map<Id,Product2> newProducts, map<Id,Product2> oldProducts) {

	}
	
	public void OnBeforeDelete(map<Id,Product2> oldProductMap) {}
     
    public void OnAfterDelete(map<Id,Product2> oldProductMap) {}
    
    public void OnUndelete(list<Product2> restoredProducts) {}

    private void updateName(list<PRoduct2> newOpps) 
    {
        Integer wasLastLastNumber;
        String SQLTxt = '';
        
        NumberCounter__c itemCS = null;
        try
        {
            if(Test.isRunningTest()){ itemCS = [Select LastNumber__c, Id From NumberCounter__c where Name = 'Product2'];} 
            else{ itemCS = [Select LastNumber__c, Id From NumberCounter__c where Name = 'Product2' for update];}
        }
        catch(Exception e){}

        if(itemCS != null) 
        {
            // RecordType rt;
            // try{
            //     rt = [select Id, DeveloperName from RecordType where DeveloperName = 'Order' and sObjectType = 'Opportunity'][0];
            // }
            // catch(Exception e) {}
            // if(rt != null)
            // {
            Integer strExtIdlength = 0;
            integer newNum;
       
            for(Product2 opp : newOpps) 
            {
            	if(opp.Jewelry_Type__c != null)
            	{
	                newNum = ( itemCS.LastNumber__c != null ? Integer.valueOf(itemCS.LastNumber__c) : 0 ) + 1;
					opp.Name = 'J' + opp.Jewelry_Type__c.substring(0,1) + String.valueOf(newNum);
					opp.ProductCode = opp.Name;
	                itemCS.LastNumber__c = newNum;
	            }
            }
            
            if(!Test.isRunningTest()){ update itemCS;}
            // }
        }
    }    
}