public without sharing class SL_SalesOrder_Handler {
	
	private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    public static boolean isBeforeUpdateHasRun = false;
    public static boolean isAfterUpdateHasRun = false;
    
    public SL_SalesOrder_Handler(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	public void OnBeforeInsert(list<Sales_Order__c> newOrderList) {


	}
	
	public void OnAfterInsert(map<Id,Sales_Order__c> newOrders) {}
	
	public void OnBeforeUpdate(map<Id,Sales_Order__c> newOrders, map<Id,Sales_Order__c> oldOrders) {

	}
	
	public void OnAfterUpdate(map<Id,Sales_Order__c> newOrders, map<Id,Sales_Order__c> oldOrders) {

	}
	
	public void OnBeforeDelete(map<Id,Sales_Order__c> oldOrderMap) {}
     
    public void OnAfterDelete(map<Id,Sales_Order__c> oldOrderMap) {}
    
    public void OnUndelete(list<Sales_Order__c> restoredOrders) {}
}