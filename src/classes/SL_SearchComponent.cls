public class SL_SearchComponent {
	public SearchComponentConfig config {get; set;}
 
	public class SearchComponentConfig {
		public Boolean debug { get; set; }
		public String searchType {get;set;}
// The elemId is generated in VF and sent to client.
		public String elemId {get; set;}
		public String nextPage { get; set; }
		public String serverCtlrName = 'SL_SearchComponent';
		public String jsCtlrName {get; set;}
	}
// constructor
	public SL_SearchComponent() {
		this.config = new SearchComponentConfig();
	}
	private final static String THUMBNAIL_LIST_JS = 'SL_SearchComponent';
	public String getSL_SearchComponentJS() {
		return config.debug ? THUMBNAIL_LIST_JS : (THUMBNAIL_LIST_JS
			+ 'Min');
	}
	public virtual String getConfigAsJson() {
		String configStr = JSON.serialize(this.config);
		System.debug(configStr);
		return configStr;
	}
}