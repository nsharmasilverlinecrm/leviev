global class SL_SearchLeviev_Remote 
{
	global static SL_SearchLevievController searchController;

	global SL_SearchLeviev_Remote() {
		
	}

	@RemoteAction
	global static List<Favorite_List__c> getUserFavorites()
	{
		List<Favorite_List__c> options = new List<Favorite_List__c>();
		//id id1 = UserInfo.getUserId();
		
		//String qryStr = 'select ID,Name,FavouriteList_Account__r.FirstName, FavouriteList_Account__r.LastName, FavouriteList_Account__c  from Favorite_List__c where OwnerId=\''+id1+'\' and Name!=\'UserTmpProds\'';
		String qryStr = 'select ID,Name,FavouriteList_Account__r.FirstName, FavouriteList_Account__r.LastName, FavouriteList_Account__c from Favorite_List__c order by CreatedDate desc';
		options = Database.query(qryStr);
		return options;
	}


	@RemoteAction
	global static List<Favorite_List__c> getFilteredUserFavorites(String itemId)
	{

		String queryStr = 'select Favorite_List__c from Favorite_List_Product__c where Jewelry__c=\''+itemId+'\' ';


		List<Favorite_List_Product__c> lstFavorites = new List<Favorite_List_Product__c>();
		 lstFavorites = Database.query(queryStr);

		 Set<id> setFavorites=new Set<id>();
		for(Favorite_List_Product__c obj:lstFavorites){
			setFavorites.add(obj.Favorite_List__c);
		}

		
		List<Favorite_List__c> options = new List<Favorite_List__c>();
		//id id1 = UserInfo.getUserId();

		String qryStr = 'select ID,Name,FavouriteList_Account__r.FirstName, FavouriteList_Account__r.LastName, FavouriteList_Account__c  from Favorite_List__c where Id NOT IN :setFavorites order by CreatedDate desc';
		
		//String qryStr = 'select ID,Name,FavouriteList_Account__r.FirstName, FavouriteList_Account__r.LastName, FavouriteList_Account__c  from Favorite_List__c where OwnerId=\''+id1+'\' and Name!=\'UserTmpProds\'';
		
		options = Database.query(qryStr);

		
		return options;
	}


	@RemoteAction
	global static List<Favorite_List_Product__c> getFavoriteDetail(String favoriteListId)
	{
		List<Favorite_List_Product__c> FavoriteListDetail = new List<Favorite_List_Product__c>();
		//id id1 = UserInfo.getUserId();
		
		//String qryStr = 'select ID,Name,FavouriteList_Account__r.FirstName, FavouriteList_Account__r.LastName, FavouriteList_Account__c  from Favorite_List__c where OwnerId=\''+id1+'\' and Name!=\'UserTmpProds\'';
		String qryStr = 'select ID,Stone__c,Stone__r.Name,Stone__r.Id,Stone__r.Jewelry__r.Image_Source__c,Stone__r.Carats__c,Stone__r.Location__c,Jewelry__c,Jewelry__r.Name, Jewelry__r.Id,Jewelry__r.Image_Source__c,Jewelry__r.GBP_Search_Price__c,Jewelry__r.Total_Weight__c,Jewelry__r.Item_Location__c  from Favorite_List_Product__c where Favorite_List__c=\''+favoriteListId+'\' ';
		FavoriteListDetail = Database.query(qryStr);
		return FavoriteListDetail;
	}

	@RemoteAction
	global static void insertFavoriteItem(String favouriteListId,String itemId)
	{
		Favorite_List_Product__c FavoriteListObj=new Favorite_List_Product__c();
		Id itemIdTemp = (Id) itemId;
		
		if(String.valueOf(itemIdTemp.getSobjectType()) == 'Stone__c') {
			FavoriteListObj.Stone__c=itemId;
		} else {
			FavoriteListObj.Jewelry__c=itemId;
		}
		
		FavoriteListObj.Favorite_List__c=favouriteListId;
		insert FavoriteListObj;
	}

	@RemoteAction
	global static List<Product2> getProductsForFav(Id favId)
	{
		
		List<Favorite_List_Product__c> prods = [select Jewelry__r.ID from Favorite_List_Product__c where Favorite_List__c= :favId];
		List<Id> prodIds = new List<Id>();

		for(Favorite_List_Product__c prod : prods)
		{
			prodIds.adD(prod.Jewelry__r.Id);
		}

		return [select ID,Name,Description,Reporting_Price__c,ProductCode,Item_Number__c,Image__c,Image_Source__c,Item_Location__c,Status__c,Primary_Color__c,Primary_Stone_Shape__c,Primary_Stone_Color__c,Total_Weight__c,item_image_link__c,Moscow_Retail_Price__c,GBP_Search_Price__c,Singapore_Retail_Price__c from Product2 where Jewelry_Type__c != 'Mtg only' AND ID in :prodIds order by ProductCode limit 250];
	
	}

	@RemoteAction
	global static List<Product2> searchJewelry(String jsonSearchParams)
	{
		system.debug('Search Params: ' + jsonSearchParams);
		SL_SearchLevievController p = (SL_SearchLevievController)JSON.deserialize(jsonSearchParams, SL_SearchLevievController.class);

		SL_SearchLevievController sl = new SL_SearchLevievController();
		sl.setILimit(50);
		sl.SearchType = 'Jewerly';
		sl.prod4.ProductCode = p.prod4.ProductCode;
		sl.setBaseColorModBeg(p.getBaseColorModBeg());
		sl.setBaseColorModEnd(p.getBaseColorModEnd());
		// sl.SearchProdStatus 
		sl.setJewTypeList(p.getJewTypeList());
		sl.prod.Item_Location__c = p.prod.Item_Location__c;
		sl.FromClarity = p.FromClarity;
		sl.FromColor = p.FromColor;
		sl.prod.Total_Weight__c = p.prod.Total_Weight__c;
		sl.prod.Set__c = p.prod.Set__c;
		sl.prod2.Total_Weight__c = p.prod2.Total_Weight__c;
		sl.toClarity = p.toClarity;
		sl.toColor = p.toColor;
		sl.prod.Primary_Color__c = p.prod.Primary_Color__c;
	    sl.prod.Reporting_Price__c = p.prod.Reporting_Price__c;
        sl.prod2.Reporting_Price__c = p.prod2.Reporting_Price__c;
		sl.prod.Primary_Stone_Color__c = 'None';
		sl.prod.Primary_Stone_Clarity__c = 'FL';
		sl.prod2.Primary_Stone_Color__c = 'None';
		sl.prod2.Primary_Stone_Clarity__c = 'I2';

		sl.setShapeList(p.getShapeList());
		


		PageReference pr = sl.searchProduct();
		return sl.getProducts();

	}

	@RemoteAction
	global static List<Stone__c> searchStones(String jsonSearchParams)
	{
		system.debug(' Search Params: ' + jsonSearchParams);
		SL_SearchLevievController p = (SL_SearchLevievController)JSON.deserialize(jsonSearchParams, SL_SearchLevievController.class);

		SL_SearchLevievController sl = new SL_SearchLevievController();

		sl.setILimit2(50);
		sl.SearchType = 'Diamond';
		sl.prod4.ProductCode = p.prod4.ProductCode;
		sl.setBaseColorModBeg(p.getBaseColorModBeg());
		sl.setBaseColorModEnd(p.getBaseColorModEnd());
		// sl.SearchProdStatus 
		sl.setJewTypeList(p.getJewTypeList());
		sl.prod.Item_Location__c = p.prod.Item_Location__c;
		sl.FromClarity = p.FromClarity;
		sl.FromColor = p.FromColor;
		sl.prod.Total_Weight__c = p.prod.Total_Weight__c;
		sl.prod.Set__c = p.prod.Set__c;
		sl.prod2.Total_Weight__c = p.prod2.Total_Weight__c;
		sl.toClarity = p.toClarity;
		sl.toColor = p.toColor;
		sl.prod.Primary_Color__c = p.prod.Primary_Color__c;
	    sl.prod.Reporting_Price__c = p.prod.Reporting_Price__c;
        sl.prod2.Reporting_Price__c = p.prod2.Reporting_Price__c;
		sl.prod.Primary_Stone_Color__c = 'None';
		sl.prod.Primary_Stone_Clarity__c = 'FL';
		sl.prod2.Primary_Stone_Color__c = 'None';
		sl.prod2.Primary_Stone_Clarity__c = 'I2';
		sl.setShapeList(p.getShapeList());

		PageReference pr = sl.searchProduct();
		return sl.getStones();

	}

	@RemoteAction
	global static List<Account> getAccountList()
	{
		List<Account> options = new List<Account>();
		id id1 = UserInfo.getUserId();
		
		String qryStr = 'select ID,Name from Account where OwnerId=\''+id1+'\' ';
		options = Database.query(qryStr);
		return options;
	}

	@RemoteAction
	global static Favorite_List__c insertNewFavoriteList(String favouriteListName,String accountId)
	{
		Favorite_List__c newFavoriteList=new Favorite_List__c();
		newFavoriteList.Name=favouriteListName;
		newFavoriteList.FavouriteList_Account__c=accountId;
		insert newFavoriteList;
		return newFavoriteList;
	}

}