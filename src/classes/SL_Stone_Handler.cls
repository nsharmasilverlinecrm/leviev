public without sharing class SL_Stone_Handler {
	
	private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    public static boolean isBeforeUpdateHasRun = false;
    public static boolean isAfterUpdateHasRun = false;
    public Id looseStoneRT;
    public Id parcelRT;

    public SL_Stone_Handler(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
		looseStoneRT = Schema.SObjectType.Stone__c.RecordTypeInfosByName.get('Loose Stone').RecordTypeId;
		parcelRT = Schema.SObjectType.Stone__c.RecordTypeInfosByName.get('Parcel').RecordTypeId;
	}

	public void OnBeforeInsert(list<Stone__c> newStoneList) {

	}
	
	public void OnAfterInsert(map<Id,Stone__c> newStones) {}
	
	public void OnBeforeUpdate(map<Id,Stone__c> newStones, map<Id,Stone__c> oldStones) {
		system.debug(this.looseStoneRT);
		for(Id id : newStones.keySet())
		{
			if(newStones.get(id).Carats__c != oldStones.get(id).Carats__c || 
				newStones.get(id).Total_Cost__c != oldStones.get(id).Total_Cost__c ||
				newStones.get(id).Cost_Ct__c != oldStones.get(id).Cost_Ct__c)
			{
				if(newStones.get(id).RecordTypeId == parcelRT) updateLotCost(newStones.get(id));
				if(newStones.get(id).RecordTypeId == looseStoneRT) updateStoneCost(newStones.get(id));
			}
		}
	}
	
	public void OnAfterUpdate(map<Id,Stone__c> newStones, map<Id,Stone__c> oldStones) {

	}
	
	public void OnBeforeDelete(map<Id,Stone__c> oldStoneMap) {}
     
    public void OnAfterDelete(map<Id,Stone__c> oldStoneMap) {}
    
    public void OnUndelete(list<Stone__c> restoredStones) {}
 
 	private void updateLotCost(Stone__c stone) {
 		stone.Total_Cost__c = stone.Cost_Ct__c * stone.Carats__c;
 	}
 	private void updateStoneCost(Stone__c stone) {
 		stone.Cost_Ct__c = stone.Carats__c == 0 ? 0 : stone.Total_Cost__c / stone.Carats__c;
 	}
}