public without sharing class SL_Stone_Partner_Handler {
	
	private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    private Account defaultPartner;
    public static boolean isBeforeUpdateHasRun = false;
    public static boolean isAfterUpdateHasRun = false;
    
    public SL_Stone_Partner_Handler(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
		for(Account a : [select Id from Account where MainPartner__c = true limit 1])
		{
			this.defaultPartner = a;
			break;
		}
	}
	
	public void OnBeforeInsert(list<Stone_Partner__c> newPartnerList) {
		checkSumPercent(newPartnerList, null);
	}
	
	public void OnAfterInsert(map<Id,Stone_Partner__c> newPartners) {
		if(!isAfterUpdateHasRun)
		{
			isAfterUpdateHasRun = true;
			createMissingPartners(newPartners);
		}
	}
	
	public void OnBeforeUpdate(map<Id,Stone_Partner__c> newPartners, map<Id,Stone_Partner__c> oldPartners) {
		
		checkSumPercent(newPartners.values(), oldPartners);
	}
	
	public void OnAfterUpdate(map<Id,Stone_Partner__c> newPartners, map<Id,Stone_Partner__c> oldPartners) {

		if(!isAfterUpdateHasRun)
		{
			isAfterUpdateHasRun = true;
			createMissingPartners(newPartners);
		}
		
	}
	
	public void OnBeforeDelete(map<Id,Stone_Partner__c> oldPartnerMap) {}
     
    public void OnAfterDelete(map<Id,Stone_Partner__c> oldPartnerMap) {
		if(!isAfterUpdateHasRun)
			createMissingPartners(oldPartnerMap);

		isAfterUpdateHasRun = true;
    }
    
    public void OnUndelete(list<Stone_Partner__c> restoredPartners) {}

    private void checkSumPercent(List<Stone_Partner__c> newPartners, map<Id,Stone_Partner__c> mapoldPartners)
    {
		Set<Id> errors = new Set<Id>();

		for(Stone__c j : getAllPartners(newPartners))
		{
			if(calculateSum(j.Stone_Partners__r) > 100)
			{
				errors.add(j.Id);
			}
		}
		Stone_Partner__c errorRecord;
		for(Stone_Partner__c jp : newPartners)
		{
			if(errors.contains(jp.Stone__c))
			{
				//if(mapoldPartners == null) 
				//{
					jp.Partner_Percent__c.addError('The sum of all the partners exceeds 100%');
				//}
				//else
				//{
				//	errorRecord = mapoldPartners.get(jp.id);
				//	errorRecord.Partner_Percent__c.addError('The sum of all the partners exceeds 100%');
				//}
			}
		}    	
    }

    private List<Stone__c> getAllPartners(List<Stone_Partner__c> newPartners)
    {
    	List<Stone__c> allPartners;
    	List<Id> setStone = new List<Id>();
    	for(Stone_Partner__c j : newPartners)
    	{
    		setStone.add(j.Stone__c);
    	}

    	if(!setStone.isEmpty())
    	{
    		allPartners = [select Id, (select Id, Clients__c, Clients__r.Name, Stone__c, Partner_Percent__c from Stone_Partners__r) from Stone__c where Id in :setStone];
    	}   
    	return allPartners; 	
    } 

    private void createMissingPartners(map<Id,Stone_Partner__c> newPartners) 
    {
    	if(defaultPartner == null) return;

    	Set<Id> setStone = new Set<Id>();
		List<Stone__c> allPartners;
		Decimal sumPercent = 0;
		List<Stone_Partner__c> createPartners = new List<Stone_Partner__c>();
		List<Stone_Partner__c> updatePartners = new List<Stone_Partner__c>();
		List<Stone__c> updateStones = new List<Stone__c>();
    	Decimal sum;
    	Id foundId;

    	allPartners = getAllPartners(newPartners.values());

    	for(Stone__c j : allPartners)
    	{
    		foundId = null;
	    	for(Stone_Partner__c jp : j.Stone_Partners__r)
	    	{    		
	    		if(jp.Clients__c == this.defaultPartner.id)
	    		{
	    			foundId = jp.Id;
	    		}
	    	}
	    	if(j.Stone_Partners__r != null)
	    	{
		    	if(foundId == null) 
		    		createPartners.add(createPartner(null,j.Id, 100 - calculateSum(j.Stone_Partners__r)));
		    	else
		    		updatePartners.add(createPartner(foundId, j.Id, 100 - calculateSum(j.Stone_Partners__r)));

	    		updateStones.add(new Stone__c(Id = j.Id, Partners__c = getAllPartnersText(j.Stone_Partners__r)));
	    	}
	    }

		insert createPartners;
		update updatePartners;

		update updateStones;

    }

    private Decimal calculateSum(List<Stone_Partner__c> lstJ)
    {
    	Decimal sum = 0;
    	for(Stone_Partner__c j : lstJ)
    	{
    		if(j.Clients__c != this.defaultPartner.id)
    			sum += j.Partner_Percent__c == null ? 0 : j.Partner_Percent__c;
    	}
    	return sum;
    }

    private Stone_Partner__c createPartner(Id partnerId, Id jId,  Decimal sum)
    {
 
 		return new Stone_Partner__c(Id = partnerId, Stone__c = jId, Clients__c = this.defaultPartner.id, Partner_Percent__c = sum);
    }

    private String getAllPartnersText(List<Stone_Partner__c> lstJ) 
    {
    	String txt = '';
    	for(Stone_Partner__c s : lstJ) 
    	{
    		txt += s.Clients__r.Name + ': ' + s.Partner_Percent__c + '%; ';
    	}
    	return txt;
    }

}