@isTest(SeeAlldata=true)
public class SL_TestMemo 
{
	static testMethod void codecoverage()
	{
		
		RecordType recordTypeId_Memo = [Select SobjectType, Name, Id From RecordType  where SobjectType ='Memo__c' LIMIT 1];
		RecordType recordTypeId_Account = [Select SobjectType, Name, Id From RecordType  where SobjectType = 'Account' and DeveloperName = 'PersonAccount' LIMIT 1];
		
		Account objOfAccount = new Account();
		objOfAccount.RecordTypeId = recordTypeId_Account.Id;
		objOfAccount.LastName = 'Testing';
		objOfAccount.CurrencyIsoCode = 'USD';
		insert objOfAccount;
		
		Memo__c objOfMemo = new Memo__c();
		objOfMemo.Clients__c = objOfAccount.Id;
		objOfMemo.RecordTypeId = recordTypeId_Memo.Id;
		objOfMemo.Shipped_By__c = 'Malca';
		insert objOfMemo;
		
		Memo_Line__c objOfMemoLine1 = new Memo_Line__c();
		objOfMemoLine1.Quantity__c = 2.0;
		objOfMemoLine1.Memo__c = objOfMemo.Id;
		
		insert objOfMemoLine1;
		
		objOfMemo.Status__c	= 'Shipped';
		update objOfMemo;
		
		Memo_Line__c objOfMemoLine2 = [select Id , Status__c from Memo_Line__c where Memo__c =: objOfMemo.Id];
		system.assertEquals(objOfMemoLine2.Status__c,'Shipped');
	}
	
	static testMethod void Test_SalesOrderLine()
	{
		Product2 objProduct = new Product2( Name = 'TestProduct1' , ProductCode = 'TestCode1' , Jewelry_Type__c = 'Bracelet') ;
		insert objProduct ;
		
		Stone__c objStone =  new Stone__c( Name = 'TestStone1', Jewelry__c = objProduct.Id);
		insert objStone ;
		
		Sales_Order__c objsalesOrder = new Sales_Order__c();
		objsalesOrder.CurrencyIsoCode = 'USD';
		insert objsalesOrder;
		
		Sales_Order_Line__c objSalesOrderLine = new Sales_Order_Line__c(Jewelry__c = objProduct.Id , Quantity__c = 2.0 , Sales_Order__c = objsalesOrder.Id);
		insert objSalesOrderLine;
		
		Product2 objProductRecord = [select Id , Status__c from Product2 where Id =: objSalesOrderLine.Jewelry__c];
		Stone__c objStoneRecord = [select Id , Status__c from Stone__c where Id =: objStone.Id];
		system.assertEquals(objProductRecord.Status__c , 'S');
		system.assertEquals(objStoneRecord.Status__c , 'S');
		
		Id JewelryId = objSalesOrderLine.Jewelry__c;
		delete objSalesOrderLine;
		Product2 objProductRecord2 = [select Id , Status__c from Product2 where Id =: JewelryId];
		Stone__c objStoneRecord2 = [select Id , Status__c from Stone__c where Id =: objStone.Id];
		system.assertEquals(objProductRecord2.Status__c , 'IN');
		
		system.assertEquals(objStoneRecord2.Status__c , 'IN');
	}
}