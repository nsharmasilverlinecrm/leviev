@isTest(seealldata=true)
public class SL_TestSearchLevievController {
	
	static testMethod void codecoverage()
	{
				
		String tvSetSearchSortFld = 'Carats';
		integer tvSearchPagePos = 0;
		Product2 tvProd,tvProd2,tvProd3,tvProd4;
		Stone__c tvStone2,tvStone3;
		Opportunity tvO,tvO2;
		List<SelectOption> optSearchType,optJewTypeList,optShapeList;
		String[] optSearchTypeNames = new String[2];
		optSearchTypeNames[0] = 'Diamond';
		optSearchTypeNames[1] = 'Jewerly';
		
		String[] optJewTypeListNames = new String[11];
		optJewTypeListNames[0] = 'Bracelet';
		optJewTypeListNames[1] = 'Brooch';
		optJewTypeListNames[2] = 'Cufflink';
		optJewTypeListNames[3] = 'Earring';
		optJewTypeListNames[4] = 'Necklace';
		optJewTypeListNames[5] = 'Other';
		optJewTypeListNames[6] = 'Pendant';
		optJewTypeListNames[7] = 'Ring';
		optJewTypeListNames[8] = 'Tiara';
		optJewTypeListNames[9] = 'Watch';
		optJewTypeListNames[10] = 'Loose';

		String[] optShapeListNames = new String[8];
		optShapeListNames[0] = 'BR';
		optShapeListNames[1] = 'PRIN';
		optShapeListNames[2] = 'E/C';
		optShapeListNames[3] = 'RAD';
		optShapeListNames[4] = 'OV';
		optShapeListNames[5] = 'PS';
		optShapeListNames[6] = 'SQ-E/C';
		optShapeListNames[7] = 'CQ';

		String tvBaseColorModBeg;
		
    	String tvBaseColorModEnd;

		String tvSearchType;
		
    	Integer tvDiamCount = 0;
    	Integer tvJewerCount = 0;
    	
    	Boolean resVal = true;
		
		
		
		Stone__c ttStone1 = new Stone__c (Name='1111',JMSNumber__c='1111', Symmetry__c='VG', Measurements__c='8.73-5.94-3.59',Table__c=72.0,Depth__c=60.4,Clarity__c='IF',Polish__c='VG',Certificate_Number__c='12222',Shape__c='E/C',Color_new__c='E',Carats__c=1.73);
		insert ttStone1;
		Stone__c ttStone2 = new Stone__c (Name='1112',JMSNumber__c='1112', Symmetry__c='X', Measurements__c='8.73-5.94-3.59',Table__c=72.0,Depth__c=60.4,Clarity__c='IF',Polish__c='VG',Certificate_Number__c='12222',Shape__c='BR',Color_new__c='E',Carats__c=1.73);
		insert ttStone2;
		Stone__c ttStone3 = new Stone__c (Name='1113',JMSNumber__c='1113', Symmetry__c='G', Measurements__c='8.73-5.94-3.59',Table__c=72.0,Depth__c=60.4,Clarity__c='IF',Polish__c='VG',Certificate_Number__c='12222',Shape__c='PS',Color_new__c='E',Carats__c=1.73);
		insert ttStone3;
		Stone__c ttStone4 = new Stone__c (Name='1114',JMSNumber__c='1114', Symmetry__c='EX', Measurements__c='10.90X10.19X6.63',Table__c=60.0,Depth__c=65.1,Clarity__c='VS2',Polish__c='EX',Certificate_Number__c='12244',Shape__c='E/C',Color_new__c='E',Carats__c=6.23);
		insert ttStone4;
		Stone__c ttStone5 = new Stone__c (Name='1115',JMSNumber__c='1115', Symmetry__c='GD', Measurements__c='6.16x4.66x3.37',Table__c=59.0,Depth__c=72.3,Clarity__c='VS1',Polish__c='GD',Certificate_Number__c='12255',Shape__c='CQ',Color_new__c='E',Carats__c=0.82);
		insert ttStone5;
		
		Product2 ttProd1 = new Product2 (Name='Pr111-JJ0111',Description='Desc pr 1',Reporting_Price__c=5000.0,IsActive=true,ProductCode='JJ0111',Item_Number__c='JJ0111',Item_Location__c='LNY',Primary_Color__c='Yellow',Secondary_Color__c='White',Primary_Stone_Shape__c='VS2',In_Store__c=true,Family='General',Design_type__c='Ring - Solitaire with Pave',Jewelry_Type__c='Ring',Set__c='rrr');
		insert ttProd1;
		ttStone1.Jewelry__r = ttProd1;
		ttStone1.Jewelry__c = ttProd1.ID;
		System.debug('stone ID #########: ' + ttStone1.ID);
		System.debug('product ID #########: ' + ttProd1.ID);
		update ttStone1;
		System.debug('stone.product ID #########: ' + ttStone1.Jewelry__r.ID);
		
		Product2 ttProd2 = new Product2 (Name='Pr112-JJ0222',Description='Desc pr 2',Reporting_Price__c=6000.0,IsActive=true,ProductCode='JJ0222',Item_Number__c='JJ0222',Item_Location__c='LNY',Primary_Color__c='Yellow',Secondary_Color__c='White',Primary_Stone_Shape__c='VS2',In_Store__c=true,Family='General',Design_type__c='Ring - Solitaire with Pave',Jewelry_Type__c='Ring',Set__c='rrr');
		insert ttProd2;
		ttStone2.Jewelry__r = ttProd2;
		ttStone2.Jewelry__c = ttProd2.ID;
		update ttStone2;

		Product2 ttProd3 = new Product2 (Name='Pr113-JJ0333',Description='Desc pr 3',Reporting_Price__c=7000.0,IsActive=true,ProductCode='JJ0333',Item_Number__c='JJ0333',Item_Location__c='LNY',Primary_Color__c='Yellow',Secondary_Color__c='White',Primary_Stone_Shape__c='VS2',In_Store__c=true,Family='General',Design_type__c='Ring - Solitaire with Pave',Jewelry_Type__c='Ring',Set__c='rrr');
		insert ttProd3;
		ttStone3.Jewelry__r = ttProd3;
		ttStone3.Jewelry__c = ttProd3.ID;
		
		update ttStone3;

		Product2 ttProd4 = new Product2 (Name='Pr111-JJ0444',Description='Desc pr 4',Reporting_Price__c=8000.0,IsActive=true,ProductCode='JJ0444',Item_Number__c='JJ0444',Item_Location__c='LNY',Primary_Color__c='Yellow',Secondary_Color__c='White',Primary_Stone_Shape__c='VS2',In_Store__c=true,Family='General',Design_type__c='Ring - Solitaire with Pave',Jewelry_Type__c='Ring',Set__c='rrr');
		insert ttProd4;
		ttStone4.Jewelry__r = ttProd4;
		ttStone4.Jewelry__c = ttProd4.ID;
		update ttStone4;

		Product2 ttProd5 = new Product2 (Name='Pr111-JJ0555',Description='Desc pr 5',Reporting_Price__c=9000.0, IsActive=true, ProductCode='JJ0555', Item_Number__c='JJ0555',Item_Location__c='LNY',Primary_Color__c='Yellow',Secondary_Color__c='White',Primary_Stone_Shape__c='VS2',In_Store__c=true,Family='General',Design_type__c='Ring - Solitaire with Pave',Jewelry_Type__c='Ring',Set__c='rrr');
		insert ttProd5;
		ttStone5.Jewelry__r = ttProd5;
		ttStone5.Jewelry__c = ttProd5.ID;
		update ttStone5;

		sObject s = [select ID from Pricebook2 where IsStandard = TRUE];
    	PricebookEntry pbe = new PricebookEntry(pricebook2id=s.id, product2id=ttProd5.id, unitprice=0.0, CurrencyIsoCode='GBP', isActive=true,UseStandardPrice=false);
    	insert pbe;
		
		Attachment attachment = new Attachment();
  		attachment.Body = Blob.valueOf('test!!!');
  		attachment.Name = String.valueOf('test.txt');
		attachment.ParentId = ttProd5.id; 
  		insert attachment;
		
		
		
		Test.startTest();
		
		PageReference pageRef = Page.SL_SearchLeviev;
		Test.setCurrentPageReference(pageRef);
		
		SL_SearchLevievController controller = new SL_SearchLevievController();
		
		string strTmp = controller.getTestFld();
		
		string strTmp2 = controller.getLinkPageSrc();
		strTmp2 = controller.getLinkPageSrc2();
		
		controller.setFavShowList('');
		System.currentPageReference().getParameters().put('prodID',ttProd5.ID);
		controller.setFavoriteHide(true);
		Boolean BolTmp = controller.getFavoriteHide();
		
		//System.currentPageReference().getParameters().put('pid',ttProd5.ID);
		controller.addFavorite();
		strTmp = controller.getUserFavList();
		controller.setUserFavList(strTmp);		
		controller.delFavList();
		
		//System.currentPageReference().getParameters().put('pid',ttProd5.ID);
		controller.addFavorite();
		//controller.setNewUFavList('Test case List');
		controller.setNewUFavList('Test case List');
		strTmp = controller.getNewUFavList();
		
		system.debug('--------------------Test case List');
		system.debug('--------------------ttProd5.ID'+ttProd5.ID);
		//System.currentPageReference().getParameters().put('pid',ttProd5.ID);
		controller.addFavorite();
	
		controller.createFavList();
		
		
		
		List<Product2> prodList = controller.getUserFavorites();
		controller.setEditUFavList('UserTmpProds');
		strTmp = controller.getEditUFavList();
		controller.editFavList();
		
		//run all getters
		controller.addFavorite();
		controller.changeFromSelectList();
		controller.changeToClarityList();
		controller.changeToSelectList();
		controller.createFavList();
		controller.delFavList();
		controller.delFavorite();
		controller.dummyMethod();
		controller.editFavList();
		controller.getHideStoneSearch();
		controller.getBaseColorModBeg();
		controller.getBaseColorModEnd();
		controller.getChooseRender();
		controller.getCurUserObj();
		controller.getDiamCount();
		controller.getEditUFavList();
		controller.getFavoriteHide();
		controller.getFavShowList();
		controller.getILimit();
		controller.getILimit2();
		controller.getIsPDFList();
		controller.getIsUserNoSingapore();
		controller.getJewerCount();
		controller.getJewTypeList();
		controller.getJewTypeListItems();
		controller.getLinkPageDiaPrintExcel();
		controller.getLinkPageDiaPrintPDF();
		controller.getLinkPageSrc();
		controller.getLinkPageSrcLinkName();
		controller.getShColorName('test');
		controller.getLinkPageSrc2();
		controller.getLinkPageSrc2LinkName();
		controller.getLinkPageSrcBase();
		controller.getLinkPageSrcBase2();
		controller.getNewUFavList();
		controller.getProd();
		controller.getProd2();
		controller.getProd3();
		controller.getProd4();
		controller.getProducts();
		controller.getProxyObject();
		controller.getProxyObject2();
		controller.getSearchPagePos();
		controller.getSearchProductStatus();
		controller.getSearchSortFld();
		controller.getSearchType();
		controller.getSearchTypeItems();
		controller.getShapeList();
		controller.getShapeListItems();
		controller.getStone2();
		controller.getStone3();
		controller.getStones();
		controller.getStones2();
		controller.getStonesSize();
		controller.getTestFld();
		controller.getUserFavList();
		controller.getUserFavListItems();
		controller.getUserFavorites();
		controller.searchProduct();		
		
		//run all setters
		controller.setBaseColorModBeg('test1');
		controller.setBaseColorModEnd('test2');
		controller.setEditUFavList('str1');
		controller.setFavoriteHide(false);
		//controller.setFavShowList(ttprod1.id);
		controller.setIsPDFList('true');
		controller.setJewTypeList(new list<String>{'str2','str21'});
		controller.setNewUFavList('test3');
		controller.setSearchPagePos(1);
		controller.setSearchProductStatus(new list<String>{'str4','str41'});
		controller.setSearchSortFld('test4');
		controller.setSearchType('test5');
		controller.setShapeList(new list<String>{'str5','str6','str7'});
		controller.setUserFavList(ttprod2.id);
		controller.getUserFavorites();
		
		controller.setEditUFavList('str1');
		controller.setUserFavList(ttprod3.id);
		controller.editFavList();
		
		//run all getters again
		controller.addFavorite();
		
		controller.changeFromSelectList();
		controller.changeToSelectList();
		controller.baseColor = 'White';
		controller.changeFromSelectList();
		controller.changeToSelectList();
		
		controller.changeToClarityList();
		controller.changeToSelectList();
		controller.createFavList();
		controller.delFavList();
		controller.delFavorite();
		controller.dummyMethod();
		controller.editFavList();
		controller.getBaseColorModBeg();
		controller.getBaseColorModEnd();
		controller.getChooseRender();
		controller.getCurUserObj();
		controller.getDiamCount();
		controller.getEditUFavList();
		controller.getFavoriteHide();
		controller.getFavShowList();
		controller.getILimit();
		controller.getILimit2();
		controller.getIsPDFList();
		controller.getIsUserNoSingapore();
		controller.getJewerCount();
		controller.getJewTypeList();
		controller.getJewTypeListItems();
		controller.getLinkPageDiaPrintExcel();
		controller.getLinkPageDiaPrintPDF();
		controller.getLinkPageSrc();
		controller.getLinkPageSrc2();
		controller.getLinkPageSrc2LinkName();
		controller.getLinkPageSrcBase();
		controller.getNewUFavList();
		controller.getProd();
		controller.getProd2();
		controller.getProd3();
		controller.getProd4();
		controller.getProducts();
		controller.getProxyObject();
		controller.getProxyObject2();
		controller.getSearchPagePos();
		controller.getSearchProductStatus();
		controller.getSearchSortFld();
		controller.getSearchType();
		controller.getSearchTypeItems();
		controller.getShapeList();
		controller.getShapeListItems();
		controller.getStone2();
		controller.getStone3();
		controller.getStones();
		controller.getStones2();
		controller.getTestFld();
		controller.getUserFavList();
		controller.getUserFavListItems();
		controller.getUserFavorites();
		
		
		controller.prod = new Product2 (Primary_Color__c='White',Primary_Stone_Color__c='D');
		controller.prod2 = new Product2 (Primary_Color__c='White',Primary_Stone_Color__c='J');
		controller.prod3 = new Product2 (Primary_Color__c='White');
		controller.prod4 = new Product2 (Primary_Color__c='Yellow',Secondary_Color__c='White');
		controller.stone2 = new Stone__c (Shape__c='BR');
		controller.stone3 = new Stone__c (Shape__c='PS');
		controller.setBaseColorModBeg('');
		controller.setBaseColorModEnd('');
		controller.prod.ProductCode = '11122';
    	controller.prod.Primary_Color__c = 'Yellow';
    	
    	controller.prod.Primary_Stone_Clarity__c = 'IF';
    	controller.prod2.Primary_Stone_Clarity__c = 'SI2';
    	controller.prod.Total_Weight__c = 0.1;
    	controller.prod2.Total_Weight__c = 140;

		controller.setBaseColorModBeg('Lt');
		tvBaseColorModBeg = controller.getBaseColorModBeg();
		controller.setBaseColorModEnd('FV');
		tvBaseColorModEnd = controller.getBaseColorModEnd();
    	
    	controller.strJewelryOrStone = 'Total Weight';
    	controller.weightChange();
    	
    	controller.strJewelryOrStone = '';
    	controller.weightChange();
    	
    	controller.isTest = true;
		
// 1 - LIST!!!!!!!!		
    	controller.searchProduct();
		System.debug('Diamont counter: ' + controller.getDiamCount());
		List<Integer> listStone2 = controller.getStones2();
		controller.prod.ProductCode = '111';
		controller.prod.Primary_Color__c = 'White';
		controller.o.closeDate = system.today();
		controller.o2.closeDate = system.today();
		controller.searchProduct();

// 2 - LIST!!!!!!!!
    	controller.prod4.ProductCode = 'JJ0';

    	controller.prod.Family = 'General';
    	controller.prod.Design_type__c='Ring - Solitaire with Pave';
    	controller.prod.In_Store__c = true;
    	controller.prod.Reporting_Price__c = 100.0;
    	controller.prod2.Reporting_Price__c = 10000.0;
    	controller.prod.Set__c='rrr';
    	controller.SearchType = 'Jewerly';
    	controller.prod.Item_Location__c = 'New York';

// 3 - LIST!!!!!!!!
		
    	controller.searchProduct();
    	controller.prod4.ProductCode = 'JJ0';
    	controller.prod.Family = 'General';
    	controller.prod.Design_type__c='Ring - Solitaire with Pave';
    	controller.prod.In_Store__c = true;
    	controller.prod.Reporting_Price__c = 100.0;
    	controller.prod2.Reporting_Price__c = 10000.0;
    	controller.prod.Set__c='rrr';
    	controller.SearchType = 'Diamond';
    	controller.prod.Item_Location__c = 'New York';
    	List<Product2> aListProd = controller.getProducts();
		controller.searchProduct();
		
		controller.setFavShowList(ttprod1.id);
		controller.getFavShowList();
		controller.searchProduct();
		
		controller.SearchType = 'Jewerly';
		controller.prod3.Total_Weight__c = 50;
		controller.prod4.Total_Weight__c = 50;
		controller.stone2.Weight__c = '50';
		controller.stone3.Weight__c = '50';
		controller.searchProduct();
		
		String tmpgetLinkPageSrcBase = controller.getLinkPageSrcBase();
		String tmpgetLinkPageSrcBase2 = controller.getLinkPageSrcBase2();
		String tmpgetLinkPageDiaPrintPDF = controller.getLinkPageDiaPrintPDF();
		String tmpgetLinkPageDiaPrintExcel = controller.getLinkPageDiaPrintExcel();
		String tmpgetLinkPageSrcLinkName = controller.getLinkPageSrcLinkName();
		String tmpgetLinkPageSrc2LinkName = controller.getLinkPageSrc2LinkName();
		Test.stopTest();
	}

}