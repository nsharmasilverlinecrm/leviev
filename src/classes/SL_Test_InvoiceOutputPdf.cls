/**
*
* 
*/
@isTest
public class SL_Test_InvoiceOutputPdf {
	
	public static testMethod void InvoiceData(){
		
		List<Invoice_Line__c> receivedInvoiceItems = new List<Invoice_Line__c>();
		Invoice__c invoiceData;
		String Customer;
		SL_ctrl_InvoiceOutputPdf testClass = new SL_ctrl_InvoiceOutputPdf();
		
		Account testAcc = new Account(Name = 'TestAcc');
		insert testAcc;
		Invoice__c testInvoice = new Invoice__c(Clients__c = testAcc.Id);
		insert testInvoice;
		Invoice_Line__c testLine = new Invoice_Line__c(Invoice__c = testInvoice.Id,Quantity__c = 100, Total_Amount__c = 100, Price_Carat__c = 1); 
		insert testLine;
		//Setting the url with invoice id.
		Test.setCurrentPage(new PageReference('/apex/InvoiceOutput'));
     	ApexPages.currentPage().getParameters().put('InvId', testInvoice.Id);
		
		receivedInvoiceItems = testClass.getreceivedInvoiceItems();
		//Added one invoice line item, checking this row count		
		system.assert(receivedInvoiceItems.size() == 1);
	}
}