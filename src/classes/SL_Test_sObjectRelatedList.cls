public with sharing class SL_Test_sObjectRelatedList
{
	public static testMethod void SL_Test_sObjectRelatedList()
	{
		// Insert Account
		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        // Insert Stone
        Stone__c objStone = new Stone__c();
        objStone.Name = 'Test Stone Name';
        insert objStone;

        // Insert Jewelry
        Product2 objProduct2 = new Product2();
        objProduct2.Name = 'Test Product';
        objProduct2.ProductCode = 'Test Code';
        objProduct2.Family = 'Pendant';
        objProduct2.Reporting_Price__c = 10000;
        objProduct2.Total_Cost__c = 10000;
        objProduct2.Market_Price__c = 10000;
        objProduct2.Insurance_Price__c = 10000;
        insert objProduct2;

        // Insert Memo
        Memo__c objMemo = new Memo__c();
        objMemo.Clients__c = objAccount.Id;
        insert objMemo;

        // Insert Memo Line for Stone
        Memo_Line__c objMemoLine1 = new Memo_Line__c();
        objMemoLine1.Memo__c = objMemo.Id;
        objMemoLine1.Stone__c = objStone.Id;
        objMemoLine1.Carat__c = 2.03;
        objMemoLine1.Quantity__c = 2;
        objMemoLine1.Price__c = 21000;
        insert objMemoLine1;

        // Insert Memo Line2 for Jewelry
        Memo_Line__c objMemoLine2 = new Memo_Line__c();
        objMemoLine2.Memo__c = objMemo.Id;
        objMemoLine2.Jewelry__c = objProduct2.Id;
        objMemoLine2.Carat__c = 2.03;
        objMemoLine2.Quantity__c = 2;
        objMemoLine2.Price__c = 21000;
        insert objMemoLine2;

        ApexPages.StandardController stdController1 = new ApexPages.StandardController(objMemo);
        SL_sObjectRelatedList objSObjectRelatedList1 = new SL_sObjectRelatedList(stdController1);

        //Populate existing memo line records
        objSObjectRelatedList1.selectedId = objProduct2.Id;
        objSObjectRelatedList1.lstsObjectLineItem[0].selectedType = 'Jewelery';
        objSObjectRelatedList1.lstsObjectLineItem[0].isChange = true;
        objSObjectRelatedList1.populateMemoValues();

        objSObjectRelatedList1.selectedId = objStone.Id;
        objSObjectRelatedList1.lstsObjectLineItem[1].selectedType = 'Stone';
        objSObjectRelatedList1.lstsObjectLineItem[1].isChange = true;
        objSObjectRelatedList1.populateMemoValues();

        //adding blank row
        objSObjectRelatedList1.addsObjectLine();
        //saving blank row to cover
        objSObjectRelatedList1.savesObjectLine();

        objSObjectRelatedList1.lstsObjectLineItem[0].isEditOrRemoveRecord = false;
        objSObjectRelatedList1.lstsObjectLineItem[1].isEditOrRemoveRecord = false;

		objSObjectRelatedList1.lstsObjectLineItem[1].objMemoLineItem.Jewelry__c = null;
        objSObjectRelatedList1.editsObjectLine();
        objSObjectRelatedList1.removesObjectLine();

        objSObjectRelatedList1.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList1.savesObjectLine();
        objSObjectRelatedList1.cancelsObjectLine();

        objSObjectRelatedList1.lstsObjectLineItem[1].isSaveOrCancelRecord = true;
        objSObjectRelatedList1.savesObjectLine();
        objSObjectRelatedList1.cancelsObjectLine();

        objSObjectRelatedList1.lstsObjectLineItem[0].isCancel = true;
	    objSObjectRelatedList1.cancelsObjectLine();

	    objSObjectRelatedList1.lstsObjectLineItem[1].isCancel = true;
	    objSObjectRelatedList1.lstsObjectLineItem[1].objMemoLineItem.Stone__c = null;
	    objSObjectRelatedList1.cancelsObjectLine();

		objSObjectRelatedList1.lstsObjectLineItem[0].isSave = true;
		objSObjectRelatedList1.lstsObjectLineItem[0].SelectedValue = '';
	    objSObjectRelatedList1.isValidateLineMemo();

	    objSObjectRelatedList1.lstsObjectLineItem[0].SelectedValue = 'Test Stone Name';
	    objSObjectRelatedList1.lstsObjectLineItem[0].selectedId = '';
	    objSObjectRelatedList1.isValidateLineMemo();

	    objSObjectRelatedList1.lstsObjectLineItem[0].selectedId = 'a0FW0000000AC4rMAG';
	    objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Quantity__c = -2;
	    objSObjectRelatedList1.isValidateLineMemo();

	    objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Quantity__c = 2;
	    objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Carat__c = -2.03;
	    objSObjectRelatedList1.isValidateLineMemo();

	    objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Carat__c = 2.03;
	    objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Price__c = -2011;
	    objSObjectRelatedList1.isValidateLineMemo();

		objSObjectRelatedList1.lstsObjectLineItem[0].objMemoLineItem.Price__c = 21000.00;
	    objSObjectRelatedList1.isValidateLineMemo();

	    objSObjectRelatedList1.lstsObjectLineItem[0].isCheckedRecord= true;
	    objSObjectRelatedList1.lstsObjectLineItem[0].SelectedValue = '';
	    objSObjectRelatedList1.isValidateMemo();

		objSObjectRelatedList1.lstsObjectLineItem[0].SelectedValue = 'Test Stone';
		objSObjectRelatedList1.lstsObjectLineItem[0].selectedId = '';
		objSObjectRelatedList1.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
		objSObjectRelatedList1.isValidateMemo();

		objSObjectRelatedList1.lstsObjectLineItem[0].selectedId = '';
		objSObjectRelatedList1.isValidateMemo();

        ApexPages.StandardController stdController2 = new ApexPages.StandardController(objMemo);
        SL_sObjectRelatedList objSObjectRelatedList2 = new SL_sObjectRelatedList(stdController2);

        objSObjectRelatedList2.addsObjectLine();
        objSObjectRelatedList2.initializeVariables();
        objSObjectRelatedList2.fetchMemoLineItems();
        objSObjectRelatedList2.savesObjectLine();



        // Insert Memo2 FOR VALIDATIONS
        Memo__c objMemo2 = new Memo__c();
        objMemo2.Clients__c = objAccount.Id;
        insert objMemo2;

        // Insert Memo Line3 for QUANTITY
        Memo_Line__c objMemoLine3 = new Memo_Line__c();
        objMemoLine3.Memo__c = objMemo2.Id;
        objMemoLine3.Jewelry__c = objProduct2.Id;
        objMemoLine3.Carat__c = 2.03;
        objMemoLine3.Price__c = 21000;
        insert objMemoLine3;

        // Insert Memo Line4 for CARAT
        Memo_Line__c objMemoLine4 = new Memo_Line__c();
        objMemoLine4.Memo__c = objMemo2.Id;
        objMemoLine4.Jewelry__c = objProduct2.Id;
        objMemoLine4.Quantity__c = 2;
        objMemoLine4.Carat__c = -2.03;
        objMemoLine4.Price__c = 21000;
        insert objMemoLine4;

        // Insert Memo Line5 for PRICE
        Memo_Line__c objMemoLine5 = new Memo_Line__c();
        objMemoLine5.Memo__c = objMemo2.Id;
        objMemoLine5.Jewelry__c = objProduct2.Id;
        objMemoLine5.Quantity__c = 2;
        objMemoLine5.Price__c = -21000;
        insert objMemoLine5;

        Memo_Line__c objMemoLine6 = new Memo_Line__c();
        objMemoLine6.Memo__c = objMemo2.Id;
        objMemoLine6.Stone__c = objStone.Id;
        objMemoLine6.Quantity__c = 2;
        objMemoLine6.Price__c = -21000;
        objMemoLine6.Carat__c = 2.03;
        insert objMemoLine6;

        ApexPages.StandardController stdController9 = new ApexPages.StandardController(objMemo2);
        SL_sObjectRelatedList objSObjectRelatedList9 = new SL_sObjectRelatedList(stdController9);

        objSObjectRelatedList9.lstsObjectLineItem[1].isSave = true;
        objSObjectRelatedList9.savesObjectLine();

        objSObjectRelatedList9.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList9.savesObjectLine();

        objSObjectRelatedList9.lstsObjectLineItem[0].isSaveOrCancelRecord = false;
        objSObjectRelatedList9.savesObjectLine();
        objSObjectRelatedList9.cancelsObjectLine();
        objSObjectRelatedList9.saveAllRecords();

        objSObjectRelatedList9.lstsObjectLineItem[1].isSaveOrCancelRecord = false;
        objSObjectRelatedList9.savesObjectLine();
        objSObjectRelatedList9.cancelsObjectLine();
        objSObjectRelatedList9.lstsObjectLineItem[1].isCheckedRecord = true;
        objSObjectRelatedList9.strRemoveSelectedOrOne = 'Selected';
        objSObjectRelatedList9.fetchSelectedLineItems();
        objSObjectRelatedList9.setToReturned();
      //  objSObjectRelatedList9.saveAllRecords();

        objSObjectRelatedList9.lstsObjectLineItem[2].isSaveOrCancelRecord = false;
        objSObjectRelatedList9.savesObjectLine();
        objSObjectRelatedList9.cancelsObjectLine();
        objSObjectRelatedList9.saveAllRecords();

        objMemoLine3.Quantity__c = 2;
        update objMemoLine3;

        objMemoLine4.Carat__c = 2.03;
        update objMemoLine4;

        objMemoLine5.Price__c = 21000;
        update objMemoLine5;

        objMemoLine6.Price__c = 21000;
        update objMemoLine6;

        ApexPages.StandardController stdController10 = new ApexPages.StandardController(objMemo2);
        SL_sObjectRelatedList objSObjectRelatedList10 = new SL_sObjectRelatedList(stdController10);
        objSObjectRelatedList10.lstsObjectLineItem[2].isCheckedRecord = true;
        objSObjectRelatedList10.lstsObjectLineItem[2].isSave = true;
        objSObjectRelatedList10.strSaveAllOrOne = 'All';
        objSObjectRelatedList10.saveAllRecords();
        objSObjectRelatedList10.savesObjectLine();

        objSObjectRelatedList10.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList10.lstsObjectLineItem[1].isSave = true;
        objSObjectRelatedList10.lstsObjectLineItem[3].isSave = true;
        objSObjectRelatedList10.savesObjectLine();

        // Insert Invoice
        Invoice__c objInvoice = new Invoice__c();
        objInvoice.Clients__c = objAccount.Id;
        insert objInvoice;

        // Insert Invoice Line
        Invoice_Line__c objInvoiceLine = new Invoice_Line__c();
        objInvoiceLine.Stone__c = objStone.Id;
        objInvoiceLine.Invoice__c = objInvoice.Id;
        objInvoiceLine.Carat__c = 2.03;
        objInvoiceLine.Quantity__c = 2;
        objInvoiceLine.Total_Amount__c = 21000;
        insert objInvoiceLine;

        // Insert Invoice Line2 for Jewelry
        Invoice_Line__c objInvoiceLine2 = new Invoice_Line__c();
        objInvoiceLine2.Invoice__c = objInvoice.Id;
        objInvoiceLine2.Jewelry__c = objProduct2.Id;
        objInvoiceLine2.Carat__c = 2.03;
        objInvoiceLine2.Quantity__c = 2;
        objInvoiceLine2.Total_Amount__c = 21000;
        insert objInvoiceLine2;

        ApexPages.StandardController stdController3 = new ApexPages.StandardController(objInvoice);
        SL_sObjectRelatedList objSObjectRelatedList3 = new SL_sObjectRelatedList(stdController3);

        //Populate existing Invoice line records (Jewelery)
        objSObjectRelatedList3.selectedId = objProduct2.Id;
        objSObjectRelatedList3.lstsObjectLineItem[1].selectedType = 'Jewelery';
        objSObjectRelatedList3.lstsObjectLineItem[1].isChange = true;
        objSObjectRelatedList3.populateInvoiceValues();

        //Populate existing Invoice line records (Stone)
        objSObjectRelatedList3.selectedId = objStone.Id;
        objSObjectRelatedList3.lstsObjectLineItem[0].selectedType = 'Stone';
        objSObjectRelatedList3.lstsObjectLineItem[0].isChange = true;
        objSObjectRelatedList3.populateInvoiceValues();

        objSObjectRelatedList3.lstsObjectLineItem[0].isEditOrRemoveRecord = false;
        objSObjectRelatedList3.editsObjectLine();
        objSObjectRelatedList3.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList3.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList3.savesObjectLine();

		objSObjectRelatedList3.lstsObjectLineItem[0].isCancel = true;
        objSObjectRelatedList3.cancelsObjectLine();

		objSObjectRelatedList3.lstsObjectLineItem[1].isCancel = true;
        objSObjectRelatedList3.lstsObjectLineItem[1].objInvoiceLineItem.Stone__c = NULL;
        objSObjectRelatedList3.cancelsObjectLine();

        objSObjectRelatedList3.lstsObjectLineItem[0].isCheckedRecord = true;
        objSObjectRelatedList3.lstsObjectLineItem[0].selectedValue = '';
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].selectedValue = 'Test Stone Name';
        objSObjectRelatedList3.lstsObjectLineItem[0].selectedId = '';
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].selectedId = '';
        objSObjectRelatedList3.strSaveAllOrOne  = 'ALL';
        objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Quantity__c = -1;
        objSObjectRelatedList3.isValidateInvoice();

		objSObjectRelatedList3.lstsObjectLineItem[0].selectedId = 'a0FW0000000ABcCMAW';
		objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Quantity__c = -2;
        objSObjectRelatedList3.isValidateInvoice();

		objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Quantity__c = 2;
		objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Carat__c = -1.01;
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Carat__c = 1.01;
        objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Total_Amount__c = -1;
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Total_Amount__c = -200;
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].objInvoiceLineItem.Total_Amount__c = 200;
        objSObjectRelatedList3.isValidateInvoice();

        objSObjectRelatedList3.lstsObjectLineItem[0].isCheckedRecord = true;
        objSObjectRelatedList3.isRemoveSelected = true;
        objSObjectRelatedList3.removesObjectLine();

        ApexPages.StandardController stdController4 = new ApexPages.StandardController(objInvoice);
        SL_sObjectRelatedList objSObjectRelatedList4 = new SL_sObjectRelatedList(stdController3);

        objSObjectRelatedList4.addsObjectLine();
        objSObjectRelatedList4.initializeVariables();
        objSObjectRelatedList4.populateInvoiceValues();
        objSObjectRelatedList4.fetchInvoiceLineItems();
        objSObjectRelatedList4.savesObjectLine();


        // Insert Invoice2
        Invoice__c objInvoice2 = new Invoice__c();
        objInvoice2.Clients__c = objAccount.Id;
        insert objInvoice2;

        // 2
        ApexPages.StandardController stdController5 = new ApexPages.StandardController(objInvoice2);
        SL_sObjectRelatedList objSObjectRelatedList5 = new SL_sObjectRelatedList(stdController5);

        //adding blank row
        objSObjectRelatedList5.addsObjectLine();

        //saving blank row to cover validation
		objSObjectRelatedList5.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList5.savesObjectLine();


        // Insert Invoice FOR VALIDATIONS
        Invoice__c objInvoice3 = new Invoice__c();
        objInvoice3.Clients__c = objAccount.Id;
        insert objInvoice3;

        // Insert Invoice Line to validate QUANTITY
        Invoice_Line__c objInvoiceLine6 = new Invoice_Line__c();
        objInvoiceLine6.Stone__c = objStone.Id;
        objInvoiceLine6.Invoice__c = objInvoice3.Id;
        objInvoiceLine6.Carat__c = 2.03;
        objInvoiceLine6.Total_Amount__c = 21000;
        insert objInvoiceLine6;

        // Insert Invoice Line to validate CARAT
        Invoice_Line__c objInvoiceLine7 = new Invoice_Line__c();
        objInvoiceLine7.Stone__c = objStone.Id;
        objInvoiceLine7.Invoice__c = objInvoice3.Id;
        objInvoiceLine7.Quantity__c = 2;
        objInvoiceLine7.Carat__c = -2;
        objInvoiceLine7.Total_Amount__c = 21000;
        insert objInvoiceLine7;

        // Insert Invoice Line to validate PRICE
        Invoice_Line__c objInvoiceLine8 = new Invoice_Line__c();
        objInvoiceLine8.Stone__c = objStone.Id;
        objInvoiceLine8.Invoice__c = objInvoice3.Id;
        objInvoiceLine8.Quantity__c = 2;
        objInvoiceLine8.Carat__c = 2.03;
        objInvoiceLine8.Total_Amount__c = -21000;
        insert objInvoiceLine8;

        ApexPages.StandardController stdController6 = new ApexPages.StandardController(objInvoice3);
        SL_sObjectRelatedList objSObjectRelatedList6 = new SL_sObjectRelatedList(stdController6);

        //Populate existing memo line records
        objSObjectRelatedList6.selectedId = objProduct2.Id;
        objSObjectRelatedList6.lstsObjectLineItem[0].selectedType = 'Jewelery';
        objSObjectRelatedList6.lstsObjectLineItem[0].isChange = true;
        objSObjectRelatedList6.populateMemoValues();

        objSObjectRelatedList6.selectedId = objStone.Id;
        objSObjectRelatedList6.lstsObjectLineItem[1].selectedType = 'Stone';
        objSObjectRelatedList6.lstsObjectLineItem[1].isChange = true;
        objSObjectRelatedList6.populateMemoValues();

        objSObjectRelatedList6.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList6.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList6.lstsObjectLineItem[1].isSave = true;
        objSObjectRelatedList6.lstsObjectLineItem[2].isSave = true;
        objSObjectRelatedList6.savesObjectLine();
        objSObjectRelatedList6.cancelsObjectLine();

        objSObjectRelatedList6.lstsObjectLineItem[1].isSaveOrCancelRecord = true;
        objSObjectRelatedList6.savesObjectLine();
        objSObjectRelatedList6.cancelsObjectLine();

        objSObjectRelatedList6.lstsObjectLineItem[2].isSaveOrCancelRecord = true;
        objSObjectRelatedList6.lstsObjectLineItem[2].ischeckedRecord = true;
        objSObjectRelatedList6.savesObjectLine();
        objSObjectRelatedList6.cancelsObjectLine();

        objSObjectRelatedList6.isHeadCheck = true;
        objSObjectRelatedList6.checkAllRecords();

        objSObjectRelatedList6.lstsObjectLineItem[0].isCheckedRecord = false;
        objSObjectRelatedList6.unSelectCheckRecords();

        objSObjectRelatedList6.isHeadCheck = false;
        objSObjectRelatedList6.checkAllRecords();

        objSObjectRelatedList6.unSelectCheckRecords();

        objSObjectRelatedList6.fetchSalesOrderLineItems();

		objInvoiceLine6.Quantity__c = 0;
        update objInvoiceLine6;

        objInvoiceLine7.Carat__c = 2.03;
        update objInvoiceLine7;

        objInvoiceLine8.Total_Amount__c = 21000;
        update objInvoiceLine8;

        ApexPages.StandardController stdController7 = new ApexPages.StandardController(objInvoice3);
        SL_sObjectRelatedList objSObjectRelatedList7 = new SL_sObjectRelatedList(stdController7);

        objSObjectRelatedList7.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList7.strSaveAllOrOne = 'One';
        objSObjectRelatedList7.saveAllRecords();
        objSObjectRelatedList7.getCurUserObj();

		objSObjectRelatedList7.lstsObjectLineItem[0].isSave = true;
		objSObjectRelatedList7.lstsObjectLineItem[0].isCheckedRecord = true;
		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Total_Amount__c = -10;
		objSObjectRelatedList7.isValidateInvoiceLine();

		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Total_Amount__c = 21000.00;
		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Carat__c = -2.03;
		objSObjectRelatedList7.isValidateInvoiceLine();

		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Carat__c = 2.03;
		objSObjectRelatedList7.lstsObjectLineItem[0].selectedId = '';
		objSObjectRelatedList7.isValidateInvoiceLine();

		objSObjectRelatedList7.lstsObjectLineItem[0].selectedId = 'a0FW0000000ABbJMAW';
		objSObjectRelatedList7.lstsObjectLineItem[0].SelectedValue = '';
		objSObjectRelatedList7.isValidateInvoiceLine();

		objSObjectRelatedList7.lstsObjectLineItem[0].SelectedValue = 'Test Stone Name';
		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Quantity__c = -2;
		objSObjectRelatedList7.isValidateInvoiceLine();

		objSObjectRelatedList7.lstsObjectLineItem[0].objInvoiceLineItem.Quantity__c = 2;
		objSObjectRelatedList7.isValidateInvoiceLine();

        //Sales_Order_Line__c
        Sales_Order__c objSalesOrder1 = new Sales_Order__c();
        objSalesOrder1.CurrencyIsoCode = 'USD';
        insert objSalesOrder1;

        Sales_Order_Line__c objSalesOrderLine1 = new Sales_Order_Line__c();
        objSalesOrderLine1.Jewelry__c = objProduct2.Id;
        objSalesOrderLine1.Sales_Order__c = objSalesOrder1.Id;
        objSalesOrderLine1.Price__c = -65;
        insert objSalesOrderLine1;

        Sales_Order_Line__c objSalesOrderLine2 = new Sales_Order_Line__c();
        objSalesOrderLine2.Jewelry__c = objProduct2.Id;
        objSalesOrderLine2.Sales_Order__c = objSalesOrder1.Id;
        objSalesOrderLine2.Price__c = 65;
        objSalesOrderLine2.Quantity__c = -5;
        insert objSalesOrderLine2;

        Sales_Order_Line__c objSalesOrderLine3 = new Sales_Order_Line__c();
        objSalesOrderLine3.Stone__c = objStone.Id;
        objSalesOrderLine3.Sales_Order__c = objSalesOrder1.Id;
        objSalesOrderLine3.Price__c = 100;
        objSalesOrderLine3.Quantity__c = -2;
        insert objSalesOrderLine3;

        Sales_Order_Line__c objSalesOrderLine4 = new Sales_Order_Line__c();
        objSalesOrderLine4.Stone__c = objStone.Id;
        objSalesOrderLine4.Sales_Order__c = objSalesOrder1.Id;
        objSalesOrderLine4.Price__c = 0;
        objSalesOrderLine4.Quantity__c = 54;
        insert objSalesOrderLine4;

        ApexPages.StandardController stdController8 = new ApexPages.StandardController(objSalesOrder1);
        SL_sObjectRelatedList objSObjectRelatedList8 = new SL_sObjectRelatedList(stdController8);

		objSObjectRelatedList8.lstsObjectLineItem[0].isCheckedRecord = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isCheckedRecord = true;

        objSObjectRelatedList8.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[2].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[3].isSave = true;

        objSObjectRelatedList8.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isSave = true;

		objSalesOrderLine1.Price__c = 65;
		update objSalesOrderLine1;

        objSalesOrderLine3.Quantity__c = 2;
        update objSalesOrderLine3;

        objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Price__c = 65;
        objSObjectRelatedList8.lstsObjectLineItem[1].objSalesOrderLineItem.Quantity__c	= 5;
        objSObjectRelatedList8.lstsObjectLineItem[2].objSalesOrderLineItem.Quantity__c	= 2;
        objSObjectRelatedList8.lstsObjectLineItem[3].objSalesOrderLineItem.Price__c	= 2;

        objSObjectRelatedList8.savesObjectLine();

		ApexPages.StandardController stdController12 = new ApexPages.StandardController(objSalesOrder1);
        SL_sObjectRelatedList objSObjectRelatedList12 = new SL_sObjectRelatedList(stdController12);

        objSObjectRelatedList12.lstsObjectLineItem[0].isSaveOrCancelRecord = true;
        objSObjectRelatedList12.lstsObjectLineItem[0].isEditOrRemoveRecord = false;

        objSObjectRelatedList12.editsObjectLine();
        objSObjectRelatedList12.strSaveAllOrOne = 'One';
        objSObjectRelatedList12.saveAllRecords();

        ApexPages.StandardController stdController11 = new ApexPages.StandardController(objSalesOrder1);
        SL_sObjectRelatedList objSObjectRelatedList11 = new SL_sObjectRelatedList(stdController11);

        objSObjectRelatedList11.lstsObjectLineItem[1].isSaveOrCancelRecord = true;
        objSObjectRelatedList11.strSaveAllOrOne = 'One';
        objSObjectRelatedList11.saveAllRecords();

        objSObjectRelatedList11.lstsObjectLineItem[0].isCheckedRecord = true;
        objSObjectRelatedList11.lstsObjectLineItem[1].isCheckedRecord = true;
        objSObjectRelatedList11.removesObjectLine();

        //Populate existing Sales order line records (Jewelery)
        objSObjectRelatedList8.selectedId = objProduct2.Id;
        objSObjectRelatedList8.lstsObjectLineItem[0].selectedType = 'Jewelery';
        objSObjectRelatedList8.lstsObjectLineItem[0].isChange = true;
        objSObjectRelatedList8.populateSalesOrderValues();

        //Populate existing Sales order line records (Stone)
        objSObjectRelatedList8.selectedId = objStone.Id;
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedType = 'Stone';
        objSObjectRelatedList8.lstsObjectLineItem[1].isChange = true;
        objSObjectRelatedList8.populateSalesOrderValues();

        objSObjectRelatedList8.fetchSalesOrderLineItems();

        objSObjectRelatedList8.addsObjectLine();

        objSObjectRelatedList8.lstsObjectLineItem[6].isCancel = true;
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedId = '01tW0000000DAH1IAO';
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedType = 'Jewelery';
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedValue = 'Test Product';
        objSObjectRelatedList8.lstsObjectLineItem[6].objSalesOrderLineItem.Stone__c = null;

        objSObjectRelatedList8.lstsObjectLineItem[1].isCancel = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedId = '01tW0000000DAH1IAO';
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedType = 'Jewelery';
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedValue = 'Test Product';
        objSObjectRelatedList8.cancelsObjectLine();

        objSObjectRelatedList8.lstsObjectLineItem[1].isCancel = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedId = '01tW0000000DAH1IAO';
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedType = 'Jewelery';
        objSObjectRelatedList8.lstsObjectLineItem[1].selectedValue = 'Test Product';
        objSObjectRelatedList8.lstsObjectLineItem[1].objSalesOrderLineItem.Stone__c = 'a0SW00000005bo3MAA';
        objSObjectRelatedList8.cancelsObjectLine();

        objSObjectRelatedList8.lstsObjectLineItem[6].isCancel = true;
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedId = '01tW0000000DAH1IAO';
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedType = 'Jewelery';
        objSObjectRelatedList8.lstsObjectLineItem[6].selectedValue = 'Test Product';
        objSObjectRelatedList8.lstsObjectLineItem[6].objSalesOrderLineItem.Stone__c = 'a0SW00000005bo3MAA';
        objSObjectRelatedList8.cancelsObjectLine();

        objSObjectRelatedList8.isHeadCheck = true;
        objSObjectRelatedList8.checkAllRecords();

        objSObjectRelatedList8.lstsObjectLineItem[0].isCheckedRecord = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isCheckedRecord = true;
        objSObjectRelatedList8.isValidateSalesOrder();

        objSObjectRelatedList8.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[2].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[3].isSave = true;
        objSObjectRelatedList8.isValidateSalesOrder();

        objSObjectRelatedList8.lstsObjectLineItem[0].isSave = true;
        objSObjectRelatedList8.lstsObjectLineItem[1].isSave = true;

		objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Price__c = -65;
		objSObjectRelatedList8.isValidateSalesOrderLine();

		objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Price__c = 65;
		objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Quantity__c = null;
		objSObjectRelatedList8.isValidateSalesOrderLine();

		objSObjectRelatedList8.lstsObjectLineItem[0].SelectedValue = '';
        objSObjectRelatedList8.isValidateSalesOrderLine();

        objSObjectRelatedList8.lstsObjectLineItem[0].selectedValue = 'ABC';
        objSObjectRelatedList8.lstsObjectLineItem[0].selectedId = '';
        objSObjectRelatedList8.isValidateSalesOrderLine();

		objSObjectRelatedList8.lstsObjectLineItem[0].selectedValue = 'ABC';
        objSObjectRelatedList8.lstsObjectLineItem[0].selectedId = 'XYZ';
		objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Quantity__c = -1;
        objSObjectRelatedList8.isValidateSalesOrderLine();

        objSObjectRelatedList8.lstsObjectLineItem[0].objSalesOrderLineItem.Quantity__c = 1;
        objSObjectRelatedList8.isValidateSalesOrderLine();

        /*for cancelsObjectLine method*/
        ApexPages.StandardController stdController13 = new ApexPages.StandardController(objSalesOrder1);
        SL_sObjectRelatedList objSObjectRelatedList13 = new SL_sObjectRelatedList(stdController13);

		/*
        //Populate existing memo line records
        objSObjectRelatedList1.selectedId = objProduct2.Id;
        objSObjectRelatedList1.lstsObjectLineItem[0].selectedType = 'Jewelery';
        objSObjectRelatedList1.lstsObjectLineItem[0].isChange = true;
        objSObjectRelatedList1.populateMemoValues();

        objSObjectRelatedList1.selectedId = objStone.Id;
        objSObjectRelatedList1.lstsObjectLineItem[1].selectedType = 'Stone';
        objSObjectRelatedList1.lstsObjectLineItem[1].isChange = true;
        objSObjectRelatedList1.populateMemoValues();

        objSObjectRelatedList13.lstsObjectLineItem[0].isCancel = true;
        objSObjectRelatedList13.lstsObjectLineItem[1].isCancel = true;
        objSObjectRelatedList13.cancelsObjectLine();
		*/

        //Error occurence condition
        //ApexPages.StandardController stdController7 = new ApexPages.StandardController(null);
        //SL_sObjectRelatedList objSObjectRelatedList7 = new SL_sObjectRelatedList(stdController7);
	}
}