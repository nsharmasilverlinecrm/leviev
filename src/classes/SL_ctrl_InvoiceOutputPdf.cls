/** 

* \author Aleksey Dubinin 

* \date 09/10/2012

* \see https://silverline.jira.com/browse/LEVIEV-53 

* \brief Controller to show Invoice data on web page in pdf format

* \details  

* \details 

* \test SL_Test_InvoiceOutputPdf

*/

public class SL_ctrl_InvoiceOutputPdf {
	
	public List<Invoice_Line__c> receivedInvoiceItems;
	public Invoice__c invoiceData { get; set; }
	private Id invoiceId;
	public String Customer{ get; set; }
	
	public List<Invoice_Line__c> getreceivedInvoiceItems() {
		
		if(invoiceId == NULL){
			invoiceId = (Id) System.currentPageReference().getParameters().get('InvId');
		}
		receivedInvoiceItems = [
			Select 
				i.Type__c, i.Total_Amount__c, i.Quantity__c, i.Price_Carat__c, i.PO_Number__c, i.Name, 
				i.Id, i.Description__c, i.Carat__c 
			From 
				Invoice_Line__c i 
			WHERE 
				i.Invoice__c = :invoiceId
			ORDER BY
				i.Name ASC
		];
		this.invoiceData = invoiceDataGet();
		return receivedInvoiceItems;
	}
	
	public Invoice__c invoiceDataGet(){
		// Getting the current invoice`s data
		Invoice__c var_data = new Invoice__c();
		for(Invoice__c invoice_data_tmp:[
			Select 
				 i.CreatedDate, i.Terms__c, i.Total__c, i.Tax__c, i.Sub_Total__c, i.Shipping__c, 
				 i.Due_Date__c, i.Invoice_Date__c, i.Clients__c, i.Invoice_Type__c, i.Name 
			From 
				Invoice__c i 
			WHERE 
				i.Id = :invoiceId
			LIMIT 1
		]){
			var_data = invoice_data_tmp;
		}
		
		if(var_data.Clients__c != NULL){
			// Getting Customer`s Name
			Account acc = new Account();
			for (Account item:[
				Select 
					a.Name
				From
					Account a
				Where
					a.Id = :var_data.Clients__c
				LIMIT 1
			])
			{
				acc = item;
			}
			String t = String.valueOf(acc.Name);
			this.Customer = t;
		}
		return var_data;
	}
}