/** 
* \author Vladislav Gumenyuk
* \date 07/11/2013
* \see https://silverline.jira.com/browse/LEVIEV-67
* \brief OpportunityLineItems PDF for Opportunity 
* \details  
*/
public with sharing class SL_ctrl_Opportunity_PDF {
	private String strOppId;
	private Id OppId;
	private Id AccId;
	public Account acc {get; set;}
	public Opportunity opp {get; set;}
	private String displayType;
	public string currencyIsoCode;	

	public SL_ctrl_Opportunity_PDF(ApexPages.StandardController controller) 
	{
		this.strOppId = controller.getId();
		//opp = (Opportunity)controller.getRecord();

		this.OppId = (Id) System.currentPageReference().getParameters().get('OppId');
		this.AccId = (Id) System.currentPageReference().getParameters().get('AccId');
		
		system.debug('#### OppId : '+OppId);
		system.debug('#### AccId : '+AccId);
		
		//if(opp != NULL && opp.AccountId != NULL)
		//	this.AccId = opp.AccountId;

		system.debug('#### Opp : '+Opp);
		//system.debug('#### opp.AccountId : '+this.opp.AccountId);

		if(OppId != null) {
        	opp = [Select o.Pricebook2Id, o.CurrencyIsoCode, o.Name, o.Id, o.Deposit_Amount__c, o.CloseDate, o.Amount, o.AccountId  
        					From Opportunity o 
							where o.Id = :OppId Limit 1];
			this.currencyIsoCode = opp.CurrencyIsoCode;
		}
		
		if(AccId != null)
        	acc = [Select a.Phone, a.PersonEmail, a.Name, a.Id, a.Fax, a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, a.BillingCity 
        					From Account a 
							where a.Id = :AccId Limit 1];
		else acc = new Account(Name = 'bad acc');
		//Select a.Phone, a.PersonEmail, a.Name, a.Id, a.Fax, a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, a.BillingCity From Account a
		system.debug('#### Acc : '+Acc);
	}
	
	public String getAccAddress()
	{
		String str_AccAddress = '';
		if(acc!=null)
		{
			if(acc.BillingStreet!=null)
				str_AccAddress = str_AccAddress+(str_AccAddress.length()>0?', ':'')+acc.BillingStreet;
			if(acc.BillingCity!=null)
				str_AccAddress = str_AccAddress+(str_AccAddress.length()>0?', ':'')+acc.BillingCity;
			if(acc.BillingState!=null)
				str_AccAddress = str_AccAddress+(str_AccAddress.length()>0?', ':'')+acc.BillingState;
			if(acc.BillingPostalCode!=null)
				str_AccAddress = str_AccAddress+(str_AccAddress.length()>0?', ':'')+acc.BillingPostalCode;
		}
		return str_AccAddress;
	}

	public Decimal getOppBalanceDue()
	{
		Decimal dec_BalanceDue = 0;
		if(opp!=null)
		{
			//if(opp.Deposit_Amount__c != null && opp.Amount != null)
//				dec_BalanceDue = opp.Amount - (opp.Deposit_Amount__c==null?0:opp.Deposit_Amount__c);
			dec_BalanceDue = (opp.Amount==null?0:opp.Amount) - (opp.Deposit_Amount__c==null?0:opp.Deposit_Amount__c);
		}
		return dec_BalanceDue;
	}
	
	public List<OpportunityLineItem> getOpportunityLineItem()
	{
		List<OpportunityLineItem> list_oppLineItem = new List<OpportunityLineItem>(); 
		if(OppId != null)
			list_oppLineItem = [Select o.UnitPrice, o.TotalPrice, o.Size__c, o.Quantity, o.PricebookEntry.ProductCode, o.PricebookEntry.UseStandardPrice, o.PricebookEntry.Product2Id, o.PricebookEntry.Pricebook2Id, o.PricebookEntry.Name, o.PricebookEntry.Id, o.PricebookEntryId, o.OpportunityId, o.Metal__c, o.ListPrice, o.Id, o.Description From OpportunityLineItem o Where o.OpportunityId = :OppId and  o.PricebookEntry.Name = 'Custom Piece'];
		return list_oppLineItem;	
	}

   public String getCurrencySign() {
   	String currencyStr = getCurrencyType();
      if (currencyStr == 'SGD')     return 'SGD';
      else if (currencyStr == 'USD') return '$';
      else if (currencyStr == 'RUB')   return 'RUB';
      else if (currencyStr == 'AED')    return '$';
      else if (currencyStr == 'EUR')   return '€';
      else if (currencyStr == 'GBP')   return '£';
      return '$';
   }

   public String getCurrencyType() {
   	// get currency from related opp
   	if(this.currencyIsoCode != null) return this.currencyIsoCode;
   	else return 'USD';
   }

}