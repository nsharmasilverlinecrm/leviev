/** 
* \author Vladislav Gumenyuk
* \date 10/15/2012
* \see https://silverline.jira.com/browse/LEVIEV-54
* \brief Replicate Invoice PDF as Sales Order Output Document 
* \details  
*/
//public class SL_ctrl_Sales_Order_PDF 
public with sharing class SL_ctrl_Sales_Order_PDF {

//	public List<Invoice_Line__c> receivedInvoiceItems;
	public List<Sales_Order_Jewelry_Item__c> receivedSalesOrderJewelryItems;
	public List<Sales_Order_Stone_Item__c> receivedSalesOrderStoneItems;
	public List<Sales_Order_Line__c> receivedSalesOrderLines;
//	public Invoice__c invoiceData { get; set; }
	public Sales_Order__c salesOrderData { get; set; }
	public String displayType {get;set;}
//	private Id invoiceId;
	private Id salesOrderId;
	public string currencyIsoCode;
	
	public String Customer{ get; set; }
	public String Attn{ get; set; }
	public String ShipVia{ get; set; }	
	public Double TotalPrice{ get; set; }
	public Double Deposit {get;set;}
	public Double SalesTax {get;set;}
	public Double SalesTaxTotalPrice{ get; set; }
	public Double GrandTotalPrice{ get; set; }

	public SL_ctrl_Sales_Order_PDF() {
		if(salesOrderId == NULL){
			salesOrderId = (Id) System.currentPageReference().getParameters().get('SOId');
		}
		displayType = System.currentPageReference().getParameters().get('displayType');
		this.salesOrderData = salesOrderDataGet();
		TotalPrice = 0;
	}



   public List<Sales_Order_Line__c> getreceivedSalesOrderLines() {

//Select s.Total_Weight__c, s.Stone__c, s.Sales_Order__c, s.Quantity__c, s.Price__c, s.Name, s.Jewelry__c, s.Id, s.CurrencyIsoCode, s.CreatedDate, s.Color_new__c, s.Clarity__c, s.Total_Price__c From Sales_Order_Line__c s           
      receivedSalesOrderLines = [
         Select
                 s.Total_Weight__c, s.Stone__c, s.Sales_Order__c, s.Quantity__c, s.Price__c, s.Name, s.Jewelry__c, s.Id, s.CurrencyIsoCode, s.CreatedDate, s.Color__c, s.Clarity__c, s.Total_Price__c
                 ,Jewelry__r.Item_Number__c, Jewelry__r.item_image_link__c, Jewelry__r.Image__c, 
                 Jewelry__r.Jewelry_Type__c, Jewelry__r.ProductCode, Jewelry__r.Description, Jewelry__r.Name,
                 Jewelry__r.Total_Cost__c,Jewelry__r.Reporting_Price__c, Jewelry__r.Singapore_Retail_Price__c, Jewelry__r.Levant_Suggested_Retail__c, Jewelry__r.GBP_Search_Price__c, Jewelry__r.Moscow_Retail_Price__c
                 ,s.Stone__r.Price__c, s.Stone__r.Certificate_Number__c, s.Stone__r.Carats__c, 
                 s.Stone__r.Description__c, s.Stone__r.Name, s.Stone__r.RapNet_Total__c, s.Stone__r.Rap_Price__c,
                 s.Sales_Order__r.Sales_Tax__c 
         From 
                 Sales_Order_Line__c s
         WHERE 
            s.Sales_Order__c = :salesOrderId
         ORDER BY
            s.Name ASC
      ];

           for(Sales_Order_Line__c sol :receivedSalesOrderLines) {
//             TotalPrice += sol.Jewelry__r.Total_Cost__c;
             this.TotalPrice += sol.Total_Price__c;
           }
      
      //Sales Tax calculation
      this.SalesTaxTotalPrice = (this.SalesTax <> null)? (this.TotalPrice * this.SalesTax)/100 : 0;
      //Grand Total calculation
      this.GrandTotalPrice = this.TotalPrice + this.SalesTaxTotalPrice - this.Deposit;
      
      return receivedSalesOrderLines;
   }

   public String getCurrencySign() {
   	String currencyStr = getCurrencyType();
   	//return ''; // commented for leviev-66
      if (currencyStr == 'SGD')     return 'SGD';
      else if (currencyStr == 'USD') return '$';
      else if (currencyStr == 'RUB')   return 'RUB';
      else if (currencyStr == 'AED')    return '$';
      else if (currencyStr == 'EUR')   return '€';
      else if (currencyStr == 'GBP')   return '£';
      return '$';
   }

   public String getCurrencyType() {
   	// get currency from related opp
  
   	if(this.currencyIsoCode != null) return this.currencyIsoCode;
   	else return 'USD';
 
   	/*
      User loggedUser = [Select ID,Division from User where Id =:UserInfo.getUserId() limit 1];
      String sCurrencyName = 'USD';
       if(loggedUser.Division=='Singapore')
         sCurrencyName = 'SGD';
       if(loggedUser.Division=='Europe')  
         sCurrencyName = 'EUR';
       if(loggedUser.Division=='Dubai')  
         sCurrencyName = 'AED';
       if(loggedUser.Division=='London')  
         sCurrencyName = 'GBP';
       if(loggedUser.Division=='Moscow')  
         sCurrencyName = 'RUB';
   	return sCurrencyName;
   	*/
   	
   }
	
//	public List<Invoice_Line__c> getreceivedInvoiceItems() {
	public List<Sales_Order_Jewelry_Item__c> getreceivedSalesOrderJewelryItems() {
		
////		if(salesOrderId == NULL){
////			salesOrderId = (Id) System.currentPageReference().getParameters().get('SOId');
////		}
/*
			            <td>{!item.Name}</td> Jewelry__r.Item_Number__c
			            <td>{!item.Description__c}</td> Jewelry__r.Name
			            <td>{!item.Carat__c}</td> Total_Weight__c
			            <td class="total">{!item.Total_Amount__c}</td> Jewelry__r.Total_Cost__c
*/

//Jewelry__r.Item_Number__c, Jewelry__r.item_image_link__c, Jewelry__r.Image__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Description, Jewelry__r.Name

//Select s.Type__c, s.Total_Weight__c, s.Sales_Order__c, s.Primary_Color__c, s.Primary_Clarity__c, s.Name, s.Jewelry__r.Levant_Last_Price__c, s.Jewelry__r.Insurance_Price__c, s.Jewelry__r.Market_Price__c, s.Jewelry__r.Mounting_Cost__c, s.Jewelry__r.Total_Cost__c, s.Jewelry__r.Item_Number__c, s.Jewelry__r.Reporting_Price__c, s.Jewelry__r.Total_Weight__c, s.Jewelry__r.Name, s.Jewelry__c, s.Id, s.Design__c From Sales_Order_Jewelry_Item__c s

		//Select s.Type__c, s.Total_Weight__c, s.Sales_Order__c, s.Primary_Color__c, s.Primary_Clarity__c, s.Name, s.Jewelry__c, s.Id, s.Design__c From Sales_Order_Jewelry_Item__c s
		receivedSalesOrderJewelryItems = [
			Select
			        s.Type__c, s.Total_Weight__c, s.Sales_Order__c, s.Primary_Color__c,
			        s.Primary_Clarity__c, s.Name, s.Jewelry__c, s.Id, s.Design__c,
			        Jewelry__r.Item_Number__c, Jewelry__r.ProductCode, Jewelry__r.item_image_link__c, Jewelry__r.Image__c, 
			        Jewelry__r.Jewelry_Type__c, Jewelry__r.Description, Jewelry__r.Name,
			        Jewelry__r.Total_Cost__c
//				i.Type__c, i.Total_Amount__c, i.Quantity__c, i.Price_Carat__c, i.PO_Number__c, i.Name, 
//				i.Id, i.Description__c, i.Carat__c 
			From 
			        Sales_Order_Jewelry_Item__c s
//				Invoice_Line__c i 
			WHERE 
//				i.Invoice__c = :salesOrderId
				s.Sales_Order__c = :salesOrderId
			ORDER BY
				s.Name ASC
		];

	        for(Sales_Order_Jewelry_Item__c soji :receivedSalesOrderJewelryItems) {
	          this.TotalPrice += soji.Jewelry__r.Total_Cost__c;
	        }
		

/////		this.salesOrderData = salesOrderDataGet();
		return receivedSalesOrderJewelryItems;
	}


	public List<Sales_Order_Stone_Item__c> getreceivedSalesOrderStoneItems() {
		
////		if(salesOrderId == NULL){
////			salesOrderId = (Id) System.currentPageReference().getParameters().get('SOId');
////		}
/*
			            <td>{!item.Name}</td> Jewelry__r.Item_Number__c
			            <td>{!item.Description__c}</td> Jewelry__r.Name
			            <td>{!item.Carat__c}</td> Total_Weight__c
			            <td class="total">{!item.Total_Amount__c}</td> Jewelry__r.Total_Cost__c
*/

//Jewelry__r.Item_Number__c, Jewelry__r.item_image_link__c, Jewelry__r.Image__c, Jewelry__r.Jewelry_Type__c, Jewelry__r.Description, Jewelry__r.Name

//Select s.Type__c, s.Total_Weight__c, s.Sales_Order__c, s.Primary_Color__c, s.Primary_Clarity__c, s.Name, s.Jewelry__r.Levant_Last_Price__c, s.Jewelry__r.Insurance_Price__c, s.Jewelry__r.Market_Price__c, s.Jewelry__r.Mounting_Cost__c, s.Jewelry__r.Total_Cost__c, s.Jewelry__r.Item_Number__c, s.Jewelry__r.Reporting_Price__c, s.Jewelry__r.Total_Weight__c, s.Jewelry__r.Name, s.Jewelry__c, s.Id, s.Design__c From Sales_Order_Jewelry_Item__c s

//Select s.Stone__c, s.Sales_Order__c, s.Name, s.Id From Sales_Order_Stone_Item__c s
//Select s.Stone__r.Price__c, s.Stone__r.Certificate_Number__c, s.Stone__r.Carats__c, s.Stone__r.Description__c, s.Stone__r.Name, s.Stone__c, s.Sales_Order__c, s.Name, s.Id From Sales_Order_Stone_Item__c s

		//Select s.Type__c, s.Total_Weight__c, s.Sales_Order__c, s.Primary_Color__c, s.Primary_Clarity__c, s.Name, s.Jewelry__c, s.Id, s.Design__c From Sales_Order_Jewelry_Item__c s
		receivedSalesOrderStoneItems = [
			Select
			        s.Stone__r.Price__c, s.Stone__r.Certificate_Number__c, s.Stone__r.Carats__c, 
			        s.Stone__r.Description__c, s.Stone__r.Name, s.Stone__c, s.Stone__r.RapNet_Total__c, 
			        s.Sales_Order__c, s.Name, s.Id, s.Stone__r.Rap_Price__c 
//				i.Type__c, i.Total_Amount__c, i.Quantity__c, i.Price_Carat__c, i.PO_Number__c, i.Name, 
//				i.Id, i.Description__c, i.Carat__c 
			From 
			        Sales_Order_Stone_Item__c s
//				Invoice_Line__c i 
			WHERE 
//				i.Invoice__c = :salesOrderId
				s.Sales_Order__c = :salesOrderId
			ORDER BY
				s.Name ASC
		];

	        for(Sales_Order_Stone_Item__c sosi :receivedSalesOrderStoneItems) {
//	          TotalPrice += sosi.Stone__r.Price__c;
	          this.TotalPrice += sosi.Stone__r.Rap_Price__c;
	        }

		//this.salesOrderData = salesOrderDataGet();
		return receivedSalesOrderStoneItems;
	}
	
	public Sales_Order__c salesOrderDataGet() {
		// Getting the current Sales_Order__c data
		Sales_Order__c var_data = new Sales_Order__c();

//		Select s.Potential_Sale__c, s.Partial__c, s.Notes_Description__c, s.Name, s.Id, s.Final__c, s.Delivery__c, s.Delivery_Method__c, s.Delivery_Date__c, s.CurrencyIsoCode, s.CreatedDate, s.CreatedById, s.Approval_Status__c From Sales_Order__c s
//				 i.CreatedDate, i.Terms__c, i.Total__c, i.Tax__c, i.Sub_Total__c, i.Shipping__c, 
//				 i.Due_Date__c, i.Invoice_Date__c, i.Clients__c, i.Invoice_Type__c, i.Name 

		for(Sales_Order__c Sales_Order_tmp:[
			Select 
			         s.Potential_Sale__c, s.Partial__c, s.Attn__c, s.Notes_Description__c, s.Name, s.Id, s.Final__c, 
			         s.Delivery__c, s.Delivery_Method__c, s.Delivery_Date__c, s.CurrencyIsoCode, 
			         s.CreatedDate, s.CreatedById, s.Approval_Status__c,
                 s.BillingStreet__c, s.BillingCity__c,s.BillingState__c,s.BillingPostalCode__c,s.BillingCountry__c,
                 s.ShippingStreet__c, s.ShippingCity__c,s.ShippingState__c,s.ShippingPostalCode__c,s.ShippingCountry__c,
                 s.Deposit_Amount__c, s.Sales_Tax__c
			From 
				Sales_Order__c s
			WHERE 
				s.Id = :salesOrderId
			LIMIT 1
		]){
			var_data = Sales_Order_tmp;
			this.Deposit = Sales_Order_tmp.Deposit_Amount__c;
			this.SalesTax = Sales_Order_tmp.Sales_Tax__c;
			this.Attn = String.valueOf(Sales_Order_tmp.Attn__c);
			this.ShipVia = String.valueOf(Sales_Order_tmp.Delivery_Method__c);
		}

		if(var_data.Potential_Sale__c != NULL){
		        list<Opportunity> opp = [Select o.AccountId, o.CurrencyIsoCode From Opportunity o where o.id = :var_data.Potential_Sale__c];
			// Getting Customer Name
			if(opp[0] != null) this.currencyIsoCode = opp[0].CurrencyIsoCode;
			Account acc = new Account();
		       if(opp.size()==1 && opp[0].AccountId != null)	
			for (Account item:[
				Select 
					a.Name
				From
					Account a
				Where
					a.Id = :opp[0].AccountId
				LIMIT 1
			])
			{
				acc = item;
			}
			String t = String.valueOf(acc.Name);
			this.Customer = t;

		}
		return var_data;
	}

}