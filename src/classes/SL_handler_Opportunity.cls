/*
* Trigger Name	: SL_Opportunity
* Unit Test	: SL_test_handler_Opportunity
* JIRA Ticket	: LEVIEV-55
* Created on	: 09/Nov/2012
* Author	: Vladislav Gumenyuk
* Action	: After Insert and After Update only when Opportunity.Stage has been changed to 'Closed/Won'
* Description   : Trigger to Insert Sales Order with Jewelry Items and Reporting to Partners 
* 
* On Opportunity Where Stage = Closed Won
* Insert Sales Order custom object record and map the following fields
* SalesOrder.PotentialSale = Opportunity.Name
* SalesOrder.SoldBy = Opportunity.Owner
* Also, we need to look at Jewelry (Product) on Opportunity and insert on new Sales Order into Sales Order Jewelry Item
* ***If any Jewelry on Opportunity has a Reporting to Partner relationship, insert a Reporting to Partner at the new Sales Order record and map ALL available fields and relationships between Jewelry and Partner
*/
public with sharing class SL_handler_Opportunity {
	
	private Boolean m_isExecuting = false;
	private Integer BatchSize = 0;
	private static boolean isOpportunityInsertHasRun = false;
	public static boolean isOpportunityUpdateHasRun = false;
    
	public SL_handler_Opportunity(Boolean isExecuting, Integer size) {
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	public void OnBeforeInsert(list<Opportunity> newOpportunityList) {}
	
	public void OnAfterInsert(map<Id,Opportunity> newOpportunitys) {
			isOpportunityInsertHasRun = true;
			createSalesOrder(newOpportunitys, null);
	}
	
	public void OnBeforeUpdate(map<Id,Opportunity> newOpportunitys, map<Id,Opportunity> oldOpportunitys) {}
	
	public void OnAfterUpdate(map<Id,Opportunity> newOpportunitys, map<Id,Opportunity> oldOpportunitys) {
			isOpportunityUpdateHasRun = true;
			createSalesOrder(newOpportunitys, oldOpportunitys);
	}
	
	public void OnBeforeDelete(map<Id,Opportunity> oldOpportunityMap) {}
	
	public void OnAfterDelete(map<Id,Opportunity> oldOpportunityMap) {}
	
	public void OnUndelete(list<Opportunity> restoredOpportunitys) {}

	
	private void createSalesOrder(map<Id,Opportunity> newOpportunitys, map<Id,Opportunity> oldOpportunitys) {
		set<Id> oppIds = new set<Id>();
		for(Id keySet : newOpportunitys.keySet()) {
			if (newOpportunitys.get(keySet).StageName == 'Closed Won' &&
			    (oldOpportunitys == null || oldOpportunitys.get(keySet).StageName != 'Closed Won')) {
				oppIds.add(newOpportunitys.get(keySet).Id);
			}
          	}

          	List<Opportunity> list_ClosedWonOpportunity = 
              [SELECT o.Wire_bank__c, o.Type, o.TotalOpportunityQuantity, o.SystemModstamp, o.StageName, o.Sale_Amount_Category__c, o.Probability, o.Pricebook2Id, o.OwnerId, o.Other_Lost_Sale_Reason__c, o.NextStep, o.Name, o.Lost_Sale_Reason_list__c, o.Lost_Sale_Reason__c, o.Location__c, o.Lead_Sales_Notes__c, o.Lead_Customer_Budget__c, o.LeadSource, o.LastModifiedDate, o.LastModifiedById, o.LastActivityDate, o.IsWon, o.IsDeleted, o.IsClosed, o.Id, o.HasOpportunityLineItem, o.ForecastCategoryName, o.ForecastCategory, o.FiscalYear, o.FiscalQuarter, o.Fiscal, o.ExpectedRevenue, o.Description, o.Deposit_Notes__c, o.Deposit_Method__c, o.Deposit_Amount__c, o.Custom_Items_Attached__c, o.CurrencyIsoCode, o.CreatedDate, o.CreatedById, o.CloseDate, o.Check_Credit_Card_Number__c, o.CampaignId, o.Amount, o.AccountId,
               o.Account.BillingStreet, o.Account.BillingCity, o.Account.BillingState, o.Account.BillingPostalCode, o.Account.BillingCountry,
               o.Account.ShippingStreet, o.Account.ShippingCity, o.Account.ShippingState, o.Account.ShippingPostalCode, o.Account.ShippingCountry
               FROM Opportunity o WHERE o.Id IN :oppIds];

          	List<Sales_Order__c> list_SalesOrder = new List<Sales_Order__c>();
          	for(Opportunity closedOpp: list_ClosedWonOpportunity) { 
          	   Sales_Order__c SalesOrder = new Sales_Order__c();
          	   SalesOrder.Potential_Sale__c = closedOpp.Id;
               SalesOrder.Account__c = closedOpp.AccountId;
          	   SalesOrder.Sold_By__c = closedOpp.OwnerId;
               SalesOrder.BillingStreet__c = closedOpp.Account.BillingStreet;
               SalesOrder.BillingCity__c = closedOpp.Account.BillingCity;
               SalesOrder.BillingState__c = closedOpp.Account.BillingState;
               SalesOrder.BillingPostalCode__c = closedOpp.Account.BillingPostalCode;
               SalesOrder.BillingCountry__c = closedOpp.Account.BillingCountry;

               SalesOrder.ShippingStreet__c = closedOpp.Account.ShippingStreet;
               SalesOrder.ShippingCity__c = closedOpp.Account.ShippingCity;
               SalesOrder.ShippingState__c = closedOpp.Account.ShippingState;
               SalesOrder.ShippingPostalCode__c = closedOpp.Account.ShippingPostalCode;
               SalesOrder.ShippingCountry__c = closedOpp.Account.ShippingCountry;
               
          	   list_SalesOrder.add(SalesOrder);
          	}
          	if(list_SalesOrder.size() > 0)
          	  insert list_SalesOrder;

          	map<id,id> map_Opp2SalesOrder_Id = new map<id,id>();
          	for(Sales_Order__c so: list_SalesOrder) 
          	   map_Opp2SalesOrder_Id.put(so.Potential_Sale__c,so.Id);

            set<id> set_opp_prod_id = new set<id>();
            Set<id> set_salesorder_id = new set<id>();
          	map<id,set<OpportunityLineItem>> map_Opp2Product = new map<id,set<OpportunityLineItem>>();
          	map<id,set<id>> map_prod_salesorders = new map<id,set<id>>(); 
        	// Find out about Product2 (Jewelry)
          	for(OpportunityLineItem oppProduct: [Select o.UnitPrice, o.TotalPrice, o.SortOrder, o.ServiceDate, o.Quantity, o.PricebookEntry.IsDeleted, o.PricebookEntry.ProductCode, o.PricebookEntry.SystemModstamp, o.PricebookEntry.LastModifiedById, o.PricebookEntry.LastModifiedDate, o.PricebookEntry.CreatedById, o.PricebookEntry.CreatedDate, o.PricebookEntry.UseStandardPrice, o.PricebookEntry.IsActive, o.PricebookEntry.UnitPrice, o.PricebookEntry.CurrencyIsoCode, o.PricebookEntry.Product2Id, o.PricebookEntry.Pricebook2Id, o.PricebookEntry.Name, o.PricebookEntry.Id, o.PricebookEntryId, o.OpportunityId, o.ListPrice, o.Is_Custom_Item__c, o.Id, o.Description, o.CurrencyIsoCode From OpportunityLineItem o WHERE o.OpportunityId IN :oppIds]) {
        	   set<OpportunityLineItem> set_prod_id = new set<OpportunityLineItem>();
          	   if(!map_Opp2Product.containsKey(oppProduct.OpportunityId) ) {
          	   	 set_prod_id.add(oppProduct); 
          	   	 
          	     //map_Opp2Product.put(oppProduct.OpportunityId, set_prod_id );
          	   } else {
          	   	 set_prod_id = map_Opp2Product.get(oppProduct.OpportunityId);
          	   	 set_prod_id.add(oppProduct);
          	   	 //map_Opp2Product.put(oppProduct.OpportunityId, set_prod_id );
          	   }
        	   map_Opp2Product.put(oppProduct.OpportunityId, set_prod_id );

          	   set_opp_prod_id.add(oppProduct.PricebookEntry.Product2Id);  
        	   
        	   if(map_prod_salesorders.get(oppProduct.PricebookEntry.Product2Id) != null)
        	     set_salesorder_id = map_prod_salesorders.get(oppProduct.PricebookEntry.Product2Id);
        	   else set_salesorder_id = new set<id>();  
        	   
        	   if(oppProduct.OpportunityId != null && map_Opp2SalesOrder_Id.get(oppProduct.OpportunityId) != null)
        	     set_salesorder_id.add( map_Opp2SalesOrder_Id.get(oppProduct.OpportunityId) );
        	   if(set_salesorder_id != null)
        	     map_prod_salesorders.put(oppProduct.PricebookEntry.Product2Id,set_salesorder_id);
          	}   
          	
          	// Also, we need to look at Jewelry (Product) on Opportunity and insert on new Sales Order into Sales Order Jewelry Item
/*
          	List<Sales_Order_Jewelry_Item__c> list_SalesOrderJewelryItem = new List<Sales_Order_Jewelry_Item__c>();
          	for(Opportunity closedOpp: list_ClosedWonOpportunity) { 
          	   if(map_Opp2Product.containsKey(closedOpp.Id) && map_Opp2Product.get(closedOpp.Id).size() > 0 && map_Opp2SalesOrder_Id.get(closedOpp.Id) != null) {
          	     for(Id prod_id : map_Opp2Product.get(closedOpp.Id) )
          	        if(prod_id != null) {
          	     		Sales_Order_Jewelry_Item__c soji = new Sales_Order_Jewelry_Item__c();
          	     		soji.Jewelry__c = prod_id;
          	     		soji.Sales_Order__c = map_Opp2SalesOrder_Id.get(closedOpp.Id);
          	     		list_SalesOrderJewelryItem.add(soji);
          	        }
          	   }
          	}
          	if(list_SalesOrderJewelryItem.size() > 0)
          	  insert list_SalesOrderJewelryItem;
*/
//             List<Sales_Order_Jewelry_Item__c> list_SalesOrderJewelryItem = new List<Sales_Order_Jewelry_Item__c>();
            List<Sales_Order_Line__c> list_SalesOrderJewelryItem = new List<Sales_Order_Line__c>();
            for(Opportunity closedOpp: list_ClosedWonOpportunity) { 
               if(map_Opp2Product.containsKey(closedOpp.Id) && map_Opp2Product.get(closedOpp.Id).size() > 0 && map_Opp2SalesOrder_Id.get(closedOpp.Id) != null) {
                 for(OpportunityLineItem prod_id : map_Opp2Product.get(closedOpp.Id) )
                    if(prod_id != null) {
/*
                     Sales_Order_Jewelry_Item__c soji = new Sales_Order_Jewelry_Item__c();
                     soji.Jewelry__c = prod_id;
                     soji.Sales_Order__c = map_Opp2SalesOrder_Id.get(closedOpp.Id);
                     list_SalesOrderJewelryItem.add(soji);
*/
                     Sales_Order_Line__c sol = new Sales_Order_Line__c();
                     sol.Jewelry__c = prod_id.PricebookEntry.Product2Id;
                     sol.Quantity__c = prod_id.Quantity;
                     sol.Price__c = prod_id.ListPrice;
                     sol.Sales_Order__c = map_Opp2SalesOrder_Id.get(closedOpp.Id);
                     list_SalesOrderJewelryItem.add(sol);
                    }
               }
            }
            if(list_SalesOrderJewelryItem.size() > 0)
              insert list_SalesOrderJewelryItem;

            // ***If any Jewelry on Opportunity has a Reporting to Partner relationship, insert a Reporting to Partner at the new Sales Order record and map ALL available fields and relationships between Jewelry and Partner
            //Reporting_To_Partners__c
            list<Reporting_To_Partners__c> list_rtp = new list<Reporting_To_Partners__c>();      
            set<String> set_unique_reporting2partners_keys = new set<String>();   
            for(Reporting_To_Partners__c rtp : [SELECT r.Total__c, r.Terms__c, r.Sales_Order__c, r.Percent__c, r.Partner__c, r.PPC__c, r.Name, r.Jewelry_Item__c, r.Invoice__c, r.Id, r.Date__c, r.CurrencyIsoCode FROM Reporting_To_Partners__c r WHERE r.Jewelry_Item__c IN :set_opp_prod_id]) 
            {
               set_unique_reporting2partners_keys.add(String.valueOf(rtp.Jewelry_Item__c)+'-'+String.valueOf(rtp.Partner__c)+'-'+String.valueOf(rtp.Sales_Order__c) );
               if(map_prod_salesorders.get(rtp.Jewelry_Item__c) != null && map_prod_salesorders.get(rtp.Jewelry_Item__c).size()>0 )
                 for(Id so_id : map_prod_salesorders.get(rtp.Jewelry_Item__c) ) {
                    if(!set_unique_reporting2partners_keys.contains(String.valueOf(rtp.Jewelry_Item__c)+'-'+String.valueOf(rtp.Partner__c)+'-'+String.valueOf(so_id) ) ) 
                 	{
                       Reporting_To_Partners__c rtp_new = new Reporting_To_Partners__c();
                       //rtp_new.Date__c = Date.today();
                       rtp_new.Date__c = rtp.Date__c;
                       rtp_new.Total__c = rtp.Total__c;
                       rtp_new.Terms__c = rtp.Terms__c; 
                       rtp_new.PPC__c = rtp.PPC__c; 
                       rtp_new.Percent__c = rtp.Percent__c;
                       rtp_new.Invoice__c = rtp.Invoice__c; 
                       rtp_new.CurrencyIsoCode = rtp.CurrencyIsoCode;
               
                       rtp_new.Jewelry_Item__c = rtp.Jewelry_Item__c;
                       rtp_new.Partner__c = rtp.Partner__c;
                       rtp_new.Sales_Order__c = so_id;  
                       list_rtp.add(rtp_new);
                       set_unique_reporting2partners_keys.add(String.valueOf(rtp.Jewelry_Item__c)+'-'+String.valueOf(rtp.Partner__c)+'-'+String.valueOf(so_id) );
                 	}  
                 }
            }
            insert list_rtp;
	}

}