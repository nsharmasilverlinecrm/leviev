/**
* @ClassName    : SL_salesOrderLine_Handler
* @JIRATicket   : LEVIEV-63
* @CreatedOn    : 24 May 2013
* @ModifiedBy   : 
* @Description  : This is Handler class for SL_SalesOrderLine Trigger.
*/


/**
@Developer Name                         :   Shailendra
Percentage of best practices followed   :   
No of SOQL queries used                 :   4
No of collections used                  :   3
Exception Handling implemented          :   Yes
Coding standards followed               :   Yes
Naming conventions followed             :   Yes
Third party integrations                :   
Maximum of No of records tested with    :   
Customer Approval                       :   
Last Modified Date                      :   
Approved by                             :   

*/


public with sharing class SL_salesOrderLine_Handler 
{
	/* Start - global variables*/
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	set<Id> setProductId = new set<Id>(); // Collect the set of Ids of records which have Jewelry__c field non empty.
	set<Id> setStoneIds = new set<Id>();
	list<Product2> lstProductToUpdate = new list<Product2>(); // List of Jewelry__c records for updation.
	list<Stone__c> lstStoneToUpdate = new list<Stone__c>(); // List of Stone__c records for updation.
	list<Stone__c> lstStoneToUpdate2 = new list<Stone__c>(); // List of Stone__c records for updation.
	/* End - global variables*/
	
	/* Start - Contructor */
	public SL_salesOrderLine_Handler(boolean isExecuting, integer size)
	{
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	/* End - Contructor */
	
	
	/*
		@MethodName : onAfterInsert 
		@param 	    : list of Sales_Order_Line__c records
		@Description: This method will call After insert of Sales_Order_Line__c records .
	*/
	public void onAfterInsert(list<Sales_Order_Line__c> listSalesOrderLineNew)
	{
		// Call setProductIds method to collect the set of ids with non empty Jewelry__c field.
		setProductId = setProductIds(listSalesOrderLineNew);
		setStoneIds = setStoneIds(listSalesOrderLineNew);

		// If the set of Id is not empty then call method to update Status__c field.
		if(!setProductId.isEmpty())
			updateProductAndStoneStatus(setProductId, 'isInsert');

		if(!setStoneIds.isEmpty())
			updateStoneStatus(setStoneIds, 'isInsert');
	}
	
	/*
		@MethodName : onAfterDelete 
		@param 	    : list of Sales_Order_Line__c records
		@Description: This method will call After deletion of Sales_Order_Line__c records .
	*/
	public void onAfterDelete(list<Sales_Order_Line__c> listSalesOrderLineOld)
	{
		// Call setProductIds method to collect the set of ids with non empty Jewelry__c field.
		setProductId = setProductIds(listSalesOrderLineOld);
		setStoneIds = setStoneIds(listSalesOrderLineOld);
		// If the set of Id is not empty then call method to update Status__c field.
		if(!setProductId.isEmpty())
			updateProductAndStoneStatus(setProductId, 'isDelete');
	}
	
	/*
		@MethodName : setProductIds 
		@param 	    : list of Sales_Order_Line__c records
		@Description: This method will call to check wether the Jewelry__c field is empty or not.if not then collect the Ids. 
	*/
	public set<Id> setProductIds(list<Sales_Order_Line__c> listSalesOrderLine)
	{
		// Iterate through list of Sales_Order_Line__c records to check the Jewelry__c field.
		for(Sales_Order_Line__c obj : listSalesOrderLine)
		{
			//If the Jewelry field is not null the collect the Ids.
			if(obj.Jewelry__c != null)
			{
				setProductId.add(obj.Jewelry__c);
			}
		}
		return setProductId;
	}

	public set<Id> setStoneIds(list<Sales_Order_Line__c> listSalesOrderLine)
	{
		// Iterate through list of Sales_Order_Line__c records to check the Jewelry__c field.
		for(Sales_Order_Line__c obj : listSalesOrderLine)
		{
			//If the Jewelry field is not null the collect the Ids.
			if(obj.Stone__c != null)
			{
				setProductId.add(obj.Stone__c);
			}
		}
		return setProductId;
	}	
	/*
		@MethodName : updateProductAndStoneStatus 
		@param 	    : set of productIds and trigger event(string)
		@Description: This method is used to Update product and Stone Status__c .
	*/
	public void updateProductAndStoneStatus(set<Id> setProductId, String strEvent)
	{
		// If the method is called in onAfterInsert method.
		 if( strEvent == 'isInsert' )
		 {
		    
	         // Updating the Product status whose Id is present in setProductIds
	         for( Product2 objProduct : [select Id,Status__c 
							            from Product2
							            where Id In:setProductId ])
	          {
		           objProduct.Status__c='S';
		           lstProductToUpdate.add(objProduct);
	          }	
	          
	         // Updating the Stone status whose Jewelry(productId) is present in setProductIds
	         for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Jewelry__c In:setProductId ])		
	         {
		          objStone.Status__c='S';
		          lstStoneToUpdate.add(objStone);
	         }	
		 }
		 
		 // If the method is called in onAfterDelete method.
		if( strEvent =='isDelete' )
		{
	          // updating the Product whose id is present in setProductIds and Status__c='M'
			for( Product2 objProduct : [select Id,Status__c 
							            from Product2
							            where Id In:setProductId and Status__c='S'])
			{
	 	
				objProduct.Status__c='IN';
		        lstProductToUpdate.add(objProduct);
	        }	
	
	          // updating the Stone whose Jewelry(productId) is present in setProductIds and Status__c='M'
			for(Stone__c objStone :[select Id,Jewelry__c,Status__c
						           from Stone__c
						           where Jewelry__c In:setProductId and Status__c='S'])		
	        {
	       		objStone.Status__c='IN';
		        lstStoneToUpdate.add(objStone);
	        }	
		}
		
		try
		{
			if(lstProductToUpdate.size()>0 )
		   		update lstProductToUpdate;
			   
			if(lstStoneToUpdate.size()>0)   
				update lstStoneToUpdate ;
		}				   
		catch(Exception ex)
		{
		}		
	}

	public void updateStoneStatus(set<Id> setProductId, String strEvent)
	{
		// If the method is called in onAfterInsert method.
		 if( strEvent == 'isInsert' )
		 {
		    
	          
	         // Updating the Stone status whose Jewelry(productId) is present in setProductIds
	         for(Stone__c objStone :[select Id,Jewelry__c
						            from Stone__c
						            where Id In:setProductId ])		
	         {
		          objStone.Status__c='S';
		          lstStoneToUpdate2.add(objStone);
	         }	
		 }
		 
		 // If the method is called in onAfterDelete method.
		if( strEvent =='isDelete' )
		{

	          // updating the Stone whose Jewelry(productId) is present in setProductIds and Status__c='M'
			for(Stone__c objStone :[select Id,Jewelry__c,Status__c
						           from Stone__c
						           where Id In:setProductId and Status__c='S'])		
	        {
	       		objStone.Status__c='IN';
		        lstStoneToUpdate2.add(objStone);
	        }	
		}
		
		try
		{
 
			if(lstStoneToUpdate2.size()>0)   
				update lstStoneToUpdate2 ;
		}				   
		catch(Exception ex)
		{
		}		
	}	
}