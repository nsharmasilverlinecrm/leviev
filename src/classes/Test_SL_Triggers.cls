/**
* This test class will cover such triggers:
* Jewelry_List_Entry_ActiveValidation
* Jewelry_List_ProductValidation
*
* added by Privlad (2011/03/25)
*/
@isTest 
private class Test_SL_Triggers {
    
    static testMethod void cover_Jewelry_List_Entry_ActiveValidation() {
        Test.startTest();
        
        
        
        Test.stopTest();
    }
    
    static testMethod void test_SL_PBEStaging_updPriceBookEntry()
    {
    	Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ',Item_Number__c='Test201104'); 
   		insert pr;
   		
   		Pricebook2 result = [select Id from Pricebook2 where isStandard=true limit 1];
	    ID stdPbId = result.ID;
    
    	// Create a pricebook entry for standard pricebook
    	PricebookEntry pbe = new PricebookEntry(Product2Id = pr.ID, Pricebook2Id = stdPbId, CurrencyIsoCode = 'USD');
		pbe.IsActive = true;
		pbe.UnitPrice = 100.0;
    	insert pbe;
   		
   		
   		Test.startTest();
   		
		PBEStaging__c p = new PBEStaging__c();
		p.stk_status__c = 'open';
		p.stk_key__c = 'Test201104';
		p.PBEKey__c = 'Test201104';
		p.Jewelry__c = pr.ID;
		p.JMS_Edit_Date__c = DateTime.now();
		p.ISOPrice__c = 100.00;
		p.Currency__c = 'USD';
		insert p;

   		Test.stopTest();
    }
    
    static testMethod void cover_Jewelry_List_ProductValidation() {
		Product2 pr = new Product2(IsCheckOut__c = true, Name = 'QQQ'); 
   		insert pr;

        Test.startTest();
        
        Jewelry_List__c testObj = new Jewelry_List__c(Name='test Name');
        testObj.Active__c = false;
        insert testObj;
        
        Jewelry_List__c testObj2 = new Jewelry_List__c(Name='test Name - 3');
        testObj2.Active__c = true;
        insert testObj2;
        
        Jewelry_List_Entry__c le1 = new Jewelry_List_Entry__c();
        le1.Jewelry_List__c = testObj.ID;
        le1.Jewelry__c = pr.ID;
        insert le1;
        
        Jewelry_List_Entry__c le2 = new Jewelry_List_Entry__c();
        le2.Jewelry_List__c = testObj2.ID;
        le2.Jewelry__c = pr.ID;
        insert le2;
        
        
        try
        {
	        testObj.Name = 'test Name - 2';
	        testObj.Active__c = true;
	        update testObj;
        }
        catch(Exception ex){ }
        
        Test.stopTest();
    }
    
}