/**
 * http://silverline.jira.com/browse/LEVIEV-13
 * Need to create triggers to enforce the business rule for Jewelry Lists and Jewelry List Members. 
 * The rule is: a Product can only be a member of one 'Active' list at any given time.
 *
 * Need to create 2 triggers to support this:
 * 1) on Jewelry list member object - when a jewelry list member record is added, if related 
 * Jewelry List is set to 'active' the trigger should check to make sure that the selected 
 * Product is not connect to any other Jewelry lists that are set to 'active'. If so, the trigger should 
 * display an error and not allow the jewelry list member to be created.
 *
 * 2) on Jewelry list object - when the user changes the 'active' flag to 'true', the trigger should 
 * check each connected jewelry list member record and verify that there are no products on that list 
 * which are already connected to another active list. If so, the trigger should display an error and 
 * show the Item Number for each item that is already connected to an active list.
 */
 
trigger Jewelry_List_Entry_ActiveValidation on Jewelry_List_Entry__c (before insert) {

	// get Jewelry List Ids
	Set<Id> JewelryList_Set = new Set<Id>();
	for(Jewelry_List_Entry__c item : Trigger.new)
		JewelryList_Set.add(item.Jewelry_List__c);
	
	// get Jewelry Lists 
	Map<Id, Jewelry_List_Entry__c> Map_JewelryListId_JewelryListEntry = new Map<Id, Jewelry_List_Entry__c>();
	Map<Id, Jewelry_List__c> Map_JewelryListId_JewelryList = 
		new Map<Id, Jewelry_List__c>([SELECT Id FROM Jewelry_List__c WHERE Id in :JewelryList_Set AND Active__c = true]);
	
	// get Product Ids which need to be verified
	Set<Id> productIdSet_forVerify = new Set<Id>();
	for(Jewelry_List_Entry__c item : Trigger.new)
		if (Map_JewelryListId_JewelryList.containsKey(item.Jewelry_List__c))
			productIdSet_forVerify.add(item.Jewelry__c);
	
	// get Product Ids which are connected
	Set<Id> productIdSet_connected = new Set<Id>();
	Map<Id, String> productCodeMap = new Map<Id, String>(); 
	for(Jewelry_List_Entry__c item : [SELECT Id, Jewelry__c, Jewelry__r.ProductCode FROM Jewelry_List_Entry__c WHERE Jewelry__c in :productIdSet_forVerify AND Jewelry_List__r.Active__c = true]){
		productIdSet_connected.add(item.Jewelry__c);
		productCodeMap.put(item.Jewelry__c, item.Jewelry__r.ProductCode);
	}
	
	// add error message to connected products
	for(Jewelry_List_Entry__c item : Trigger.new)
		if (productIdSet_connected.contains(item.Jewelry__c))
			item.addError('Item ' + productCodeMap.get(item.Jewelry__c) + ' cannot be added since it is already a member of this or another active Jewelry List');
}