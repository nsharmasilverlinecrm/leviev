/**
 * http://silverline.jira.com/browse/LEVIEV-13
 * Need to create triggers to enforce the business rule for Jewelry Lists and Jewelry List Members. 
 * The rule is: a Product can only be a member of one 'Active' list at any given time.
 *
 * Need to create 2 triggers to support this:
 * 1) on Jewelry list member object - when a jewelry list member record is added, if related 
 * Jewelry List is set to 'active' the trigger should check to make sure that the selected 
 * Product is not connect to any other Jewelry lists that are set to 'active'. If so, the trigger should 
 * display an error and not allow the jewelry list member to be created.
 *
 * 2) on Jewelry list object - when the user changes the 'active' flag to 'true', the trigger should 
 * check each connected jewelry list member record and verify that there are no products on that list 
 * which are already connected to another active list. If so, the trigger should display an error and 
 * show the Item Number for each item that is already connected to an active list.
 */

trigger Jewelry_List_ProductValidation on Jewelry_List__c (before update) {
	
	// get Jewelry_List__c item for handling
	Set<ID> JewelryListSet_forHandle = new Set<ID>();
	Map<ID,Set<ID>> mapList2LE = new Map<ID,Set<ID>>();
	
	// get all Products for handling lists
	Map<Id, Set<String>> Map_JLId_JLEntrySet = new Map<Id, Set<String>>();	
	
	for(Jewelry_List__c item : Trigger.new)
	{
		if (item.Active__c == true && Trigger.oldMap.get(item.Id).Active__c == false)
		{
			JewelryListSet_forHandle.add(item.ID);
			mapList2LE.put(item.ID, new Set<ID>());
			Map_JLId_JLEntrySet.put(item.ID, new Set<String>());
		}
	}
	if(JewelryListSet_forHandle.size()>0)
	{		
		Set<ID> setProductIDs = new Set<ID>();
		List<Jewelry_List_Entry__c> listLE = [SELECT Id, Name, Jewelry__c, Jewelry_List__c, Jewelry_List__r.Name FROM Jewelry_List_Entry__c WHERE Jewelry_List__c in :JewelryListSet_forHandle]; 
		
		for(Jewelry_List_Entry__c item : listLE) {
			setProductIDs.add(item.Jewelry__c);
			mapList2LE.get(item.Jewelry_List__c).add(item.Jewelry__c);
		}
		
		for(Jewelry_List_Entry__c item : listLE)
		{
			for(ID curID:mapList2LE.keySet())
			{
				for(ID ProdID:mapList2LE.get(curID))
				{
					if(ProdID==item.Jewelry__c && item.Jewelry_List__c!=curID) Map_JLId_JLEntrySet.get(curID).add(item.Jewelry_List__r.Name);
				}
			}
		}
		
		
		
		for(Jewelry_List_Entry__c item : [SELECT Id, Name, Jewelry__c, Jewelry_List__c, Jewelry_List__r.Name  FROM Jewelry_List_Entry__c WHERE Jewelry__c in :setProductIDs AND Jewelry_List__r.Active__c = true]) {
			/*
			if (!Map_JLId_JLEntrySet.containsKey(item.Jewelry_List__c))
				Map_JLId_JLEntrySet.put(item.Jewelry_List__c, new Set<Jewelry_List_Entry__c>());
			Map_JLId_JLEntrySet.get(item.Jewelry_List__c).add(item);
			*/
			for(ID curID:mapList2LE.keySet())
			{
				for(ID ProdID:mapList2LE.get(curID))
				{
					if(ProdID==item.Jewelry__c) Map_JLId_JLEntrySet.get(curID).add(item.Jewelry_List__r.Name);
				}
			}
		}
		
		// add error messages to needed lists
		String itemNumberMsg = '';
		//boolean is
		for(Jewelry_List__c item : Trigger.new) {
			if (Map_JLId_JLEntrySet.containsKey(item.Id)) {
				itemNumberMsg = '';
				for(String itemSet : Map_JLId_JLEntrySet.get(item.Id)) itemNumberMsg += ',' + itemSet;
				item.addError('In this list following items are connected to another active lists: ' + itemNumberMsg.substring(1));
			}
		}
	}
}