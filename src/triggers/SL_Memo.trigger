/*
* Trigger Name	: SL_Memo
* JIRA Ticket	: LEVIEV-61
* Created on	: 23/May/2013
* Author	    : Shailendra
* Action	    : After Insert and After Update 
* Description   : Trigger to make Memo_Line__c Status field 'Shipped' if Memo__c Status is 'Shipped'
*/
trigger SL_Memo on Memo__c (after insert, after update) 
{
	SL_Memo_Handler obj  =new SL_Memo_Handler();
	if(trigger.isAfter && trigger.isInsert)
	{
		obj.onAfterInsert(trigger.new);
	}
	if(trigger.isAfter && trigger.isUpdate)
	{
		obj.onAfterUpdate(trigger.newMap, trigger.oldMap);
	}
}