/**
* @TriggerName  : SL_Memo_Line
* @JIRATicket   : 
* @CreatedOn    : 
* @ModifiedBy   : 
* @Description  : 
*/


trigger SL_Memo_Line on Memo_Line__c (after delete, after insert, after update) 
{
	
	   // Handler class for calling functions based on event
     SL_Memo_Line_Handler objMemoLineHandler = new SL_Memo_Line_Handler(Trigger.isExecuting, Trigger.size);
   
       // fires on After Insert of Memo_Line record
      if(Trigger.isInsert && Trigger.isAfter)
      {
          objMemoLineHandler.onAfterInsert(Trigger.newMap);
      }
    
        // fires on After delete of Memo_Line record
      if(Trigger.isDelete && Trigger.isAfter)
      {
          objMemoLineHandler.onAfterDelete(Trigger.oldMap);
      }   
      if(Trigger.isUpdate && Trigger.isAfter)
      {
        objMemoLineHandler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
      }
    
}