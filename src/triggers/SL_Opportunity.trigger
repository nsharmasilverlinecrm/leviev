/*
* Trigger Name	: SL_Opportunity
* Unit Test	: SL_test_handler_Opportunity
* JIRA Ticket	: LEVIEV-55
* Created on	: 09/Nov/2012
* Author	: Vladislav Gumenyuk
* Action	: After Insert and After Update only when Opportunity.Stage has been changed to 'Closed/Won'
* Description   : Trigger to Insert Sales Order with Jewelry Items and Reporting to Partners 
*/
trigger SL_Opportunity on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	SL_handler_Opportunity handler = new SL_handler_Opportunity(trigger.isExecuting, trigger.size);
	
	if (trigger.isInsert) {
		if(trigger.IsBefore) {
			handler.OnBeforeInsert(trigger.new);
		}
		else {
			handler.OnAfterInsert(trigger.newMap);
                        //SL_handler_Opportunity.OnAfterInsertAsync(trigger.newMap.keySet());
		}
	}
	else if (trigger.isUpdate) {
		if (trigger.IsBefore) {
			handler.OnBeforeUpdate(trigger.newMap, trigger.oldMap);
		}
		else {
			handler.OnAfterUpdate(trigger.newMap, trigger.oldMap); 
                        //SL_handler_Opportunity.OnAfterUpdateAsync(trigger.newMap.keySet());
		}
	}
	else if (trigger.isDelete) {
                if (trigger.IsBefore) {
                    handler.OnBeforeDelete(trigger.oldMap);
                }
                else {
                    handler.OnAfterDelete(trigger.oldMap);
                   //SL_handler_Opportunity.OnAfterDeleteAsync(trigger.newMap.keySet());
                }
        }
        else {
        	handler.OnUndelete(trigger.new);
        }
}