trigger SL_PBEStaging_updPriceBookEntry on PBEStaging__c (after insert, after update)
{
	//OrgSettings__c.getValues('PricebookID').Value__c
	List<String> stkKeyIDs = new List<String>();
	Map<String,ID> mapPBExtID2TriID = new Map<String,ID>();
	Map<ID,PBEStaging__c> mapID2Obj = new Map<ID,PBEStaging__c>();
	map<String,ID> mapExtID2ID = new map<String,ID>();
	map<String,boolean> mapCheckIDCur2Exist = new map<String,boolean>();


	for(PBEStaging__c item: trigger.new)
	{
		if(item.stk_key__c!=null && item.stk_key__c!='')
		{
			stkKeyIDs.add(item.stk_key__c);
			mapID2Obj.put(item.ID, item);
			//mapExtID2ID.put(item.stk_key__c, null);
		}
	}
	
	if(stkKeyIDs.size()>0)
	{
		
		OrgSettings__c CSObj = OrgSettings__c.getOrgDefaults();
		ID CSPricebookID = (ID)CSObj.PricebookID__c;
		ID StandardPricebookID = [select ID from Pricebook2 where Name='Standard Price Book'].ID;
		
		
		map<ID,String> mapProdID2ItemNumb = new map<ID,String>();
		for(Product2 item:[Select p.Item_Number__c, p.Id From Product2 p where p.Item_Number__c in :stkKeyIDs])
		{
			mapExtID2ID.put(item.Item_Number__c,item.ID);
			mapProdID2ItemNumb.put(item.ID,item.Item_Number__c);
		}
		
		// add Currency MAP<ProdID+'-'+Currency, PBEStagingID>
		for(PBEStaging__c item: trigger.new)
		{
			mapPBExtID2TriID.put(mapExtID2ID.get(item.stk_key__c)+'-'+item.CurrencyIsoCode, item.ID);
		}
		
		//----------------------
		map<String, PricebookEntry> mapID2PE = new map<String, PricebookEntry>(); 
		for(PricebookEntry item:[Select p.UseStandardPrice, p.UnitPrice, p.SystemModstamp, p.ProductCode, p.Pricebook2.Name, p.Product2Id, p.Pricebook2Id, p.Name, p.IsActive, p.Id, p.CurrencyIsoCode From PricebookEntry p where p.Product2Id in :mapExtID2ID.values() and p.Pricebook2.Name='Standard Price Book'])
		{
			mapID2PE.put(item.Product2Id+'-'+item.CurrencyIsoCode, item);
		}
		System.debug('mapID2PE::::::::: '+mapID2PE);
		
		
		
		Map<String,ID> mapStkKey2PBEID = new Map<String,ID>();
		for(PricebookEntry item:[select Id,Product2Id,CurrencyIsoCode from PricebookEntry where Product2Id in :mapExtID2ID.values() and Pricebook2Id = :CSPricebookID])
		{
			mapStkKey2PBEID.put(item.Product2Id+'-'+item.CurrencyIsoCode,item.ID);
		}
		
		List<PricebookEntry> upsertStandardAddList = new List<PricebookEntry>();
		Map<String,PricebookEntry> upsertStandardAddMapPre = new Map<String,PricebookEntry>();

		List<PricebookEntry> upsertList = new List<PricebookEntry>();
		Map<String,PricebookEntry> upsertMapPre = new Map<String,PricebookEntry>();
		
		Schema.DescribeFieldResult fieldResult = PBEStaging__c.CurrencyIsoCode.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		
		for(PBEStaging__c item: trigger.new)
		{
			String strID = item.stk_key__c;
			PricebookEntry PEItem;
			boolean isError = false;
			
			
			if(mapExtID2ID.containsKey(strID)){ PEItem = new PricebookEntry(Product2Id = mapExtID2ID.get(strID), Pricebook2Id = CSPricebookID, CurrencyIsoCode = item.Currency__c);}
			else
			{
				//PEItem = new PricebookEntry(Pricebook2Id = OrgSettings__c.getValues('PricebookID').Value__c, CurrencyIsoCode = mapID2Obj.get(mapPBExtID2TriID.get(strID)).Currency__c);
				mapID2Obj.get(mapPBExtID2TriID.get(strID)).addError('Incorrect \'stk_key\' value!');
				isError = true;
			}
			
			boolean inList = false;
			String strTmp = '';
			for( Schema.PicklistEntry f : ple)
			{
				strTmp += ', '+ f.getValue();
				if(item.Currency__c==f.getValue()) inList = true;
			}       
			
			if(!inList)
			{
				if(strTmp!='') strTmp = strTmp.substring(2);
				mapID2Obj.get(item.ID).addError('Incorrect \'Currency\' value! Possible values: '+strTmp);
				isError = true;
			}
			
			if(!isError)
			{
				if(CSPricebookID!=StandardPricebookID && !mapID2PE.containsKey(mapExtID2ID.get(strID)+'-'+item.Currency__c))
				{
					PricebookEntry PEItemAdd = new PricebookEntry(Product2Id = mapExtID2ID.get(strID), Pricebook2Id = StandardPricebookID, CurrencyIsoCode = item.Currency__c);
					PEItemAdd.UseStandardPrice = false;
					PEItemAdd.IsActive = true;
					PEItemAdd.UnitPrice = item.ISOPrice__c;
					//upsertStandardAddList.add(PEItemAdd);
					upsertStandardAddMapPre.put(mapExtID2ID.get(strID)+'-'+item.Currency__c, PEItemAdd);
				}
				
				
				if(mapStkKey2PBEID.containsKey(mapExtID2ID.get(strID)+'-'+item.Currency__c))
				{
					PEItem = new PricebookEntry(ID=mapStkKey2PBEID.get(mapExtID2ID.get(strID)+'-'+item.Currency__c), Product2Id = mapExtID2ID.get(strID), Pricebook2Id = CSPricebookID, CurrencyIsoCode = item.Currency__c);
				}
			}
			
			if(!isError){
				PEItem.UseStandardPrice = false;
				PEItem.IsActive = true;
				PEItem.UnitPrice = item.ISOPrice__c;
				
				upsertMapPre.put(mapExtID2ID.get(strID)+'-'+item.Currency__c, PEItem);
			}
		}
		
		//try
		//{
			//System.debug('------------ '+upsertList);
			
			System.debug('upsertStandardAddMapPre::::::::: '+upsertStandardAddMapPre);
			upsertStandardAddList = upsertStandardAddMapPre.values();
			if(upsertStandardAddList.size()>0) insert upsertStandardAddList;

			upsertList = upsertMapPre.values();
			if(upsertList.size()>0) upsert upsertList;
		//}
		//catch(exception ex){}
		
	}


}