/*
* Trigger Name	: SL_SalesOrderLine
* JIRA Ticket	: LEVIEV-63
* Created on	: 24/May/2013
* Author	    : Shailendra
* Action	    : After Insert and After Delete 
* Description   : 
*/


trigger SL_SalesOrderLine on Sales_Order_Line__c (after delete, after insert) 
{
	// Handler class for calling functions based on event
	SL_salesOrderLine_Handler obj = new SL_salesOrderLine_Handler(Trigger.isExecuting, Trigger.size);
	
	 // fires on After Insert of Sales_Order_Line__c record
	if(trigger.isAfter && trigger.isInsert)
	{
		obj.onAfterInsert(trigger.new);
	}
	
	 // fires on After Deletion of Sales_Order_Line__c record
	if(trigger.isAfter && trigger.isDelete)
	{
		obj.onAfterDelete(trigger.old);
	}
}