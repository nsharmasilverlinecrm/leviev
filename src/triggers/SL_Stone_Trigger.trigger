trigger SL_Stone_Trigger on Stone__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	SL_Stone_Handler handler = new SL_Stone_Handler(trigger.isExecuting, trigger.size);
	
	if (trigger.isInsert) {
		if(trigger.IsBefore) {
			handler.OnBeforeInsert(trigger.new);
		}
		else {
			handler.OnAfterInsert(trigger.newMap);
		}
	}
	
	else if (trigger.isUpdate) {
		if (trigger.IsBefore) {
			handler.OnBeforeUpdate(trigger.newMap, trigger.oldMap);
		}
		else {
			handler.OnAfterUpdate(trigger.newMap, trigger.oldMap); 
		}
	}
	
	else if (trigger.isDelete) {
        if (trigger.IsBefore) {
            handler.OnBeforeDelete(trigger.oldMap);
        }
        else {
            handler.OnAfterDelete(trigger.oldMap);
        }
    }
     
    else {
        handler.OnUndelete(trigger.new);
    }
}